﻿<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
    <meta charset="UTF-8"/>
    <title>HRC-BOAT Vietnam</title>
	<link rel="stylesheet" href="static/build/all.min.css"/>
    <link rel="manifest" href="manifest.json"/>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" type="image/x-icon" href="static/i/favicon@0,75x.png"/>
    <link rel="icon" sizes="192x192" href="static/i/favicon@4x.png">
	<meta property="og:type" content="website"/>
	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:image" content="/static/i/preview.jpg"/>
	<meta property="og:image:width" content="1200"/>
	<meta property="og:image:height" content="675"/>
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:site" content="@hrcboat"/>
	<meta name="twitter:creator" content="@hrcboat"/>
	<meta name="twitter:title" content=""/>
	<meta name="twitter:description" content=""/>
	<meta name="twitter:image" content="/static/i/preview.jpg"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="apple-touch-icon" sizes="60x60" href="static/i/touch-icon@60w.png">
    <link rel="apple-touch-icon" sizes="76x76" href="static/i/touch-icon@76w.png">
    <link rel="apple-touch-icon" sizes="120x120" href="static/i/touch-icon@120w.png">
    <link rel="apple-touch-icon" sizes="152x152" href="static/i/touch-icon@152w.png">
</head>

<body>
    <div id="content" class="content">
    <div class="modal modal-youtube">
        <div class="modal-bg"></div>
        <div class="modal-close"></div>
        <div id="player" class="modal-player"></div>
    </div>
    <div class="sections">
        
        <div id="about" class="section section-about scrollto">
            <div class="section-i">
                <div class="section-image-preview"></div>
                <div class="main-logo vertical">
					<div class="main-logo-m"></div>
				</div>
				<div class="landing">
					<div class="landing-slide">
						<div class="landing-title">
							
						</div>
					</div>
				</div>
            </div>
            <div class="swipe section-bg section-bg-inner section-bg-about bg-loop">
                <div class="swipe-wrap">
                    
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/rc-boats.png);"
                         data-pic="static/i/rc-boats.png"></div>
                    
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/rc-boats-1.png);"
                         data-pic="static/i/rc-boats-1.png"></div>
                    
                </div>
            </div>
            <a class="swipe-close modal-close"></a>
            <div class="swipe-controls">
                <a class="swipe-controls-btn swipe-controls-btn-prev"></a>
                <a class="swipe-controls-btn swipe-controls-btn-next"></a>
            </div>
            <div class="swipe-points" data-colors='["w","w"]'>
                <div class="swipe-point" data-index="0"></div>
                <div class="swipe-point" data-index="1"></div>
            </div>
        </div>
        
        <div id="intro" class="section section-intro scrollto">
			<div class="section-i">
				<div class="section-image-preview"></div>
				<div class="section-title">
					Latest&nbsp;News
				</div>
			</div>
			<div data-background-video='{"mp4":"v2_rc_testing.mp4","ogv":"v2_rc_testing.ogv","webm":"v2_rc_testing.webm","poster":"v2_rc_testing.jpg"}' class="section-bg bg-video section-bg-inner section-bg-intro" style="background-image: url(/static/i/v2_rc_testing.jpg);"></div>
		</div>
        
        <div id="robocar" class="section section-robocar scrollto">
            <div class="section-i">
                <div class="section-image-preview"></div>
				<div class="section-title">
					HRC-BOAT
				</div>
				<div class="section-text">
					<p> World’s first competition for human + machine teams, using both self-driving and manually-controlled cars. It is a new platform for brands, organizations and individuals to test the development of their automated driving systems. <br /><br />Race formats will feature new forms of immersive entertainment to engage the public’s imagination. Through sport, innovations in autonomous technology will be advanced, accelerating improvements to road safety.</p> HRC-BOAT <br><br> The world’s first driverless electric racing car. Designed by Daniel Simon, known for his work on Hollywood films such as Tron: Legacy, Oblivion and Captain America.</p>
				</div>
				<div class="section-video">
					<div data-id="-U-PLCtn_7M" class="section-video-control youtube-video"></div>
					<div class="section-video-text">
						Watch video
					</div>
				</div>
            </div>
            
            <div class="swipe section-bg section-bg-inner section-bg-robocar bg-loop">
                <div class="swipe-wrap">
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/slider-01.jpg);"
                         data-pic="static/i/slider-01.jpg"></div>
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/slider-02.jpg);"
                         data-pic="static/i/slider-02.jpg"></div>
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/slider-03.jpg);"
                         data-pic="static/i/slider-03.jpg"></div>
                </div>
            </div>
            <a class="swipe-close modal-close"></a>
            <div class="swipe-controls">
                <a class="swipe-controls-btn swipe-controls-btn-prev"></a>
                <a class="swipe-controls-btn swipe-controls-btn-next"></a>
            </div>
            
            <div class="swipe-points" data-colors='["b","w","w","w","w"]'>
                <div class="swipe-point" data-index="0"></div>
                <div class="swipe-point" data-index="1"></div>
                <div class="swipe-point" data-index="2"></div>
            </div>
        </div>
        
        <div id="devbot" class="section section-devbot scrollto">
            <div class="section-i">
                <div class="section-image-preview"></div>
				<div class="section-title">
					devbot
				</div>
				<div class="section-subtitle">
					Our development car
				</div>
				<div class="section-text">
					<p>The primary purpose of&nbsp;the DevBot is&nbsp;to&nbsp;allow teams to&nbsp;develop their software and experience the hardware that will be&nbsp;used on&nbsp;the "HRC-BOAT".</p><p>Unlike the HRC-BOAT the DevBot has a&nbsp;cabin that can be&nbsp;driven by&nbsp;a&nbsp;human or&nbsp;a&nbsp;computer allowing teams to&nbsp;fully understand how the car thinks and feels on&nbsp;a&nbsp;racetrack alongside the comprehensive real-time data.</p>
				</div>
				<div class="section-video">
					<div data-id="xrfCQ7qwPh8" class="section-video-control youtube-video"></div>
					<div class="section-video-text">
						Watch devbot
					</div>
				</div>
            </div>
            
            <div class="swipe section-bg section-bg-inner section-bg-devbot bg-loop">
                <div class="swipe-wrap">
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/slider-04.jpg);"
                         data-pic="static/i/slider-04.jpg"></div>
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/slider-05.jpg);"
                         data-pic="static/i/slider-05.jpg"></div>
					<div class="bg-pic section-bg-pic" style="background-image: url(static/i/slider-06.jpg);"
                         data-pic="static/i/slider-06.jpg"></div>
                </div>
            </div>
			
            <a class="swipe-close modal-close"></a>
            <div class="swipe-controls">
                <a class="swipe-controls-btn swipe-controls-btn-prev"></a>
                <a class="swipe-controls-btn swipe-controls-btn-next"></a>
            </div>
            
            <div class="swipe-points" data-colors='["w","b"]'>
                <div class="swipe-point" data-index="0"></div>
                <div class="swipe-point" data-index="1"></div>
				<div class="swipe-point" data-index="2"></div>
            </div>
            
        </div>
        
        <div id="stories" class="section section-stories scrollto">
            <div class="section-i">
				<div class="section-title">
					What is HRC-BOAT?
				</div>
				<div class="section-subtitle">
					Human + Machine teams working together to advance autonomous driving technology
				</div>
				<div class="video">
					<div class="video-list-c">
						<img src="static/i/ios-arrow-left.svg" alt="" class="video-btn-prev video-btn">
						<div class="video-list-i">
							<div class="video-list">
								<div class="video-list-item">
									<div class="video-list-content">
										<div class="video-list-title">RC BOATS RACING</div>
										<div class="video-list-preview youtube-video" data-id="EfHTlgHj8WI">
											<div class="video-list-pic" style="background-image: url('https://img.youtube.com/vi/EfHTlgHj8WI/maxresdefault.jpg')"></div>
										</div>
									</div>
								</div>
								<div class="video-list-item">
									<div class="video-list-content">
										<div class="video-list-title">ULTRA FAST! RC BOAT RACE!</div>
										<div class="video-list-preview youtube-video" data-id="4k5fD6_Jl1o">
											<div class="video-list-pic" style="background-image: url('https://img.youtube.com/vi/4k5fD6_Jl1o/maxresdefault.jpg')"></div>
										</div>
									</div>
								</div>
								<div class="video-list-item">
									<div class="video-list-content">
										<div class="video-list-title">R/C Hydroplane Pit Tour - 2018 RCUnlimiteds Bernie Little Memorial</div>
										<div class="video-list-preview youtube-video" data-id="eJF3cP_WZ9Q">
											<div class="video-list-pic" style="background-image: url('https://img.youtube.com/vi/eJF3cP_WZ9Q/maxresdefault.jpg')"></div>
										</div>
									</div>
								</div>
								<div class="video-list-item">
									<div class="video-list-content">
										<div class="video-list-title">Pro Boat UL-19 first run with speed test (63.5mph) 100% all stock.</div>
										<div class="video-list-preview youtube-video" data-id="uyHuPh9Qlv0">
											<div class="video-list-pic" style="background-image: url('https://img.youtube.com/vi/uyHuPh9Qlv0/maxresdefault.jpg')"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<img src="static/i/ios-arrow-right.svg" alt="" class="video-btn video-btn-next">
					</div>
					<div class="video-list-subscribe-control">
						<div class="g-ytsubscribe" data-channelid="UCDThZNmxhmEOfp0sm7HtFvw" data-layout="default" data-theme="dark"
							 data-count="hidden"></div>
						<div class="video-list-subscribe-text">subscribe</div>
					</div>
				</div>
            </div>
            <div class="section-bg section-bg-inner section-bg-stories" style="background-image: url(static/i/inside.jpg);"
                 data-pic="static/i/inside.jpg"></div>
        </div>
        
        <div id="partners" class="section section-partners scrollto">
            <div class="section-i">
                <div class="section-image-preview"></div>
				<div class="section-title">
					partners
				</div>
				<div class="partners">
					<div class="partners-i">
					
					<div class="partner-title nvidia" data-partner="nvidia">Nvidia</div>
					
					<div class="partner-title michelin" data-partner="michelin">Michelin</div>
					
					<div class="partner-title ARRIVAL" data-partner="ARRIVAL">ARRIVAL</div>
					
					</div>
				</div>
				<div class="partner-logos">
					<div class="partner-logo nvidia">
					
					</div>
					
					<div class="partner-logo michelin">
							<a href="http://www.michelin.com/" target="_blank" class="section-logo-pic partner-logo-pic" style="background-image: url('static/i/michelin-logo.svg')"></a>
					</div>
					
					<div class="partner-logo ARRIVAL">
					
					</div>
				</div>
				<div class="partner-partnership-link">Become a Partner</div>  
            </div>
            
            <div class="swipe section-bg section-bg-inner section-bg-partners bg-loop-partner">
                <div class="swipe-wrap">
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/nvidia.jpg);"
                         data-pic="static/i/nvidia.jpg"></div>
                    
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/michelin.jpg);"
                         data-pic="static/i/michelin.jpg"></div>
                    
                    <div class="bg-pic section-bg-pic" style="background-image: url(static/i/arrival.jpg);"
                         data-pic="static/i/arrival.jpg"></div>
                    
                </div>
            </div>
            <a class="swipe-close modal-close"></a>
            <div class="swipe-controls">
                <a class="swipe-controls-btn swipe-controls-btn-prev"></a>
                <a class="swipe-controls-btn swipe-controls-btn-next"></a>
            </div>
            
            <div class="swipe-points" data-colors='["w","w","w"]'>
                <div class="swipe-point" data-index="0"></div>
                <div class="swipe-point" data-index="1"></div>
                <div class="swipe-point" data-index="2"></div>
            </div>
        </div>
        
        <div id="joinus" class="section section-joinus scrollto">
            <div class="section-i">
                <form data-persist="garlic" id="feedback-form" class="form">
				<div class="form-i">
					<div class="step">
						<div class="form-header">Join us</div>
						<div class="form-subheader">
							<p>We are looking for the brightest minds to help us grow.</p>
							<p>Get in touch if you want to help create the future of mobility.</p>
						</div>
						<div class="form-subheader-success">
							<p>Thank you for taking an interest in a career with HRC-BOAT. Please check your emails.</p>
						</div>
						<div class="form-input">
							<label class="form-input-label">Name:</label>
							<input
								autocomplete="off"
								autocorrect="off"
								autocapitalize="off"
								spellcheck="false"
								class="form-input"
								name="name"
								placeholder="First Last"
							/>
						</div>
						<div class="form-input">
							<label class="form-input-label">Email Address:</label>
							<input
								autocomplete="off"
								autocorrect="off"
								autocapitalize="off"
								spellcheck="false"
								class="form-input"
								name="email"
								placeholder="you@example.com"
							/>
						</div>
						<div class="form-input-textarea">
							<label class="form-input-label">Tell us a bit about you:</label>
							<textarea
								autocomplete="off"
								autocorrect="off"
								autocapitalize="off"
								spellcheck="false"
								class="form-input-textarea"
								name="text"
								placeholder="Tell us a bit about you."
							></textarea>
						</div>
						<div class="form-error">
							Please check your email address.
						</div>
						<div class="form-actions form-actions-joinus">
							<input type="file" name="cv" id="cv" accept=".doc, .docx, .pdf">
							<label for="cv" class="form-action form-action-primary">Upload your CV (PDF or docx)</label>
							<button type="submit" class="joinus-button cv-submit form-action form-action-primary">Send</button> 
							 
						</div>
						<div style="text-align:center">    
						<br><br> <a href="/static/docs/PrivacyPolicy.pdf">Privacy Policy</a>
						</div>
					</div>
				</div>
				</form>
            </div>
        </div>
    </div>
</div>
<div class="menu">
    <div class="menu-i">
        <div class="menu-item menu-item-intro">
            <a href="#intro" class="menu-item-a">
                latest news
            </a>
        </div>
        <div class="menu-item menu-item-robocar">
            <a href="#robocar" class="menu-item-a">
                hrc-boat
            </a>
        </div>
        <div class="menu-item menu-item-devbot">
            <a href="#devbot" class="menu-item-a">
                devbot
            </a>
        </div>
        <div class="menu-item menu-item-stories">
            <a href="#stories" class="menu-item-a">
                stories
            </a>
        </div>
        <div class="menu-item menu-item-partners">
            <a href="#partners" class="menu-item-a">
                partners
            </a>
        </div>
        <div class="menu-item menu-item-joinus">
            <a href="#joinus" class="menu-item-a">
                Join us
            </a>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/garlic.js/1.2.4/garlic.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/ScrollToPlugin.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>
<script type="text/javascript" src="static/typed.js"></script>
<script>
    document.write('<scrip' + 't src="js/check-code.js?v=' + Date.now() + '"></s' + 'cript>');
</script>
<script>
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

</script>
<script type="text/javascript" src="static/build/all.js"></script>
<script type="text/javascript" src="https://www.youtube.com/iframe_api"></script>

<!-- Metrika -->
<script>
(function(d, w, c) {
       (w[c] = w[c] || []).push(function() {
           try {
               w.yaCounter34021130 = new Ya.Metrika({
                   id: 34021130,
                   clickmap: true,
                   trackLinks: true,
                   accurateTrackBounce: true,
                   webvisor: true
               });
           } catch (e) {
           }
       });
       var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
           n.parentNode.insertBefore(s, n);
       };
       s.type = "text/javascript";
       s.async = true;
       s.src = "https://mc.yandex.ru/metrika/watch.js";
       if (w.opera == "[object Opera]") {
           d.addEventListener("DOMContentLoaded", f, false);
       } else {
           f();
       }
   })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/34021130" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
</body>
</html>

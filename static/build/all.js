﻿/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	__webpack_require__(1);
	var initForm = __webpack_require__(2);
	var menu = __webpack_require__(80);
	var sections = __webpack_require__(83);
	var youtubePlayers = __webpack_require__(86);
	var bgLoop = __webpack_require__(84);
	var bgVideos = __webpack_require__(87);
	var follow = __webpack_require__(88);
	var partner = __webpack_require__(89);
	var inside = __webpack_require__(90);

	window.mediaQueryMatch = 3;

	if (window.matchMedia("screen and (min-width: 22em)").matches) {
	    window.mediaQueryMatch = 2;
	}
	if (window.matchMedia("screen and (min-width: 48em)").matches) {
	    window.mediaQueryMatch = 1;
	}
	if (window.matchMedia("screen and (min-width: 79em)").matches) {
	    window.mediaQueryMatch = 0;
	}
	if (window.matchMedia("screen and (max-height: 414px)").matches) {
	    window.mediaQueryMatch = 4;
	}

	$(function () {
	    initForm();
	    menu.init();
	    sections.init();

	    follow.init();
	    partner.init();
	    inside.init();

	    bgLoop.init();
	    bgVideos.init();

	    window.onYouTubeIframeAPIReady = youtubePlayers.init;
	});

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	'use strict';

	(function () {
	    if (!Array.prototype.find) {
	        Object.defineProperty(Array.prototype, 'find', {
	            value: function value(predicate) {
	                'use strict';

	                if (this == null) {
	                    throw new TypeError('Array.prototype.find called on null or undefined');
	                }
	                if (typeof predicate !== 'function') {
	                    throw new TypeError('predicate must be a function');
	                }
	                var list = Object(this);
	                var length = list.length >>> 0;
	                var thisArg = arguments[1];
	                var value;

	                for (var i = 0; i < length; i++) {
	                    value = list[i];
	                    if (predicate.call(thisArg, value, i, list)) {
	                        return value;
	                    }
	                }
	                return undefined;
	            }
	        });
	    }
	})();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _stringify = __webpack_require__(3);

	var _stringify2 = _interopRequireDefault(_stringify);

	var _taggedTemplateLiteral2 = __webpack_require__(6);

	var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

	var _templateObject = (0, _taggedTemplateLiteral3.default)(['<div class="form-header">', '</div>'], ['<div class="form-header">', '</div>']),
	    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['<div class="form-text-content">', '</div>'], ['<div class="form-text-content">', '</div>']),
	    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['<div data-step-id="', '" class="form-step form-step-', '">', '</div>'], ['<div data-step-id="', '" class="form-step form-step-', '">', '</div>']);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var helpers = __webpack_require__(46);
	var menu = __webpack_require__(80);

	var $form = $('#feedback-form');
	var $formSteps = $form.find('.steps');
	var $submitControl = $('#form-submit');
	var $cvInput = $("input[type='file'][name='cv']");
	var topics = __webpack_require__(82).topics;

	var currentTopic = void 0;
	var wasCaptcha = false;

	/**
	 * Make Topic screens
	 * @param {Object} topic
	 * @param {Object} step
	 */
	var makeOneStep = function makeOneStep(topic, step) {
	    var stepInside = '';
	    var stepContent = '';

	    if (step.header !== false) {
	        stepInside += helpers.html(_templateObject, typeof step.header !== 'undefined' ? step.header : topic.topic);
	    }

	    if (step.textFields) {
	        step.textFields.forEach(function (field) {
	            stepContent += helpers.renderTmpl('./form/textInputTmpl.ejs', { data: field });
	        });
	    }

	    if (step.radioGroup) {
	        stepContent += helpers.renderTmpl('./form/radioTmpl.ejs', { data: step.radioGroup });
	    }

	    if (step.hidden) {
	        stepContent += helpers.renderTmpl('./form/hiddenTmpl.ejs', { data: step.hidden });
	    }

	    if (step.textarea) {
	        stepContent += helpers.renderTmpl('./form/textAreaTmpl.ejs', { data: step.textarea });
	    }

	    if (step.textContent || step.social) {
	        var doneContent = '';

	        if (step.textContent) {
	            doneContent += helpers.html(_templateObject2, step.textContent);
	        }

	        if (step.social) {
	            doneContent += helpers.renderTmpl('./common/socialTmpl.ejs', { data: step.social });
	        }

	        stepContent += '<div class="form-done">' + doneContent + '</div>';
	    }

	    stepInside += '<div class="form-step-content">' + stepContent + '</div>';

	    if (step.actions) {
	        stepInside += helpers.renderTmpl('./form/buttonsTmpl.ejs', { data: step.actions });
	    }

	    return helpers.html(_templateObject3, step.id, step.id, stepInside);
	};

	var modifyStepsLine = function modifyStepsLine(topicId) {
	    var val = void 0;
	    var content = '';

	    if (topicId === 'join-us-as-partner-or-sponsor') {
	        val = $form.find('input[name=partner-type]:checked').attr('data-index');
	        $formSteps.find('.form-step-step2').remove();
	    }

	    if (typeof val !== 'undefined') {
	        var steps = topics[topicId].steps.filter(function (step) {
	            return step.line == (val == 3 ? 1 : 0);
	        });

	        steps.forEach(function (step) {
	            content += makeOneStep(topics[topicId], step);
	        });

	        $formSteps.append(content);
	    }
	};
	/**
	 * Shows a fieldset associated with selected topic.
	 * @param {String} topicId
	 * @param {String} stepId
	 */
	var selectTopic = function selectTopic(topicId, stepId) {

	    var makeSteps = function makeSteps() {
	        var topicId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'subscribe-to-news';

	        var topic = topics[topicId];

	        var content = '<input type="hidden" name="topic"  value="' + topic.topic + '"/>' + ('<input type="hidden" name="topicValue"  value="' + topic.topicValue + '"/>') + '<input name="company"  value="" class="js-company-field"/>';

	        topic.steps.forEach(function (step) {
	            content += makeOneStep(topic, step);
	        });

	        $formSteps.html(content);
	    };

	    if (currentTopic !== topicId) {
	        makeSteps(topicId);
	        currentTopic = topicId;
	    }

	    if (stepId) {
	        $formSteps.find('.form-step').removeClass('active');
	        $formSteps.find('.form-step-' + stepId).addClass('active');
	    }
	    if (stepId === 'done') {
	        $form.find('.close').addClass('is-hidden');
	    }

	    $form.garlic();

	    toggleDisabled();
	};

	/**
	 * Serializes form
	 * @returns {Object}
	 */
	var serialize = function serialize() {
	    var $detailsInputs = $(':input', $form).not('[name=name], [name=email], [name=topic], [name=topicValue], [name=message],[name=captcha], [name=captcha-code], button');

	    // Map inputs to a list of "<key>: <value>" lines
	    var details = {};
	    $detailsInputs.each(function () {
	        var $input = $(this);
	        var name = $input.attr('name');
	        var text = $input.siblings('label').text();
	        var type = $input.attr('type');
	        var value = void 0;

	        if (type === 'radio') {
	            text = $input.parents('label').text();
	            value = $input.is(':checked') ? $input.val() : false;
	        } else {
	            value = $input.val();
	        }

	        if (name === 'apply-type') {
	            text = 'Apply for';
	        }

	        if (name === 'partner-type') {
	            text = 'Apply as';
	        }

	        if (name === 'experience') {
	            text = 'Experience';
	        }

	        if (type === 'radio' && value || type !== 'radio') {
	            details[name] = { text: text, value: value };
	        }
	    });

	    return {
	        name: $('[name=name]', $form).val(),
	        email: $('[name=email]', $form).val(),
	        topic: $('[name=topic]', $form).val(),
	        topicValue: $('[name=topicValue]', $form).val(),
	        details: details,
	        message: $('[name=message]', $form).val()
	    };
	};

	/**
	 * Parses url and returns route
	 * @returns {Object}
	 */
	var getRoute = function getRoute() {
	    var hash = window.location.hash || '';

	    if (hash.indexOf('#enquire') === -1) {
	        return {
	            topic: null,
	            step: null
	        };
	    }

	    var parts = hash.replace(/^#enquire\/*\/*/, '').split('/');

	    return {
	        topic: parts.length && parts[0],
	        step: parts.length > 1 && parts[1]
	    };
	};

	/**
	 * Sets url
	 * @param {Object} params
	 */
	var setRoute = function setRoute() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

	    var hash = window.location.hash;
	    if (params.topic) {
	        hash = '#enquire/' + params.topic;
	    }
	    if (params.step) {
	        hash += '/' + params.step;
	    }

	    if (window.ga && hash !== window.location.hash) {
	        window.ga('send', 'pageview', hash);
	    }

	    window.location.hash = hash;
	};

	/**
	 * Reads form state from location hasg and navigates to form state
	 */
	var navigateToState = function navigateToState() {
	    var route = getRoute();

	    menu.stop();

	    if (!route.topic) {
	        currentTopic = '';
	        $form.find('.topics').removeClass('is-hidden');
	        $formSteps.addClass('is-hidden');
	        $('.feedback-email').removeClass('is-hidden');
	        return $formSteps.html('');
	    }

	    // Go to topics
	    if (route.topic === 'topics') {
	        currentTopic = '';
	        $form.find('.topics').removeClass('is-hidden');
	        $form.find('.close').removeClass('is-hidden');
	        $formSteps.addClass('is-hidden');
	        $('.feedback-email').removeClass('is-hidden');
	        return $formSteps.html('');
	    }

	    if (route.topic && !route.step) {
	        setRoute({ topic: route.topic, step: 'step1' });
	    }

	    if (route.topic && topics[route.topic]) {
	        $form.find('.topics').addClass('is-hidden');
	        $form.find('.close').removeClass('is-hidden');
	        $formSteps.removeClass('is-hidden');
	        $('.feedback-email').addClass('is-hidden');

	        return selectTopic(route.topic, route.step);
	    }

	    setRoute();
	};

	var toggleDisabled = function toggleDisabled(e) {
	    // If all required fields are filled in, enable submit button
	    var $step = e ? $(e.target).parents('.form-step') : $form.find('.form-step.active');

	    var $inputs = $step.find('input, textarea');
	    var $radio = $step.find('input[type=radio]');
	    var $button = $step.find('.form-action-primary');

	    var emptyInputs = $inputs.filter(function (index, input) {
	        return !$(input).val();
	    });

	    var radioNotChecked = $radio.length && !$step.find('input[type=radio]:checked').length;

	    if (emptyInputs.length || radioNotChecked) {
	        $button.addClass('is-disabled');
	    } else {
	        $button.removeClass('is-disabled');
	    }
	};

	var validateInput = function validateInput($input) {
	    var value = $.trim($input.val());
	    var urlPattern = /^((https?|ftp):\/\/)?[^\s/$.?#]+\.[^\s]+$/i;
	    var emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

	    // Checks if input value is non-empty
	    var listener = function listener() {
	        var $input = $(this);
	        var value = $input.val();
	        if (value && $.trim(value).length > 0) {
	            $input.closest('.form-input').removeClass('is-invalid');
	            $input.off('keyup.validator', listener);
	            $input.off('change.validator', listener);
	        }
	    };

	    var callback = function callback() {
	        $input.closest('.form-input').addClass('is-invalid');
	        $input.on('keyup.validator', listener);
	        $input.on('change.validator', listener);
	    };
	    var isValidUrl = !$input.is('[type=url]') || urlPattern.test(value);
	    var isValidEmail = !$input.is('[type=email]') || emailPattern.test(value);

	    if (!value || !isValidUrl || !isValidEmail) {
	        throw new ValidationException(callback, $input);
	    }
	};

	var validateRadioGroup = function validateRadioGroup($group) {
	    var listener = function listener() {
	        var $group = $(this);
	        if (isRadioGroupChecked($group)) {
	            $group.removeClass('is-invalid');
	            $group.off('click.validator input', listener);
	        }
	    };

	    var callback = function callback() {
	        $group.addClass('is-invalid');
	        $group.on('click.validator input', listener);
	    };

	    if (!isRadioGroupChecked($group)) {
	        throw new ValidationException(callback, $group);
	    }
	};

	var ValidationException = function ValidationException(callback, $input) {
	    this.callback = callback;
	    this.$input = $input;
	};

	var getRequiredInputs = function getRequiredInputs($node) {
	    var $inputs = $(':input:visible', $node);
	    return $inputs.not('[type=radio]').not('[type=checkbox]');
	};

	var getRequiredRadioGroups = function getRequiredRadioGroups($node) {
	    return $node.find('.form-radio-group');
	};

	var isRadioGroupChecked = function isRadioGroupChecked($group) {
	    return $group.find('input[type=radio]:checked').length > 0;
	};

	/**
	 * Validates form inputs
	 * @returns {boolean}
	 */
	var validate = function validate(e) {
	    // Select all required inputs that are currently visible
	    var isValid = true;
	    var $step = $(e.target).closest('.form-step');
	    var $inputs = getRequiredInputs($step);
	    var $radioGroups = getRequiredRadioGroups($step);

	    $inputs.add($radioGroups).each(function () {
	        var $element = $(this);
	        try {
	            if ($element.is('.form-radio-group')) {
	                validateRadioGroup($element);
	            } else {
	                validateInput($element);
	            }
	        } catch (e) {
	            if (!(e instanceof ValidationException)) {
	                throw e;
	            }
	            e.callback();
	            isValid = false;
	        }
	    });

	    return isValid;
	};

	module.exports = function () {
	    var route = getRoute();

	    if (route.step) {
	        setRoute({ topic: route.topic });
	    } else {
	        navigateToState();
	    }

	    $(window).on('hashchange', function () {
	        navigateToState();
	    });

	    $submitControl.on('click', '.send', function (e) {
	        e.preventDefault();
	        $form.trigger('submit');
	    });

	    $('.form-open').on('click', function (e) {
	        ga('send', 'event', 'contact-form', 'open');
	        e.preventDefault();
	        setRoute({ topic: 'topics' });
	    });

	    $form.on('click', '.close', function (e) {
	        ga('send', 'event', 'contact-form', 'close');
	        e.preventDefault();
	        setRoute({ topic: 'topics' });
	    });

	    $form.on('click', '.js-close', function (e) {
	        ga('send', 'event', 'contact-form', 'close');
	        e.preventDefault();
	        setRoute({ topic: 'topics' });
	    });

	    $form.on('click', '.js-back', function (e) {
	        ga('send', 'event', 'contact-form', 'back');
	        e.preventDefault();
	        var $button = $(e.currentTarget);
	        var goto = $button.attr('data-goto');

	        if (!goto) {
	            setRoute({ topic: 'topics' });
	        } else {
	            setRoute({ topic: currentTopic, step: goto });
	        }
	    });

	    $form.on('click', '.js-next', function (e) {
	        ga('send', 'event', 'contact-form', 'next');
	        var $button = $(e.currentTarget);
	        var goto = $button.attr('data-goto');
	        e.preventDefault();

	        if (!validate(e)) {
	            return;
	        }

	        if ($button.is('.is-disabled')) {
	            return;
	        }

	        if (!goto) {
	            setRoute({ topic: 'topics' });
	        } else {
	            setRoute({ topic: currentTopic, step: goto });
	        }
	    });

	    $form.on('click', '.form-topic', function (e) {
	        var $link = $(e.currentTarget);
	        var hash = $link.attr('href');

	        if (window.ga && hash !== '#') {
	            window.ga('send', 'pageview', hash);
	        }
	    });

	    $cvInput.change(function (e) {
	        var fileName = e.target.value.split('\\').pop();

	        if (fileName !== undefined) {
	            $("label[for='cv']").text(fileName);
	        }
	    });

	    $form.on('click', '.cv-submit', function (e) {
	        ga('send', 'event', 'cv-form', 'submit');
	        $('.form-error').hide(); // Reset

	        var cv = $("input[type=file][name='cv']").prop('files')[0];
	        var name = $form.find('input[name=name]').val();
	        var email = $form.find('input[name=email]').val();
	        var text = $form.find('textarea[name=text]').val();

	        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	        var emailCheck = re.test(email);
	        var nameCheck = name.length > 0;
	        var textCheck = text.length > 0;
	        var fileCheck = cv !== undefined;

	        e.preventDefault();

	        if (!nameCheck || !emailCheck || !textCheck || !fileCheck) {
	            var errorMessage = '';

	            if (!emailCheck) {
	                errorMessage = 'Please check your name.';
	            } else if (!emailCheck) {
	                errorMessage = 'Please check your email address.';
	            } else if (!textCheck) {
	                errorMessage = 'What not tell us a bit about you?';
	            } else if (!fileCheck) {
	                errorMessage = 'Please check your attachment. File format must be pdf or docx.';
	            }

	            $('.form-error').text(errorMessage);
	            $('.form-error').show();
	        } else {
	            $("label[for='cv']").hide();
	            $('input').prop('disabled', true);
	            $('textarea').prop('disabled', true);
	            $('.cv-submit').addClass('loading');
	            $('.cv-submit').text('Uploading');
	            $('.cv-submit').prop('disabled', true);
	            console.log(name, email, text, cv);

	            var data = new FormData();
	            data.append('file', cv);
	            data.append('name', 'cv');

	            $.ajax({
	                type: 'POST',
	                url: 'https://cors-anywhere.herokuapp.com/https://arrival.com/files/upload',
	                processData: false,
	                contentType: false,
	                data: data,
	                success: function success(data, _, xhr) {
	                    if (xhr.status === 200) {
	                        console.log('OK!');
	                        var filePath = data.path;
	                        $.ajax({
	                            type: 'POST',
	                            url: 'https://cors-anywhere.herokuapp.com/https://arrival.com/mailgun/jobs',
	                            processData: false,
	                            dataType: 'json',
	                            headers: { "content-type": "application/json" },
	                            data: (0, _stringify2.default)({
	                                name: name,
	                                email: email,
	                                text: text,
	                                filePath: filePath
	                            }),
	                            success: function success(data, _, xhr) {
	                                if (xhr.status === 200) {
	                                    $form.trigger('ok');
	                                    $form.garlic('destroy');
	                                    $('.form-input').hide();
	                                    $('.form-input-textarea').hide();
	                                    $('.cv-submit').hide();
	                                    $('.form-subheader').hide();
	                                    $('.form-subheader-success').show();
	                                } else {
	                                    $form.trigger('error');
	                                }
	                            },
	                            error: function error() {
	                                $form.trigger('error');
	                                $('input').prop('disabled', false);
	                                $('textarea').prop('disabled', false);
	                                $('.cv-submit').prop('disabled', false);
	                                $("label[for='cv']").show();
	                                $('.cv-submit').removeClass('loading');
	                                $('.cv-submit').text('Send');
	                            }
	                        });
	                    }
	                },
	                error: function error() {
	                    $form.trigger('error');
	                    $('input').prop('disabled', false);
	                    $('textarea').prop('disabled', false);
	                    $('.cv-submit').prop('disabled', false);
	                    $("label[for='cv']").show();
	                    $('.cv-submit').removeClass('loading');
	                    $('.cv-submit').text('Send');
	                }
	            });
	        }
	    });

	    $form.on('click', '.js-submit', function (e) {
	        ga('send', 'event', 'contact-form', 'submit');

	        var name = $form.find('input[name=name]').val();

	        var headers = {
	            "content-type": "application/json"
	        };

	        var $button = $(e.currentTarget);

	        e.preventDefault();

	        if (!validate(e)) {
	            return;
	        }

	        if ($button.is('.is-disabled')) {
	            return;
	        }

	        if ($('[name=captcha]', $form).val()) {
	            headers['x-captcha'] = $('[name=captcha]', $form).val();
	        }

	        if ($('[name=captcha-code]', $form).val()) {
	            headers['x-captcha-code'] = $('[name=captcha-code]', $form).val();
	        }

	        if (window._checkCode_) {
	            headers["x-check-code"] = window._checkCode_;
	        }

	        $form.addClass('loading');

	        $.ajax({
	            type: 'POST',
	            url: '/subscribe/',
	            dataType: 'json',
	            headers: headers,
	            data: (0, _stringify2.default)(serialize()),
	            success: function success(data) {
	                if (data.status === 'success') {
	                    $form.trigger('ok');
	                    $form.garlic('destroy');
	                } else if (data.status === 'captcha') {
	                    ga('send', 'event', 'contact-form', 'captcha');
	                    $form.trigger('captcha', data.data);
	                } else if (data.error && data.error === "Email has already been taken") {
	                    $form.trigger('captcha');
	                } else if (data.data && data.data.title === "Member Exists") {
	                    $form.trigger('ok');
	                    $form.garlic('destroy');
	                } else {
	                    $form.trigger('error');
	                }
	            },
	            error: function error() {
	                $form.trigger('error');
	            }
	        });
	    });

	    $form.on('change keyup', ':input', toggleDisabled);

	    $form.on('ok', function () {
	        ga('send', 'event', 'contact-form', 'successful');
	        $form.removeClass('loading');
	        if (wasCaptcha) {
	            setRoute({ topic: currentTopic, step: 'captcha-done' });
	        } else {
	            setRoute({ topic: currentTopic, step: 'done' });
	        }

	        wasCaptcha = false;
	    });

	    $form.on('error', function () {
	        ga('send', 'event', 'contact-form', 'error');
	        $form.removeClass('loading');
	        setRoute({ topic: currentTopic, step: 'error' });
	        wasCaptcha = false;
	    });

	    $form.on('captcha', function (e, data) {
	        wasCaptcha = true;
	        ga('send', 'event', 'contact-form', 'error');
	        $form.removeClass('loading');
	        $form.find('.js-captcha-code').val(data.code);

	        var $label = $form.find('.js-captcha .form-input-label');
	        $label.html('Hi, it\u2019s Roborace AI speaking.<br />I&nbsp;believe&nbsp;you&nbsp;are one of us. Aren\u2019t&nbsp;you?<br /><br />%%captcha%% is:'.replace('%%captcha%%', data.question));
	        setRoute({ topic: currentTopic, step: 'captcha' });
	    });
	};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(4), __esModule: true };

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	var core = __webpack_require__(5);
	var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
	module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
	  return $JSON.stringify.apply($JSON, arguments);
	};


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	var core = module.exports = { version: '2.5.7' };
	if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _defineProperties = __webpack_require__(7);

	var _defineProperties2 = _interopRequireDefault(_defineProperties);

	var _freeze = __webpack_require__(41);

	var _freeze2 = _interopRequireDefault(_freeze);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (strings, raw) {
	  return (0, _freeze2.default)((0, _defineProperties2.default)(strings, {
	    raw: {
	      value: (0, _freeze2.default)(raw)
	    }
	  }));
	};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(8), __esModule: true };

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(9);
	var $Object = __webpack_require__(5).Object;
	module.exports = function defineProperties(T, D) {
	  return $Object.defineProperties(T, D);
	};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	var $export = __webpack_require__(10);
	// 19.1.2.3 / 15.2.3.7 Object.defineProperties(O, Properties)
	$export($export.S + $export.F * !__webpack_require__(19), 'Object', { defineProperties: __webpack_require__(25) });


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	var global = __webpack_require__(11);
	var core = __webpack_require__(5);
	var ctx = __webpack_require__(12);
	var hide = __webpack_require__(14);
	var has = __webpack_require__(24);
	var PROTOTYPE = 'prototype';

	var $export = function (type, name, source) {
	  var IS_FORCED = type & $export.F;
	  var IS_GLOBAL = type & $export.G;
	  var IS_STATIC = type & $export.S;
	  var IS_PROTO = type & $export.P;
	  var IS_BIND = type & $export.B;
	  var IS_WRAP = type & $export.W;
	  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
	  var expProto = exports[PROTOTYPE];
	  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
	  var key, own, out;
	  if (IS_GLOBAL) source = name;
	  for (key in source) {
	    // contains in native
	    own = !IS_FORCED && target && target[key] !== undefined;
	    if (own && has(exports, key)) continue;
	    // export native or passed
	    out = own ? target[key] : source[key];
	    // prevent global pollution for namespaces
	    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
	    // bind timers to global for call from export context
	    : IS_BIND && own ? ctx(out, global)
	    // wrap global constructors for prevent change them in library
	    : IS_WRAP && target[key] == out ? (function (C) {
	      var F = function (a, b, c) {
	        if (this instanceof C) {
	          switch (arguments.length) {
	            case 0: return new C();
	            case 1: return new C(a);
	            case 2: return new C(a, b);
	          } return new C(a, b, c);
	        } return C.apply(this, arguments);
	      };
	      F[PROTOTYPE] = C[PROTOTYPE];
	      return F;
	    // make static versions for prototype methods
	    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
	    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
	    if (IS_PROTO) {
	      (exports.virtual || (exports.virtual = {}))[key] = out;
	      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
	      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
	    }
	  }
	};
	// type bitmap
	$export.F = 1;   // forced
	$export.G = 2;   // global
	$export.S = 4;   // static
	$export.P = 8;   // proto
	$export.B = 16;  // bind
	$export.W = 32;  // wrap
	$export.U = 64;  // safe
	$export.R = 128; // real proto method for `library`
	module.exports = $export;


/***/ }),
/* 11 */
/***/ (function(module, exports) {

	// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
	var global = module.exports = typeof window != 'undefined' && window.Math == Math
	  ? window : typeof self != 'undefined' && self.Math == Math ? self
	  // eslint-disable-next-line no-new-func
	  : Function('return this')();
	if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	// optional / simple context binding
	var aFunction = __webpack_require__(13);
	module.exports = function (fn, that, length) {
	  aFunction(fn);
	  if (that === undefined) return fn;
	  switch (length) {
	    case 1: return function (a) {
	      return fn.call(that, a);
	    };
	    case 2: return function (a, b) {
	      return fn.call(that, a, b);
	    };
	    case 3: return function (a, b, c) {
	      return fn.call(that, a, b, c);
	    };
	  }
	  return function (/* ...args */) {
	    return fn.apply(that, arguments);
	  };
	};


/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = function (it) {
	  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
	  return it;
	};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	var dP = __webpack_require__(15);
	var createDesc = __webpack_require__(23);
	module.exports = __webpack_require__(19) ? function (object, key, value) {
	  return dP.f(object, key, createDesc(1, value));
	} : function (object, key, value) {
	  object[key] = value;
	  return object;
	};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	var anObject = __webpack_require__(16);
	var IE8_DOM_DEFINE = __webpack_require__(18);
	var toPrimitive = __webpack_require__(22);
	var dP = Object.defineProperty;

	exports.f = __webpack_require__(19) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
	  anObject(O);
	  P = toPrimitive(P, true);
	  anObject(Attributes);
	  if (IE8_DOM_DEFINE) try {
	    return dP(O, P, Attributes);
	  } catch (e) { /* empty */ }
	  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
	  if ('value' in Attributes) O[P] = Attributes.value;
	  return O;
	};


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(17);
	module.exports = function (it) {
	  if (!isObject(it)) throw TypeError(it + ' is not an object!');
	  return it;
	};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

	module.exports = function (it) {
	  return typeof it === 'object' ? it !== null : typeof it === 'function';
	};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = !__webpack_require__(19) && !__webpack_require__(20)(function () {
	  return Object.defineProperty(__webpack_require__(21)('div'), 'a', { get: function () { return 7; } }).a != 7;
	});


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

	// Thank's IE8 for his funny defineProperty
	module.exports = !__webpack_require__(20)(function () {
	  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
	});


/***/ }),
/* 20 */
/***/ (function(module, exports) {

	module.exports = function (exec) {
	  try {
	    return !!exec();
	  } catch (e) {
	    return true;
	  }
	};


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(17);
	var document = __webpack_require__(11).document;
	// typeof document.createElement is 'object' in old IE
	var is = isObject(document) && isObject(document.createElement);
	module.exports = function (it) {
	  return is ? document.createElement(it) : {};
	};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.1.1 ToPrimitive(input [, PreferredType])
	var isObject = __webpack_require__(17);
	// instead of the ES6 spec version, we didn't implement @@toPrimitive case
	// and the second argument - flag - preferred type is a string
	module.exports = function (it, S) {
	  if (!isObject(it)) return it;
	  var fn, val;
	  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
	  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
	  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
	  throw TypeError("Can't convert object to primitive value");
	};


/***/ }),
/* 23 */
/***/ (function(module, exports) {

	module.exports = function (bitmap, value) {
	  return {
	    enumerable: !(bitmap & 1),
	    configurable: !(bitmap & 2),
	    writable: !(bitmap & 4),
	    value: value
	  };
	};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

	var hasOwnProperty = {}.hasOwnProperty;
	module.exports = function (it, key) {
	  return hasOwnProperty.call(it, key);
	};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	var dP = __webpack_require__(15);
	var anObject = __webpack_require__(16);
	var getKeys = __webpack_require__(26);

	module.exports = __webpack_require__(19) ? Object.defineProperties : function defineProperties(O, Properties) {
	  anObject(O);
	  var keys = getKeys(Properties);
	  var length = keys.length;
	  var i = 0;
	  var P;
	  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
	  return O;
	};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.14 / 15.2.3.14 Object.keys(O)
	var $keys = __webpack_require__(27);
	var enumBugKeys = __webpack_require__(40);

	module.exports = Object.keys || function keys(O) {
	  return $keys(O, enumBugKeys);
	};


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

	var has = __webpack_require__(24);
	var toIObject = __webpack_require__(28);
	var arrayIndexOf = __webpack_require__(32)(false);
	var IE_PROTO = __webpack_require__(36)('IE_PROTO');

	module.exports = function (object, names) {
	  var O = toIObject(object);
	  var i = 0;
	  var result = [];
	  var key;
	  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
	  // Don't enum bug & hidden keys
	  while (names.length > i) if (has(O, key = names[i++])) {
	    ~arrayIndexOf(result, key) || result.push(key);
	  }
	  return result;
	};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

	// to indexed object, toObject with fallback for non-array-like ES3 strings
	var IObject = __webpack_require__(29);
	var defined = __webpack_require__(31);
	module.exports = function (it) {
	  return IObject(defined(it));
	};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	// fallback for non-array-like ES3 and non-enumerable old V8 strings
	var cof = __webpack_require__(30);
	// eslint-disable-next-line no-prototype-builtins
	module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
	  return cof(it) == 'String' ? it.split('') : Object(it);
	};


/***/ }),
/* 30 */
/***/ (function(module, exports) {

	var toString = {}.toString;

	module.exports = function (it) {
	  return toString.call(it).slice(8, -1);
	};


/***/ }),
/* 31 */
/***/ (function(module, exports) {

	// 7.2.1 RequireObjectCoercible(argument)
	module.exports = function (it) {
	  if (it == undefined) throw TypeError("Can't call method on  " + it);
	  return it;
	};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	// false -> Array#indexOf
	// true  -> Array#includes
	var toIObject = __webpack_require__(28);
	var toLength = __webpack_require__(33);
	var toAbsoluteIndex = __webpack_require__(35);
	module.exports = function (IS_INCLUDES) {
	  return function ($this, el, fromIndex) {
	    var O = toIObject($this);
	    var length = toLength(O.length);
	    var index = toAbsoluteIndex(fromIndex, length);
	    var value;
	    // Array#includes uses SameValueZero equality algorithm
	    // eslint-disable-next-line no-self-compare
	    if (IS_INCLUDES && el != el) while (length > index) {
	      value = O[index++];
	      // eslint-disable-next-line no-self-compare
	      if (value != value) return true;
	    // Array#indexOf ignores holes, Array#includes - not
	    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
	      if (O[index] === el) return IS_INCLUDES || index || 0;
	    } return !IS_INCLUDES && -1;
	  };
	};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.1.15 ToLength
	var toInteger = __webpack_require__(34);
	var min = Math.min;
	module.exports = function (it) {
	  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
	};


/***/ }),
/* 34 */
/***/ (function(module, exports) {

	// 7.1.4 ToInteger
	var ceil = Math.ceil;
	var floor = Math.floor;
	module.exports = function (it) {
	  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
	};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

	var toInteger = __webpack_require__(34);
	var max = Math.max;
	var min = Math.min;
	module.exports = function (index, length) {
	  index = toInteger(index);
	  return index < 0 ? max(index + length, 0) : min(index, length);
	};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	var shared = __webpack_require__(37)('keys');
	var uid = __webpack_require__(39);
	module.exports = function (key) {
	  return shared[key] || (shared[key] = uid(key));
	};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	var core = __webpack_require__(5);
	var global = __webpack_require__(11);
	var SHARED = '__core-js_shared__';
	var store = global[SHARED] || (global[SHARED] = {});

	(module.exports = function (key, value) {
	  return store[key] || (store[key] = value !== undefined ? value : {});
	})('versions', []).push({
	  version: core.version,
	  mode: __webpack_require__(38) ? 'pure' : 'global',
	  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
	});


/***/ }),
/* 38 */
/***/ (function(module, exports) {

	module.exports = true;


/***/ }),
/* 39 */
/***/ (function(module, exports) {

	var id = 0;
	var px = Math.random();
	module.exports = function (key) {
	  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
	};


/***/ }),
/* 40 */
/***/ (function(module, exports) {

	// IE 8- don't enum bug keys
	module.exports = (
	  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
	).split(',');


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(42), __esModule: true };

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(43);
	module.exports = __webpack_require__(5).Object.freeze;


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.5 Object.freeze(O)
	var isObject = __webpack_require__(17);
	var meta = __webpack_require__(44).onFreeze;

	__webpack_require__(45)('freeze', function ($freeze) {
	  return function freeze(it) {
	    return $freeze && isObject(it) ? $freeze(meta(it)) : it;
	  };
	});


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	var META = __webpack_require__(39)('meta');
	var isObject = __webpack_require__(17);
	var has = __webpack_require__(24);
	var setDesc = __webpack_require__(15).f;
	var id = 0;
	var isExtensible = Object.isExtensible || function () {
	  return true;
	};
	var FREEZE = !__webpack_require__(20)(function () {
	  return isExtensible(Object.preventExtensions({}));
	});
	var setMeta = function (it) {
	  setDesc(it, META, { value: {
	    i: 'O' + ++id, // object ID
	    w: {}          // weak collections IDs
	  } });
	};
	var fastKey = function (it, create) {
	  // return primitive with prefix
	  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
	  if (!has(it, META)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return 'F';
	    // not necessary to add metadata
	    if (!create) return 'E';
	    // add missing metadata
	    setMeta(it);
	  // return object ID
	  } return it[META].i;
	};
	var getWeak = function (it, create) {
	  if (!has(it, META)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return true;
	    // not necessary to add metadata
	    if (!create) return false;
	    // add missing metadata
	    setMeta(it);
	  // return hash weak collections IDs
	  } return it[META].w;
	};
	// add metadata on freeze-family methods calling
	var onFreeze = function (it) {
	  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
	  return it;
	};
	var meta = module.exports = {
	  KEY: META,
	  NEED: false,
	  fastKey: fastKey,
	  getWeak: getWeak,
	  onFreeze: onFreeze
	};


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	// most Object methods by ES6 should accept primitives
	var $export = __webpack_require__(10);
	var core = __webpack_require__(5);
	var fails = __webpack_require__(20);
	module.exports = function (KEY, exec) {
	  var fn = (core.Object || {})[KEY] || Object[KEY];
	  var exp = {};
	  exp[KEY] = exec(fn);
	  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
	};


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	module.exports = {
	    html: function html(literalSections) {
	        // Use raw literal sections: we don’t want
	        // backslashes (\n etc.) to be interpreted
	        var raw = literalSections.raw;

	        var result = '';

	        for (var _len = arguments.length, substs = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	            substs[_key - 1] = arguments[_key];
	        }

	        substs.forEach(function (subst, i) {
	            // Retrieve the literal section preceding
	            // the current substitution
	            var lit = raw[i];

	            // In the example, map() returns an array:
	            // If substitution is an array (and not a string),
	            // we turn it into a string
	            if (Array.isArray(subst)) {
	                subst = subst.join('');
	            }

	            result += lit;
	            result += subst;
	        });
	        // Take care of last literal section
	        // (Never fails, because an empty template string
	        // produces one literal section, an empty string)
	        result += raw[raw.length - 1]; // (A)

	        return result;
	    },

	    getRandomArbitrary: function getRandomArbitrary(min, max) {
	        return Math.floor(Math.random() * (max - min) + min);
	    },

	    debounce: function debounce(func, wait, immediate) {
	        var timeout = void 0;
	        return function () {
	            var context = this,
	                args = arguments;
	            var later = function later() {
	                timeout = null;
	                if (!immediate) func.apply(context, args);
	            };
	            var callNow = immediate && !timeout;
	            clearTimeout(timeout);
	            timeout = setTimeout(later, wait);
	            if (callNow) func.apply(context, args);
	        };
	    },
	    /**
	     * Create a custom context for ejs-loader
	     * @param {String} path - path to a template relative to '/tmpl' directory
	     * @param {Object} data - data to inject into a template
	     * @returns {String} - rendered html
	     */
	    renderTmpl: function renderTmpl(path, data) {
	        var req = __webpack_require__(47);
	        var html = req(path)(data);
	        return html;
	    }
	};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

	var map = {
		"./common/counters.ejs": 48,
		"./common/footer.ejs": 49,
		"./common/head-social.ejs": 50,
		"./common/head.ejs": 51,
		"./common/socialTmpl.ejs": 52,
		"./form/buttonsTmpl.ejs": 53,
		"./form/closeTmpl.ejs": 54,
		"./form/hiddenTmpl.ejs": 55,
		"./form/radioTmpl.ejs": 56,
		"./form/textAreaTmpl.ejs": 57,
		"./form/textInputTmpl.ejs": 58,
		"./index.ejs": 59,
		"./index/about.ejs": 60,
		"./index/common-section.ejs": 61,
		"./index/content.ejs": 62,
		"./index/enquire.ejs": 63,
		"./index/events.ejs": 64,
		"./index/follow.ejs": 65,
		"./index/joinus.ejs": 66,
		"./index/menu.ejs": 67,
		"./index/partners.ejs": 68,
		"./index/robocar-360.ejs": 69,
		"./index/robocar.ejs": 70,
		"./index/scripts.ejs": 71,
		"./index/video-list.ejs": 72,
		"./legal.ejs": 73,
		"./legal/content.ejs": 74,
		"./media.ejs": 75,
		"./media/content.ejs": 76,
		"./media/logo.ejs": 77,
		"./media/photos.ejs": 78,
		"./media/releases.ejs": 79
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 47;


/***/ }),
/* 48 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "<!-- Metrika -->\n<script>\n(function(d, w, c) {\n       (w[c] = w[c] || []).push(function() {\n           try {\n               w.yaCounter34021130 = new Ya.Metrika({\n                   id: 34021130,\n                   clickmap: true,\n                   trackLinks: true,\n                   accurateTrackBounce: true,\n                   webvisor: true\n               });\n           } catch (e) {\n           }\n       });\n       var n = d.getElementsByTagName(\"script\")[0], s = d.createElement(\"script\"), f = function() {\n           n.parentNode.insertBefore(s, n);\n       };\n       s.type = \"text/javascript\";\n       s.async = true;\n       s.src = \"https://mc.yandex.ru/metrika/watch.js\";\n       if (w.opera == \"[object Opera]\") {\n           d.addEventListener(\"DOMContentLoaded\", f, false);\n       } else {\n           f();\n       }\n   })(document, window, \"yandex_metrika_callbacks\");\n</script>\n<noscript>\n    <div><img src=\"https://mc.yandex.ru/watch/34021130\" style=\"position:absolute; left:-9999px;\" alt=\"\"/></div>\n</noscript>\n<!-- GA -->\n<script>\n    (function(i, s, o, g, r, a, m) {\n        i['GoogleAnalyticsObject'] = r;\n        i[r] = i[r] || function() {\n                    (i[r].q = i[r].q || []).push(arguments)\n                }, i[r].l = 1 * new Date();\n        a = s.createElement(o),\n                m = s.getElementsByTagName(o)[0];\n        a.async = 1;\n        a.src = g;\n        m.parentNode.insertBefore(a, m)\n    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');\n\n    ga('create', 'UA-69551352-1', 'auto');\n    ga('send', 'pageview');\n</script>\n<!-- Facebook Pixel Code -->\n<script>\n    !function(f, b, e, v, n, t, s) {\n        if (f.fbq)return;\n        n = f.fbq = function() {\n            n.callMethod ?\n                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)\n        };\n        if (!f._fbq)f._fbq = n;\n        n.push = n;\n        n.loaded = !0;\n        n.version = '2.0';\n        n.queue = [];\n        t = b.createElement(e);\n        t.async = !0;\n        t.src = v;\n        s = b.getElementsByTagName(e)[0];\n        s.parentNode.insertBefore(t, s)\n    }(window,\n            document, 'script', '//connect.facebook.net/en_US/fbevents.js');\n\n    fbq('init', '1008425669236727');\n    fbq('track', \"PageView\");</script>\n\n\n", __filename = "src/tmpl/common/counters.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append("<!-- Metrika -->\n<script>\n(function(d, w, c) {\n       (w[c] = w[c] || []).push(function() {\n           try {\n               w.yaCounter34021130 = new Ya.Metrika({\n                   id: 34021130,\n                   clickmap: true,\n                   trackLinks: true,\n                   accurateTrackBounce: true,\n                   webvisor: true\n               });\n           } catch (e) {\n           }\n       });\n       var n = d.getElementsByTagName(\"script\")[0], s = d.createElement(\"script\"), f = function() {\n           n.parentNode.insertBefore(s, n);\n       };\n       s.type = \"text/javascript\";\n       s.async = true;\n       s.src = \"https://mc.yandex.ru/metrika/watch.js\";\n       if (w.opera == \"[object Opera]\") {\n           d.addEventListener(\"DOMContentLoaded\", f, false);\n       } else {\n           f();\n       }\n   })(document, window, \"yandex_metrika_callbacks\");\n</script>\n<noscript>\n    <div><img src=\"https://mc.yandex.ru/watch/34021130\" style=\"position:absolute; left:-9999px;\" alt=\"\"/></div>\n</noscript>\n<!-- GA -->\n<script>\n    (function(i, s, o, g, r, a, m) {\n        i['GoogleAnalyticsObject'] = r;\n        i[r] = i[r] || function() {\n                    (i[r].q = i[r].q || []).push(arguments)\n                }, i[r].l = 1 * new Date();\n        a = s.createElement(o),\n                m = s.getElementsByTagName(o)[0];\n        a.async = 1;\n        a.src = g;\n        m.parentNode.insertBefore(a, m)\n    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');\n\n    ga('create', 'UA-69551352-1', 'auto');\n    ga('send', 'pageview');\n</script>\n<!-- Facebook Pixel Code -->\n<script>\n    !function(f, b, e, v, n, t, s) {\n        if (f.fbq)return;\n        n = f.fbq = function() {\n            n.callMethod ?\n                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)\n        };\n        if (!f._fbq)f._fbq = n;\n        n.push = n;\n        n.loaded = !0;\n        n.version = '2.0';\n        n.queue = [];\n        t = b.createElement(e);\n        t.async = !0;\n        t.src = v;\n        s = b.getElementsByTagName(e)[0];\n        s.parentNode.insertBefore(t, s)\n    }(window,\n            document, 'script', '//connect.facebook.net/en_US/fbevents.js');\n\n    fbq('init', '1008425669236727');\n    fbq('track', \"PageView\");</script>\n\n\n");
	            __line = 74;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 49 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<%\nvar today = new Date();\nvar year = today.getFullYear();\n%>\n\n<div class="slide slide_footer">\n    <div class="slide-i">\n        <div class="row">\n            <div class="col col-footer-row">\n                <div>© <%= year %> Roborace</div>\n                <div><a href="mailto:hello@roborace.com">Media Enquiries</a></div>\n            </div>\n            <% if(page === \'index\') { %>\n            <div class="col"><a target="_blank" href="http://kinetik.vc/"><img class="kinetik"\n                            src="i/kinetik-logo-white.svg"></a>\n            </div>\n                <div class="col"><a href="/legal/">Legal & Privacy</a></div>\n            <% } %>\n        </div>\n    </div>\n</div>', __filename = "src/tmpl/common/footer.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            var today = new Date();
	            var year = today.getFullYear();
	            __line = 4;
	            __append('\n\n<div class="slide slide_footer">\n    <div class="slide-i">\n        <div class="row">\n            <div class="col col-footer-row">\n                <div>© ');
	            __line = 10;
	            __append(escapeFn(year));
	            __append(' Roborace</div>\n                <div><a href="mailto:hello@roborace.com">Media Enquiries</a></div>\n            </div>\n            ');
	            __line = 13;
	            if (page === "index") {
	                __append('\n            <div class="col"><a target="_blank" href="http://kinetik.vc/"><img class="kinetik"\n                            src="i/kinetik-logo-white.svg"></a>\n            </div>\n                <div class="col"><a href="/legal/">Legal & Privacy</a></div>\n            ');
	                __line = 18;
	            }
	            __append("\n        </div>\n    </div>\n</div>");
	            __line = 21;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 50 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<meta property="fb:app_id" content="193643317641668"/>\n<meta property="og:type" content="website"/>\n<meta property="og:title" content="<%- title %>"/>\n<meta property="og:description" content="<%- description %>"/>\n<meta property="og:image" content="/static/i/preview.jpg"/>\n<meta property="og:image:width" content="1200"/>\n<meta property="og:image:height" content="675"/>\n<meta name="twitter:card" content="summary_large_image"/>\n<meta name="twitter:site" content="@roborace"/>\n<meta name="twitter:creator" content="@roborace"/>\n<meta name="twitter:title" content="<%- title %>"/>\n<meta name="twitter:description" content="<%- description %>"/>\n<meta name="twitter:image" content="/static/i/preview.jpg"/>', __filename = "src/tmpl/common/head-social.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<meta property="fb:app_id" content="193643317641668"/>\n<meta property="og:type" content="website"/>\n<meta property="og:title" content="');
	            __line = 3;
	            __append(title);
	            __append('"/>\n<meta property="og:description" content="');
	            __line = 4;
	            __append(description);
	            __append('"/>\n<meta property="og:image" content="/static/i/preview.jpg"/>\n<meta property="og:image:width" content="1200"/>\n<meta property="og:image:height" content="675"/>\n<meta name="twitter:card" content="summary_large_image"/>\n<meta name="twitter:site" content="@roborace"/>\n<meta name="twitter:creator" content="@roborace"/>\n<meta name="twitter:title" content="');
	            __line = 11;
	            __append(title);
	            __append('"/>\n<meta name="twitter:description" content="');
	            __line = 12;
	            __append(description);
	            __append('"/>\n<meta name="twitter:image" content="/static/i/preview.jpg"/>');
	            __line = 13;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 51 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">\n    <meta charset="UTF-8"/>\n    <title>Roborace</title>\n    <% if (page === \'index\') { %>\n        <link rel="stylesheet" href="static/build/all<%- dev ? \'\' : \'.min\' %>.css"/>\n    <% } else if (page === \'media\')  { %>\n        <link rel="stylesheet" href="../static/build/media<%- dev ? \'\' : \'.min\'%>.css"/>\n    <% } else  { %>\n        <link rel="stylesheet" href="../static/build/inner<%- dev ? \'\' : \'.min\' %>.css"/>\n    <% } %>\n    <link rel="manifest" href="/manifest.json"/>\n    <meta name="description" content="<%- description %>" />\n    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>\n    <link rel="shortcut icon" type="image/x-icon" href="static/i/favicon@0,75x.png"/>\n    <link rel="icon" sizes="192x192" href="static/i/favicon@4x.png">\n    <%- include(\'head-social\') %>\n    <meta name="apple-mobile-web-app-capable" content="yes"/>\n    <meta name="google-site-verification" content="PODKZgh1_uJUab0kSoaRxxG227y8GYmhTOxlV7W4dEo" />\n    <meta name="p:domain_verify" content="be7548368a65a5331d001b7924cb0242"/>\n    <link rel="apple-touch-icon" sizes="60x60" href="static/i/touch-icon@60w.png">\n    <link rel="apple-touch-icon" sizes="76x76" href="static/i/touch-icon@76w.png">\n    <link rel="apple-touch-icon" sizes="120x120" href="static/i/touch-icon@120w.png">\n    <link rel="apple-touch-icon" sizes="152x152" href="static/i/touch-icon@152w.png">\n</head>\n', __filename = "src/tmpl/common/head.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">\n    <meta charset="UTF-8"/>\n    <title>Roborace</title>\n    ');
	            __line = 4;
	            if (page === "index") {
	                __append('\n        <link rel="stylesheet" href="static/build/all');
	                __line = 5;
	                __append(dev ? "" : ".min");
	                __append('.css"/>\n    ');
	                __line = 6;
	            } else if (page === "media") {
	                __append('\n        <link rel="stylesheet" href="../static/build/media');
	                __line = 7;
	                __append(dev ? "" : ".min");
	                __append('.css"/>\n    ');
	                __line = 8;
	            } else {
	                __append('\n        <link rel="stylesheet" href="../static/build/inner');
	                __line = 9;
	                __append(dev ? "" : ".min");
	                __append('.css"/>\n    ');
	                __line = 10;
	            }
	            __append('\n    <link rel="manifest" href="/manifest.json"/>\n    <meta name="description" content="');
	            __line = 12;
	            __append(description);
	            __append('" />\n    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>\n    <link rel="shortcut icon" type="image/x-icon" href="static/i/favicon@0,75x.png"/>\n    <link rel="icon" sizes="192x192" href="static/i/favicon@4x.png">\n    ');
	            __line = 16;
	            __append(include("head-social"));
	            __append('\n    <meta name="apple-mobile-web-app-capable" content="yes"/>\n    <meta name="google-site-verification" content="PODKZgh1_uJUab0kSoaRxxG227y8GYmhTOxlV7W4dEo" />\n    <meta name="p:domain_verify" content="be7548368a65a5331d001b7924cb0242"/>\n    <link rel="apple-touch-icon" sizes="60x60" href="static/i/touch-icon@60w.png">\n    <link rel="apple-touch-icon" sizes="76x76" href="static/i/touch-icon@76w.png">\n    <link rel="apple-touch-icon" sizes="120x120" href="static/i/touch-icon@120w.png">\n    <link rel="apple-touch-icon" sizes="152x152" href="static/i/touch-icon@152w.png">\n</head>\n');
	            __line = 25;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 52 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="social">\n    <% if (data) { %>\n    <div class=\'social-title\'>\n        <%= data.text %>\n    </div>\n    <% } %>\n    <a class="social-link" target="_blank" href="https://facebook.com/roborace">\n        <svg width="259px" height="259px" viewBox="0 0 259 259" version="1.1" xmlns="http://www.w3.org/2000/svg"\n                xmlns:xlink="http://www.w3.org/1999/xlink">\n            <g transform="scale(0.75) translate(40, 40)" id="facebook-logo" stroke="none" stroke-width="1"\n                    fill-rule="evenodd" fill="#FFFFFF">\n                <path d="M244.082,258.307 C251.936,258.307 258.305,251.938 258.305,244.082 L258.305,14.812 C258.305,6.955 251.937,0.588 244.082,0.588 L14.812,0.588 C6.955,0.588 0.588,6.955 0.588,14.812 L0.588,244.082 C0.588,251.937 6.954,258.307 14.812,258.307 L244.082,258.307 Z M178.409,258.307 L178.409,158.504 L211.908,158.504 L216.924,119.609 L178.409,119.609 L178.409,94.777 C178.409,83.516 181.536,75.842 197.684,75.842 L218.28,75.833 L218.28,41.045 C214.718,40.571 202.492,39.512 188.268,39.512 C158.573,39.512 138.243,57.638 138.243,90.925 L138.243,119.609 L104.658,119.609 L104.658,158.504 L138.243,158.504 L138.243,258.307 L178.409,258.307 Z"></path>\n            </g>\n        </svg>\n    </a><a class="social-link" target="_blank" href="http://twitter.com/roborace">\n        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n                width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"\n                xml:space="preserve" fill="#FFFFFF">\n                            <path id="twitter-logo" d="M492,109.5c-17.4,7.7-36,12.9-55.6,15.3c20-12,35.4-31,42.6-53.6c-18.7,11.1-39.4,19.2-61.5,23.5\n                                C399.8,75.8,374.6,64,346.8,64c-53.5,0-96.8,43.4-96.8,96.9c0,7.6,0.8,15,2.5,22.1c-80.5-4-151.9-42.6-199.6-101.3\n                                c-8.3,14.3-13.1,31-13.1,48.7c0,33.6,17.2,63.3,43.2,80.7C67,210.7,52,206.3,39,199c0,0.4,0,0.8,0,1.2c0,47,33.4,86.1,77.7,95\n                                c-8.1,2.2-16.7,3.4-25.5,3.4c-6.2,0-12.3-0.6-18.2-1.8c12.3,38.5,48.1,66.5,90.5,67.3c-33.1,26-74.9,41.5-120.3,41.5\n                                c-7.8,0-15.5-0.5-23.1-1.4C62.8,432,113.7,448,168.3,448C346.6,448,444,300.3,444,172.2c0-4.2-0.1-8.4-0.3-12.5\n                                C462.6,146,479,129,492,109.5z"/>\n                        </svg>\n    </a><a class="social-link" target="_blank" href="https://www.instagram.com/roborace">\n        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n                width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512"\n                xml:space="preserve" fill="#FFFFFF">\n                    <path id="instagram-logo" d="M448.5,112c0-26.233-21.267-47.5-47.5-47.5H112c-26.233,0-47.5,21.267-47.5,47.5v289\n                    \tc0,26.233,21.267,47.5,47.5,47.5h289c26.233,0,47.5-21.267,47.5-47.5V112z M257,175.833c44.182,0,80,35.816,80,80s-35.818,80-80,80\n                    \ts-80-35.816-80-80S212.818,175.833,257,175.833z M416.5,160.5c0,8.836-7.163,16-16,16h-48c-8.837,0-16-7.164-16-16v-48\n                    \tc0-8.836,7.163-16,16-16h48c8.837,0,16,7.164,16,16V160.5z M401.5,416.5h-288c-8.822,0-17-8.178-17-17v-175h53.072\n                    \tc-3.008,10-4.572,20.647-4.572,31.583C145,286,156.65,314,177.805,335.154s49.279,32.741,79.195,32.741s58.041-11.681,79.195-32.835\n                    \tS369,286.016,369,256.099c0-10.936-1.563-21.599-4.572-31.599H416.5v175C416.5,408.322,410.322,416.5,401.5,416.5z"/>\n                    </svg>\n    </a><a class="social-link" target="_blank" href="https://youtube.com/roborace">\n        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"\n                x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512"\n                style="enable-background:new 0 0 512 512;" xml:space="preserve">\n                   \t<path d="M508.6,148.8c0-45-33.1-81.2-74-81.2C379.2,65,322.7,64,265,64c-3,0-6,0-9,0s-6,0-9,0c-57.6,0-114.2,1-169.6,3.6\n                   \t\tc-40.8,0-73.9,36.4-73.9,81.4C1,184.6-0.1,220.2,0,255.8C-0.1,291.4,1,327,3.4,362.7c0,45,33.1,81.5,73.9,81.5\n                   \t\tc58.2,2.7,117.9,3.9,178.6,3.8c60.8,0.2,120.3-1,178.6-3.8c40.9,0,74-36.5,74-81.5c2.4-35.7,3.5-71.3,3.4-107\n                   \t\tC512.1,220.1,511,184.5,508.6,148.8z M207,353.9V157.4l145,98.2L207,353.9z" fill="#FFFFFF"/>\n                   </svg>\n    </a>\n</div>\n        ', __filename = "src/tmpl/common/socialTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="social">\n    ');
	            __line = 2;
	            if (data) {
	                __append("\n    <div class='social-title'>\n        ");
	                __line = 4;
	                __append(escapeFn(data.text));
	                __append("\n    </div>\n    ");
	                __line = 6;
	            }
	            __append('\n    <a class="social-link" target="_blank" href="https://facebook.com/roborace">\n        <svg width="259px" height="259px" viewBox="0 0 259 259" version="1.1" xmlns="http://www.w3.org/2000/svg"\n                xmlns:xlink="http://www.w3.org/1999/xlink">\n            <g transform="scale(0.75) translate(40, 40)" id="facebook-logo" stroke="none" stroke-width="1"\n                    fill-rule="evenodd" fill="#FFFFFF">\n                <path d="M244.082,258.307 C251.936,258.307 258.305,251.938 258.305,244.082 L258.305,14.812 C258.305,6.955 251.937,0.588 244.082,0.588 L14.812,0.588 C6.955,0.588 0.588,6.955 0.588,14.812 L0.588,244.082 C0.588,251.937 6.954,258.307 14.812,258.307 L244.082,258.307 Z M178.409,258.307 L178.409,158.504 L211.908,158.504 L216.924,119.609 L178.409,119.609 L178.409,94.777 C178.409,83.516 181.536,75.842 197.684,75.842 L218.28,75.833 L218.28,41.045 C214.718,40.571 202.492,39.512 188.268,39.512 C158.573,39.512 138.243,57.638 138.243,90.925 L138.243,119.609 L104.658,119.609 L104.658,158.504 L138.243,158.504 L138.243,258.307 L178.409,258.307 Z"></path>\n            </g>\n        </svg>\n    </a><a class="social-link" target="_blank" href="http://twitter.com/roborace">\n        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n                width="512px" height="512px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"\n                xml:space="preserve" fill="#FFFFFF">\n                            <path id="twitter-logo" d="M492,109.5c-17.4,7.7-36,12.9-55.6,15.3c20-12,35.4-31,42.6-53.6c-18.7,11.1-39.4,19.2-61.5,23.5\n                                C399.8,75.8,374.6,64,346.8,64c-53.5,0-96.8,43.4-96.8,96.9c0,7.6,0.8,15,2.5,22.1c-80.5-4-151.9-42.6-199.6-101.3\n                                c-8.3,14.3-13.1,31-13.1,48.7c0,33.6,17.2,63.3,43.2,80.7C67,210.7,52,206.3,39,199c0,0.4,0,0.8,0,1.2c0,47,33.4,86.1,77.7,95\n                                c-8.1,2.2-16.7,3.4-25.5,3.4c-6.2,0-12.3-0.6-18.2-1.8c12.3,38.5,48.1,66.5,90.5,67.3c-33.1,26-74.9,41.5-120.3,41.5\n                                c-7.8,0-15.5-0.5-23.1-1.4C62.8,432,113.7,448,168.3,448C346.6,448,444,300.3,444,172.2c0-4.2-0.1-8.4-0.3-12.5\n                                C462.6,146,479,129,492,109.5z"/>\n                        </svg>\n    </a><a class="social-link" target="_blank" href="https://www.instagram.com/roborace">\n        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n                width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512"\n                xml:space="preserve" fill="#FFFFFF">\n                    <path id="instagram-logo" d="M448.5,112c0-26.233-21.267-47.5-47.5-47.5H112c-26.233,0-47.5,21.267-47.5,47.5v289\n                    \tc0,26.233,21.267,47.5,47.5,47.5h289c26.233,0,47.5-21.267,47.5-47.5V112z M257,175.833c44.182,0,80,35.816,80,80s-35.818,80-80,80\n                    \ts-80-35.816-80-80S212.818,175.833,257,175.833z M416.5,160.5c0,8.836-7.163,16-16,16h-48c-8.837,0-16-7.164-16-16v-48\n                    \tc0-8.836,7.163-16,16-16h48c8.837,0,16,7.164,16,16V160.5z M401.5,416.5h-288c-8.822,0-17-8.178-17-17v-175h53.072\n                    \tc-3.008,10-4.572,20.647-4.572,31.583C145,286,156.65,314,177.805,335.154s49.279,32.741,79.195,32.741s58.041-11.681,79.195-32.835\n                    \tS369,286.016,369,256.099c0-10.936-1.563-21.599-4.572-31.599H416.5v175C416.5,408.322,410.322,416.5,401.5,416.5z"/>\n                    </svg>\n    </a><a class="social-link" target="_blank" href="https://youtube.com/roborace">\n        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"\n                x="0px" y="0px" width="512px" height="512px" viewBox="0 0 512 512"\n                style="enable-background:new 0 0 512 512;" xml:space="preserve">\n                   \t<path d="M508.6,148.8c0-45-33.1-81.2-74-81.2C379.2,65,322.7,64,265,64c-3,0-6,0-9,0s-6,0-9,0c-57.6,0-114.2,1-169.6,3.6\n                   \t\tc-40.8,0-73.9,36.4-73.9,81.4C1,184.6-0.1,220.2,0,255.8C-0.1,291.4,1,327,3.4,362.7c0,45,33.1,81.5,73.9,81.5\n                   \t\tc58.2,2.7,117.9,3.9,178.6,3.8c60.8,0.2,120.3-1,178.6-3.8c40.9,0,74-36.5,74-81.5c2.4-35.7,3.5-71.3,3.4-107\n                   \t\tC512.1,220.1,511,184.5,508.6,148.8z M207,353.9V157.4l145,98.2L207,353.9z" fill="#FFFFFF"/>\n                   </svg>\n    </a>\n</div>\n        ');
	            __line = 48;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 53 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "\n\n<div class=\"form-actions <%= data.class || '' %>\">\n    <% if (data.secondary) { %>\n        <%- '<a ' + (data.secondary.goTo ? 'data-goto=\"' + data.secondary.goTo + '\"' : '') + 'class=\"form-action form-action-secondary ' + (data.secondary.class || 'js-secondary') + '\"></a>' %>\n    <% } %>\n\n    <% if (data.primary) { %>\n        <%- '<a ' + (data.primary.goTo ? 'data-goto=\"' + data.primary.goTo + '\"' : '') + 'class=\"form-action form-action-primary ' + (data.primary.enabled ? 'is-disabled' : '') + (data.primary.class || 'js-primaty') + '\"><?xml version=\"1.0\" encoding=\"utf-8\"?><i>' + data.primary.text + '</i></a>' %>\n    <% } %>\n</div>", __filename = "src/tmpl/form/buttonsTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('\n\n<div class="form-actions ');
	            __line = 3;
	            __append(escapeFn(data.class || ""));
	            __append('">\n    ');
	            __line = 4;
	            if (data.secondary) {
	                __append("\n        ");
	                __line = 5;
	                __append("<a " + (data.secondary.goTo ? 'data-goto="' + data.secondary.goTo + '"' : "") + 'class="form-action form-action-secondary ' + (data.secondary.class || "js-secondary") + '"></a>');
	                __append("\n    ");
	                __line = 6;
	            }
	            __append("\n\n    ");
	            __line = 8;
	            if (data.primary) {
	                __append("\n        ");
	                __line = 9;
	                __append("<a " + (data.primary.goTo ? 'data-goto="' + data.primary.goTo + '"' : "") + 'class="form-action form-action-primary ' + (data.primary.enabled ? "is-disabled" : "") + (data.primary.class || "js-primaty") + '"><?xml version="1.0" encoding="utf-8"?><i>' + data.primary.text + "</i></a>");
	                __append("\n    ");
	                __line = 10;
	            }
	            __append("\n</div>");
	            __line = 11;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 54 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<a href="#" class="close">\n    <svg version="1.1"\n         xmlns="http://www.w3.org/2000/svg"\n         x="0px" y="0px"\n         width="512px" height="512px"\n         viewBox="0 0 512 512">\n        <polygon fill="#FFFFFF"\n                 points="340.2,160 255.8,244.3 171.8,160.4 160,172.2 244,256 160,339.9 171.8,351.6 255.8,267.8 340.2,352 352,340.3 267.6,256 352,171.8 "/>\n    </svg>\n</a>', __filename = "src/tmpl/form/closeTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<a href="#" class="close">\n    <svg version="1.1"\n         xmlns="http://www.w3.org/2000/svg"\n         x="0px" y="0px"\n         width="512px" height="512px"\n         viewBox="0 0 512 512">\n        <polygon fill="#FFFFFF"\n                 points="340.2,160 255.8,244.3 171.8,160.4 160,172.2 244,256 160,339.9 171.8,351.6 255.8,267.8 340.2,352 352,340.3 267.6,256 352,171.8 "/>\n    </svg>\n</a>');
	            __line = 10;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 55 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<input class="form-input-hidden <%= data.class || \'\' %>" type="hidden" name="<%= data.id %>" id="<%= data.id %>"\n        value=""/>', __filename = "src/tmpl/form/hiddenTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<input class="form-input-hidden ');
	            __append(escapeFn(data.class || ""));
	            __append('" type="hidden" name="');
	            __append(escapeFn(data.id));
	            __append('" id="');
	            __append(escapeFn(data.id));
	            __append('"\n        value=""/>');
	            __line = 2;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 56 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "<div class='form-radio-group'>\n    <% if (data.title) { %>\n        <div class='form-radio-label'>\n            <%= data.title  + ' hello'%>\n        </div>\n    <% } %>\n    <% data.items.forEach(function(item, index) { %>\n        <label for='<%= data.id + index %>'\n               class='form-radio-label <%= item.class || \"\"%>'>\n               <input id='<%= data.id + index %>'\n                      data-index='<%= index %>'\n                      type='radio'\n                      name='<%= data.id %>'\n                      value='<%= item.text %>'\n                      class='form-radio'/>\n               <%=  item.text %>\n        </label>\n    <% }) %>\n</div>", __filename = "src/tmpl/form/radioTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append("<div class='form-radio-group'>\n    ");
	            __line = 2;
	            if (data.title) {
	                __append("\n        <div class='form-radio-label'>\n            ");
	                __line = 4;
	                __append(escapeFn(data.title + " hello"));
	                __append("\n        </div>\n    ");
	                __line = 6;
	            }
	            __append("\n    ");
	            __line = 7;
	            data.items.forEach(function(item, index) {
	                __append("\n        <label for='");
	                __line = 8;
	                __append(escapeFn(data.id + index));
	                __append("'\n               class='form-radio-label ");
	                __line = 9;
	                __append(escapeFn(item.class || ""));
	                __append("'>\n               <input id='");
	                __line = 10;
	                __append(escapeFn(data.id + index));
	                __append("'\n                      data-index='");
	                __line = 11;
	                __append(escapeFn(index));
	                __append("'\n                      type='radio'\n                      name='");
	                __line = 13;
	                __append(escapeFn(data.id));
	                __append("'\n                      value='");
	                __line = 14;
	                __append(escapeFn(item.text));
	                __append("'\n                      class='form-radio'/>\n               ");
	                __line = 16;
	                __append(escapeFn(item.text));
	                __append("\n        </label>\n    ");
	                __line = 18;
	            });
	            __append("\n</div>");
	            __line = 19;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 57 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="form-input form-input-textarea <%= data.class || \'\' %>">\n    <label for="<%= data.id %>" class="form-input-label">\n        <%- data.label %>\n    </label>\n    <textarea name="<%= data.id %>" id="<%= data.id %>" ></textarea>\n</div>', __filename = "src/tmpl/form/textAreaTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="form-input form-input-textarea ');
	            __append(escapeFn(data.class || ""));
	            __append('">\n    <label for="');
	            __line = 2;
	            __append(escapeFn(data.id));
	            __append('" class="form-input-label">\n        ');
	            __line = 3;
	            __append(data.label);
	            __append('\n    </label>\n    <textarea name="');
	            __line = 5;
	            __append(escapeFn(data.id));
	            __append('" id="');
	            __append(escapeFn(data.id));
	            __append('" ></textarea>\n</div>');
	            __line = 6;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 58 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="form-input <%= data.class || \'\' %> ">\n    <div class="form-input-i">\n            <label for="<%= data.id %>" class="form-input-label"><%= data.label %></label>\n            <input type="<%= data.type ? data.type : \'text\'%>"\n                   name="<%= data.id %>"\n                   autocomplete="<%= data.autocomplete === true ? \'on\' : \'off\' %>"\n                   autocorrect="<%= data.autocorrect === true ? \'on\' : \'off\' %>" \n                   autocapitalize="<%= data.autocapitalize === true ? \'on\' : \'off\' %>"\n                   placeholder="<%= data.placeholder && data.placeholder !== \'\' ? data.placeholder : \'\' %>"\n                   id="<%= data.id %>"/>\n</div>\n</div>', __filename = "src/tmpl/form/textInputTmpl.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="form-input ');
	            __append(escapeFn(data.class || ""));
	            __append(' ">\n    <div class="form-input-i">\n            <label for="');
	            __line = 3;
	            __append(escapeFn(data.id));
	            __append('" class="form-input-label">');
	            __append(escapeFn(data.label));
	            __append('</label>\n            <input type="');
	            __line = 4;
	            __append(escapeFn(data.type ? data.type : "text"));
	            __append('"\n                   name="');
	            __line = 5;
	            __append(escapeFn(data.id));
	            __append('"\n                   autocomplete="');
	            __line = 6;
	            __append(escapeFn(data.autocomplete === true ? "on" : "off"));
	            __append('"\n                   autocorrect="');
	            __line = 7;
	            __append(escapeFn(data.autocorrect === true ? "on" : "off"));
	            __append('" \n                   autocapitalize="');
	            __line = 8;
	            __append(escapeFn(data.autocapitalize === true ? "on" : "off"));
	            __append('"\n                   placeholder="');
	            __line = 9;
	            __append(escapeFn(data.placeholder && data.placeholder !== "" ? data.placeholder : ""));
	            __append('"\n                   id="');
	            __line = 10;
	            __append(escapeFn(data.id));
	            __append('"/>\n</div>\n</div>');
	            __line = 12;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 59 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "<!DOCTYPE html>\n<html lang=\"en\">\n<%- include('common/head', {page: 'index'}) %>\n<body>\n\n    <%- include('index/content', {page: 'index'}) %>\n</div>\n<%- include('index/scripts') %>\n<%- include('common/counters') %>\n</body>\n</html>", __filename = "src/tmpl/index.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<!DOCTYPE html>\n<html lang="en">\n');
	            __line = 3;
	            __append(include("common/head", {
	                page: "index"
	            }));
	            __append("\n<body>\n\n    ");
	            __line = 6;
	            __append(include("index/content", {
	                page: "index"
	            }));
	            __append("\n</div>\n");
	            __line = 8;
	            __append(include("index/scripts"));
	            __append("\n");
	            __line = 9;
	            __append(include("common/counters"));
	            __append("\n</body>\n</html>");
	            __line = 11;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 60 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="main-logo vertical">\n    <div class="main-logo-m"></div>\n</div>\n<div class="landing">\n    <div class="landing-slide">\n        <div class="landing-title">\n            <%- data.content.title %>\n        </div>\n    </div>\n</div>', __filename = "src/tmpl/index/about.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="main-logo vertical">\n    <div class="main-logo-m"></div>\n</div>\n<div class="landing">\n    <div class="landing-slide">\n        <div class="landing-title">\n            ');
	            __line = 7;
	            __append(data.content.title);
	            __append("\n        </div>\n    </div>\n</div>");
	            __line = 10;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 61 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<% if(data.title) { %>\n<div class="section-title">\n    <%- data.header || data.title %>\n</div>\n<% } if(data.content && data.content.title) { %>\n<div class="section-subtitle">\n    <%- data.content.title %>\n</div>\n<% } if(data.content && data.content.text) { %>\n<div class="section-text">\n    <%- data.content.text %>\n</div>\n<% } %>\n<% if(data.video) { %>\n<div class="section-video">\n    <div data-id="<%- data.video %>" class="section-video-control youtube-video"></div>\n    <% if(data.content.videoText) { %>\n    <div class="section-video-text">\n        <%- data.content.videoText %>\n    </div>\n    <% } %>\n</div>\n<% } %>\n<% if(data.content && data.content.videoList && data.content.videoList.length ) { %>\n<%- include(\'video-list\', { items: data.content.videoList }) %>\n<% } %>', __filename = "src/tmpl/index/common-section.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            if (data.title) {
	                __append('\n<div class="section-title">\n    ');
	                __line = 3;
	                __append(data.header || data.title);
	                __append("\n</div>\n");
	                __line = 5;
	            }
	            if (data.content && data.content.title) {
	                __append('\n<div class="section-subtitle">\n    ');
	                __line = 7;
	                __append(data.content.title);
	                __append("\n</div>\n");
	                __line = 9;
	            }
	            if (data.content && data.content.text) {
	                __append('\n<div class="section-text">\n    ');
	                __line = 11;
	                __append(data.content.text);
	                __append("\n</div>\n");
	                __line = 13;
	            }
	            __append("\n");
	            __line = 14;
	            if (data.video) {
	                __append('\n<div class="section-video">\n    <div data-id="');
	                __line = 16;
	                __append(data.video);
	                __append('" class="section-video-control youtube-video"></div>\n    ');
	                __line = 17;
	                if (data.content.videoText) {
	                    __append('\n    <div class="section-video-text">\n        ');
	                    __line = 19;
	                    __append(data.content.videoText);
	                    __append("\n    </div>\n    ");
	                    __line = 21;
	                }
	                __append("\n</div>\n");
	                __line = 23;
	            }
	            __append("\n");
	            __line = 24;
	            if (data.content && data.content.videoList && data.content.videoList.length) {
	                __append("\n");
	                __line = 25;
	                __append(include("video-list", {
	                    items: data.content.videoList
	                }));
	                __append("\n");
	                __line = 26;
	            }
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 62 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div id="content" class="content">\n    <div class="modal modal-youtube">\n        <div class="modal-bg"></div>\n        <div class="modal-close"></div>\n        <div id="player" class="modal-player"></div>\n    </div>\n    <div class="sections">\n        <% content.forEach(function(section, i){ %>\n        <div id="<%- section.alias %>" class="section section-<%- section.alias %> scrollto">\n            <div class="section-i">\n                <% if(section.imagePreview) { %>\n                <div class="section-image-preview"></div>\n                <% } %>\n                <% if(section.type === \'custom\') { %>\n                <%- include(section.alias, {data: section}) %>\n                <% } else { %>\n                <%- include(\'common-section\', {data: section}) %>\n                <% } %>\n\n                <% if (section.backgroundVideo && section.backgroundVideo.youtube) { %>\n                <div class="section-bg-youtube section-bg-<%- section.alias %>">\n                    <iframe id="youtube_bg_<%- section.alias %>" class="section-video-iframe" type="text/html" width="100%" height="100%"\n                            src="http://www.youtube.com/embed/<%- section.backgroundVideo.youtube %>?version=3&enablejsapi=1&autoplay=1&controls=0&loop=1&rel=0&disablekb=1&iv_load_policy=3&modestbranding=1&showinfo=0&playlist=<%- section.backgroundVideo.youtube %>"\n                            frameborder="0"></iframe>\n                </div>\n                <% } %>\n            </div>\n            <% if(section.pic && typeof section.pic === \'string\'){ %>\n            <div class="section-bg section-bg-inner section-bg-<%- section.alias %>" <%- section.pic ? \'style="background-image: url(/static/i/\' + section.pic + \');"\' : \'\' %>\n                 data-pic="<%- section.pic ? \'/static/i/\' + section.pic : \'\' %>"></div>\n            <% } else if (section.pic) { %>\n            <div class="swipe section-bg section-bg-inner section-bg-<%- section.alias %> <%- section.bgLoopPartner ? \'bg-loop-partner\' : \'bg-loop\' %>">\n                <div class="swipe-wrap">\n                    <% section.pic.forEach(function(picRaw, i){ var pic = picRaw.split(\'@\'); %>\n                    <div class="bg-pic section-bg-pic" <%- pic[0] ? \'style="background-image: url(/static/i/\' + pic[0] + \');"\' : \'\' %>\n                         data-pic="<%- pic[0] ? \'/static/i/\' + pic[0] : \'\' %>"></div>\n                    <% }) %>\n                </div>\n            </div>\n            <a class="swipe-close modal-close"></a>\n            <div class="swipe-controls">\n                <a class="swipe-controls-btn swipe-controls-btn-prev"></a>\n                <a class="swipe-controls-btn swipe-controls-btn-next"></a>\n            </div>\n            <% var colors = []; section.pic.forEach(function (picRaw) {\n                colors.push(picRaw.split(\'@\')[1])\n            }) %>\n            <div class="swipe-points" data-colors=\'<%- JSON.stringify(colors) %>\'>\n                <% section.pic.forEach(function(pic, i){ %>\n                <div class="swipe-point" data-index="<%- i %>"></div>\n                <% }) %>\n            </div>\n            <% } else if (section.backgroundVideo && !section.backgroundVideo.youtube) { %>\n            <div data-background-video=\'<%- JSON.stringify(section.backgroundVideo) %>\'\n                 class="section-bg bg-video section-bg-inner section-bg-<%- section.alias %>"\n                 style="background-image: url(/static/i/<%- section.backgroundVideo.poster %>);"></div>\n            <% } %>\n        </div>\n        <% }) %>\n    </div>\n</div>\n<%- include(\'menu\') %>', __filename = "src/tmpl/index/content.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div id="content" class="content">\n    <div class="modal modal-youtube">\n        <div class="modal-bg"></div>\n        <div class="modal-close"></div>\n        <div id="player" class="modal-player"></div>\n    </div>\n    <div class="sections">\n        ');
	            __line = 8;
	            content.forEach(function(section, i) {
	                __append('\n        <div id="');
	                __line = 9;
	                __append(section.alias);
	                __append('" class="section section-');
	                __append(section.alias);
	                __append(' scrollto">\n            <div class="section-i">\n                ');
	                __line = 11;
	                if (section.imagePreview) {
	                    __append('\n                <div class="section-image-preview"></div>\n                ');
	                    __line = 13;
	                }
	                __append("\n                ");
	                __line = 14;
	                if (section.type === "custom") {
	                    __append("\n                ");
	                    __line = 15;
	                    __append(include(section.alias, {
	                        data: section
	                    }));
	                    __append("\n                ");
	                    __line = 16;
	                } else {
	                    __append("\n                ");
	                    __line = 17;
	                    __append(include("common-section", {
	                        data: section
	                    }));
	                    __append("\n                ");
	                    __line = 18;
	                }
	                __append("\n\n                ");
	                __line = 20;
	                if (section.backgroundVideo && section.backgroundVideo.youtube) {
	                    __append('\n                <div class="section-bg-youtube section-bg-');
	                    __line = 21;
	                    __append(section.alias);
	                    __append('">\n                    <iframe id="youtube_bg_');
	                    __line = 22;
	                    __append(section.alias);
	                    __append('" class="section-video-iframe" type="text/html" width="100%" height="100%"\n                            src="http://www.youtube.com/embed/');
	                    __line = 23;
	                    __append(section.backgroundVideo.youtube);
	                    __append("?version=3&enablejsapi=1&autoplay=1&controls=0&loop=1&rel=0&disablekb=1&iv_load_policy=3&modestbranding=1&showinfo=0&playlist=");
	                    __append(section.backgroundVideo.youtube);
	                    __append('"\n                            frameborder="0"></iframe>\n                </div>\n                ');
	                    __line = 26;
	                }
	                __append("\n            </div>\n            ");
	                __line = 28;
	                if (section.pic && typeof section.pic === "string") {
	                    __append('\n            <div class="section-bg section-bg-inner section-bg-');
	                    __line = 29;
	                    __append(section.alias);
	                    __append('" ');
	                    __append(section.pic ? 'style="background-image: url(/static/i/' + section.pic + ');"' : "");
	                    __append('\n                 data-pic="');
	                    __line = 30;
	                    __append(section.pic ? "/static/i/" + section.pic : "");
	                    __append('"></div>\n            ');
	                    __line = 31;
	                } else if (section.pic) {
	                    __append('\n            <div class="swipe section-bg section-bg-inner section-bg-');
	                    __line = 32;
	                    __append(section.alias);
	                    __append(" ");
	                    __append(section.bgLoopPartner ? "bg-loop-partner" : "bg-loop");
	                    __append('">\n                <div class="swipe-wrap">\n                    ');
	                    __line = 34;
	                    section.pic.forEach(function(picRaw, i) {
	                        var pic = picRaw.split("@");
	                        __append('\n                    <div class="bg-pic section-bg-pic" ');
	                        __line = 35;
	                        __append(pic[0] ? 'style="background-image: url(/static/i/' + pic[0] + ');"' : "");
	                        __append('\n                         data-pic="');
	                        __line = 36;
	                        __append(pic[0] ? "/static/i/" + pic[0] : "");
	                        __append('"></div>\n                    ');
	                        __line = 37;
	                    });
	                    __append('\n                </div>\n            </div>\n            <a class="swipe-close modal-close"></a>\n            <div class="swipe-controls">\n                <a class="swipe-controls-btn swipe-controls-btn-prev"></a>\n                <a class="swipe-controls-btn swipe-controls-btn-next"></a>\n            </div>\n            ');
	                    __line = 45;
	                    var colors = [];
	                    section.pic.forEach(function(picRaw) {
	                        colors.push(picRaw.split("@")[1]);
	                    });
	                    __line = 47;
	                    __append('\n            <div class="swipe-points" data-colors=\'');
	                    __line = 48;
	                    __append(JSON.stringify(colors));
	                    __append("'>\n                ");
	                    __line = 49;
	                    section.pic.forEach(function(pic, i) {
	                        __append('\n                <div class="swipe-point" data-index="');
	                        __line = 50;
	                        __append(i);
	                        __append('"></div>\n                ');
	                        __line = 51;
	                    });
	                    __append("\n            </div>\n            ");
	                    __line = 53;
	                } else if (section.backgroundVideo && !section.backgroundVideo.youtube) {
	                    __append("\n            <div data-background-video='");
	                    __line = 54;
	                    __append(JSON.stringify(section.backgroundVideo));
	                    __append("'\n                 class=\"section-bg bg-video section-bg-inner section-bg-");
	                    __line = 55;
	                    __append(section.alias);
	                    __append('"\n                 style="background-image: url(/static/i/');
	                    __line = 56;
	                    __append(section.backgroundVideo.poster);
	                    __append(');"></div>\n            ');
	                    __line = 57;
	                }
	                __append("\n        </div>\n        ");
	                __line = 59;
	            });
	            __append("\n    </div>\n</div>\n");
	            __line = 62;
	            __append(include("menu"));
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 63 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<form data-persist="garlic" id="feedback-form" class="form">\n    <div class="form-i">\n        <div class="topics">\n            <div class="form-header"><%- data.header %></div>\n            <% data.content.forEach(function(item) { %>\n            <div class="topic">\n                <a href="<%= item.href %>">\n                    <%= item.text %>\n                    <img class="topic-link-img" src="/static/i/ios-arrow-right.svg" alt="">\n                </a>\n            </div>\n            <% }) %>\n        </div>\n        <div class="steps"></div>\n    </div>\n</form>\n<a class="feedback-email" href="mailto:<%- data.contactEmail %>"><%- data.contactEmail %></a>', __filename = "src/tmpl/index/enquire.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<form data-persist="garlic" id="feedback-form" class="form">\n    <div class="form-i">\n        <div class="topics">\n            <div class="form-header">');
	            __line = 4;
	            __append(data.header);
	            __append("</div>\n            ");
	            __line = 5;
	            data.content.forEach(function(item) {
	                __append('\n            <div class="topic">\n                <a href="');
	                __line = 7;
	                __append(escapeFn(item.href));
	                __append('">\n                    ');
	                __line = 8;
	                __append(escapeFn(item.text));
	                __append('\n                    <img class="topic-link-img" src="/static/i/ios-arrow-right.svg" alt="">\n                </a>\n            </div>\n            ');
	                __line = 12;
	            });
	            __append('\n        </div>\n        <div class="steps"></div>\n    </div>\n</form>\n<a class="feedback-email" href="mailto:');
	            __line = 17;
	            __append(data.contactEmail);
	            __append('">');
	            __append(data.contactEmail);
	            __append("</a>");
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 64 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<% if(data.title) { %>\n<div class="section-title">\n    <%- data.header || data.title %>\n</div>\n<% } if(data.content && data.content.title) { %>\n<div class="section-subtitle">\n    <%- data.content.title %>\n</div>\n<% } if(data.content && data.content.text) { %>\n<div class="section-text">\n    <%- data.content.text %>\n</div>\n<% } %>\n\n<div class="events">\n    <div class="events-map">\n        <div class="events-map-i">\n            <% data.content.events.forEach(function(item) { %>\n            <% if (item.dot) { %>\n            <div class="events-dot <%- item.dot %>">\n                <img class="events-dot-pic" src="static/i/map/<%- item.dot + \'-grey\' %>.png">\n                <img class="events-dot-pic-next" src="static/i/map/<%- item.dot %>.png">\n            </div>\n            <% } %>\n            <% }) %>\n            <img class="events-map-pic" src="<%- \'static/i/map/\' + data.content.activeMap %>">\n        </div>\n    </div>\n    <div class="events-places">\n        <div class="events-places-i">\n            <% data.content.events.forEach(function(item) { %>\n            <div id="place-dot-<%- item.dot%>" class="events-place <%- item.isPast ? \'events-place-past\' : \'\'%>" data-dot="<%- item.dot || \'\' %>" data-positionLeft="<%- item.positionLeft || \'\' %>">\n                <div class="events-city"><%= item.place %></div>\n                <div class="events-date"><%= item.date %></div>\n            </div>\n            <% }) %>\n        </div>\n    </div>\n</div>\n<% if(data.content && (data.content.logoLeft || data.content.logoRight)) { %>\n<div class="section-logos">\n    <% if(data.content.logoLeft) { %>\n    <div class="section-logo section-logo-left">\n        <div class="section-logo-text"><%- data.content.logoLeft.text %></div>\n        <a class="section-logo-pic" style="background-image: url(\'/static/i/<%- data.content.logoLeft.pic %>\')"></a>\n    </div>\n    <% } %>\n    <% if(data.content.logoRight) { %>\n    <div class="section-logo section-logo-right">\n        <div class="section-logo-text"><%- data.content.logoRight.text %></div>\n        <a class="section-logo-pic" style="background-image: url(\'/static/i/<%- data.content.logoRight.pic %>\')"></a>\n    </div>\n    <% } %>\n</div>\n<% } %>\n', __filename = "src/tmpl/index/events.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            if (data.title) {
	                __append('\n<div class="section-title">\n    ');
	                __line = 3;
	                __append(data.header || data.title);
	                __append("\n</div>\n");
	                __line = 5;
	            }
	            if (data.content && data.content.title) {
	                __append('\n<div class="section-subtitle">\n    ');
	                __line = 7;
	                __append(data.content.title);
	                __append("\n</div>\n");
	                __line = 9;
	            }
	            if (data.content && data.content.text) {
	                __append('\n<div class="section-text">\n    ');
	                __line = 11;
	                __append(data.content.text);
	                __append("\n</div>\n");
	                __line = 13;
	            }
	            __append('\n\n<div class="events">\n    <div class="events-map">\n        <div class="events-map-i">\n            ');
	            __line = 18;
	            data.content.events.forEach(function(item) {
	                __append("\n            ");
	                __line = 19;
	                if (item.dot) {
	                    __append('\n            <div class="events-dot ');
	                    __line = 20;
	                    __append(item.dot);
	                    __append('">\n                <img class="events-dot-pic" src="static/i/map/');
	                    __line = 21;
	                    __append(item.dot + "-grey");
	                    __append('.png">\n                <img class="events-dot-pic-next" src="static/i/map/');
	                    __line = 22;
	                    __append(item.dot);
	                    __append('.png">\n            </div>\n            ');
	                    __line = 24;
	                }
	                __append("\n            ");
	                __line = 25;
	            });
	            __append('\n            <img class="events-map-pic" src="');
	            __line = 26;
	            __append("static/i/map/" + data.content.activeMap);
	            __append('">\n        </div>\n    </div>\n    <div class="events-places">\n        <div class="events-places-i">\n            ');
	            __line = 31;
	            data.content.events.forEach(function(item) {
	                __append('\n            <div id="place-dot-');
	                __line = 32;
	                __append(item.dot);
	                __append('" class="events-place ');
	                __append(item.isPast ? "events-place-past" : "");
	                __append('" data-dot="');
	                __append(item.dot || "");
	                __append('" data-positionLeft="');
	                __append(item.positionLeft || "");
	                __append('">\n                <div class="events-city">');
	                __line = 33;
	                __append(escapeFn(item.place));
	                __append('</div>\n                <div class="events-date">');
	                __line = 34;
	                __append(escapeFn(item.date));
	                __append("</div>\n            </div>\n            ");
	                __line = 36;
	            });
	            __append("\n        </div>\n    </div>\n</div>\n");
	            __line = 40;
	            if (data.content && (data.content.logoLeft || data.content.logoRight)) {
	                __append('\n<div class="section-logos">\n    ');
	                __line = 42;
	                if (data.content.logoLeft) {
	                    __append('\n    <div class="section-logo section-logo-left">\n        <div class="section-logo-text">');
	                    __line = 44;
	                    __append(data.content.logoLeft.text);
	                    __append('</div>\n        <a class="section-logo-pic" style="background-image: url(\'/static/i/');
	                    __line = 45;
	                    __append(data.content.logoLeft.pic);
	                    __append("')\"></a>\n    </div>\n    ");
	                    __line = 47;
	                }
	                __append("\n    ");
	                __line = 48;
	                if (data.content.logoRight) {
	                    __append('\n    <div class="section-logo section-logo-right">\n        <div class="section-logo-text">');
	                    __line = 50;
	                    __append(data.content.logoRight.text);
	                    __append('</div>\n        <a class="section-logo-pic" style="background-image: url(\'/static/i/');
	                    __line = 51;
	                    __append(data.content.logoRight.pic);
	                    __append("')\"></a>\n    </div>\n    ");
	                    __line = 53;
	                }
	                __append("\n</div>\n");
	                __line = 55;
	            }
	            __append("\n");
	            __line = 56;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 65 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<% if(data.title) { %>\n<div class="section-title">\n    <%- data.title %>\n</div>\n<% } %>\n<div class="follow">\n    <div class="follow-i">\n        <form class="follow-form">\n            <div class="follow-form-i follow-form-i-normal">\n                <div class="follow-input">\n                    <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"\n                            class="follow-input-c" name="email" placeholder="<%- data.inputText %>"/>\n                </div>\n                <button type="submit" class="follow-button js-submit"><%- data.inputButton %></button>\n                <input type="hidden" name="topic" value="<%- data.topic %>"/> <input type="hidden" name="topicValue"\n                        value="<%- data.topicValue %>"/>\n            </div>\n            <div class="follow-form-captcha js-captcha">\n                <div class="form-input-label"></div>\n                <div class="follow-form-i follow-form-i-captcha">\n                    <div class="follow-input">\n                        <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"\n                                class="follow-input-c" name="captcha" placeholder="123"/>\n                    </div>\n                    <button type="submit" class="follow-button js-submit"><%- data.inputButton %></button>\n                    <input class="js-captcha-code" type="hidden" name="captcha-code" value=""/>\n                </div>\n            </div>\n            <div class="follow-form-done"><%- data.done %></div>\n            <div class="follow-form-error"><%- data.error %></div>\n        </form>\n    </div>\n    <div class="follow-social"><%- include(\'../common/socialTmpl\', {data: null}) %></div>\n</div>\n', __filename = "src/tmpl/index/follow.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            if (data.title) {
	                __append('\n<div class="section-title">\n    ');
	                __line = 3;
	                __append(data.title);
	                __append("\n</div>\n");
	                __line = 5;
	            }
	            __append('\n<div class="follow">\n    <div class="follow-i">\n        <form class="follow-form">\n            <div class="follow-form-i follow-form-i-normal">\n                <div class="follow-input">\n                    <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"\n                            class="follow-input-c" name="email" placeholder="');
	            __line = 12;
	            __append(data.inputText);
	            __append('"/>\n                </div>\n                <button type="submit" class="follow-button js-submit">');
	            __line = 14;
	            __append(data.inputButton);
	            __append('</button>\n                <input type="hidden" name="topic" value="');
	            __line = 15;
	            __append(data.topic);
	            __append('"/> <input type="hidden" name="topicValue"\n                        value="');
	            __line = 16;
	            __append(data.topicValue);
	            __append('"/>\n            </div>\n            <div class="follow-form-captcha js-captcha">\n                <div class="form-input-label"></div>\n                <div class="follow-form-i follow-form-i-captcha">\n                    <div class="follow-input">\n                        <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"\n                                class="follow-input-c" name="captcha" placeholder="123"/>\n                    </div>\n                    <button type="submit" class="follow-button js-submit">');
	            __line = 25;
	            __append(data.inputButton);
	            __append('</button>\n                    <input class="js-captcha-code" type="hidden" name="captcha-code" value=""/>\n                </div>\n            </div>\n            <div class="follow-form-done">');
	            __line = 29;
	            __append(data.done);
	            __append('</div>\n            <div class="follow-form-error">');
	            __line = 30;
	            __append(data.error);
	            __append('</div>\n        </form>\n    </div>\n    <div class="follow-social">');
	            __line = 33;
	            __append(include("../common/socialTmpl", {
	                data: null
	            }));
	            __append("</div>\n</div>\n");
	            __line = 35;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 66 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<form data-persist="garlic" id="feedback-form" class="form">\n    <div class="form-i">\n        <div class="step">\n            <div class="form-header"><%- data.header %></div>\n            <div class="form-subheader">\n                <p>We are looking for the brightest minds to help us grow.</p>\n                <p>Get in touch if you want to help create the future of mobility.</p>\n            </div>\n            <div class="form-subheader-success">\n                <p>Thank you for taking an interest in a career with Roborace. Please check your emails.</p>\n            </div>\n            <div class="form-input">\n                <label class="form-input-label">Name:</label>\n                <input\n                    autocomplete="off"\n                    autocorrect="off"\n                    autocapitalize="off"\n                    spellcheck="false"\n                    class="form-input"\n                    name="name"\n                    placeholder="First Last"\n                />\n            </div>\n            <div class="form-input">\n                <label class="form-input-label">Email Address:</label>\n                <input\n                    autocomplete="off"\n                    autocorrect="off"\n                    autocapitalize="off"\n                    spellcheck="false"\n                    class="form-input"\n                    name="email"\n                    placeholder="you@example.com"\n                />\n            </div>\n            <div class="form-input-textarea">\n                <label class="form-input-label">Tell us a bit about you:</label>\n                <textarea\n                    autocomplete="off"\n                    autocorrect="off"\n                    autocapitalize="off"\n                    spellcheck="false"\n                    class="form-input-textarea"\n                    name="text"\n                    placeholder="Tell us a bit about you."\n                ></textarea>\n            </div>\n            <div class="form-error">\n                Please check your email address.\n            </div>\n            <div class="form-actions form-actions-joinus">\n                <input type="file" name="cv" id="cv" accept=".doc, .docx, .pdf">\n                <label for="cv" class="form-action form-action-primary">Upload your CV (PDF or docx)</label>\n                <button type="submit" class="joinus-button cv-submit form-action form-action-primary">Send</button> \n                 \n       \t\t</div>\n            <div style="text-align:center">    \n  \t\t\t<br><br> <a href="/static/docs/PrivacyPolicy.pdf">Privacy Policy</a>\n  \t\t\t</div>\n        </div>\n\n    </div>\n   \n</form>\n\n', __filename = "src/tmpl/index/joinus.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<form data-persist="garlic" id="feedback-form" class="form">\n    <div class="form-i">\n        <div class="step">\n            <div class="form-header">');
	            __line = 4;
	            __append(data.header);
	            __append('</div>\n            <div class="form-subheader">\n                <p>We are looking for the brightest minds to help us grow.</p>\n                <p>Get in touch if you want to help create the future of mobility.</p>\n            </div>\n            <div class="form-subheader-success">\n                <p>Thank you for taking an interest in a career with Roborace. Please check your emails.</p>\n            </div>\n            <div class="form-input">\n                <label class="form-input-label">Name:</label>\n                <input\n                    autocomplete="off"\n                    autocorrect="off"\n                    autocapitalize="off"\n                    spellcheck="false"\n                    class="form-input"\n                    name="name"\n                    placeholder="First Last"\n                />\n            </div>\n            <div class="form-input">\n                <label class="form-input-label">Email Address:</label>\n                <input\n                    autocomplete="off"\n                    autocorrect="off"\n                    autocapitalize="off"\n                    spellcheck="false"\n                    class="form-input"\n                    name="email"\n                    placeholder="you@example.com"\n                />\n            </div>\n            <div class="form-input-textarea">\n                <label class="form-input-label">Tell us a bit about you:</label>\n                <textarea\n                    autocomplete="off"\n                    autocorrect="off"\n                    autocapitalize="off"\n                    spellcheck="false"\n                    class="form-input-textarea"\n                    name="text"\n                    placeholder="Tell us a bit about you."\n                ></textarea>\n            </div>\n            <div class="form-error">\n                Please check your email address.\n            </div>\n            <div class="form-actions form-actions-joinus">\n                <input type="file" name="cv" id="cv" accept=".doc, .docx, .pdf">\n                <label for="cv" class="form-action form-action-primary">Upload your CV (PDF or docx)</label>\n                <button type="submit" class="joinus-button cv-submit form-action form-action-primary">Send</button> \n                 \n       \t\t</div>\n            <div style="text-align:center">    \n  \t\t\t<br><br> <a href="/static/docs/PrivacyPolicy.pdf">Privacy Policy</a>\n  \t\t\t</div>\n        </div>\n\n    </div>\n   \n</form>\n\n');
	            __line = 66;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 67 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="menu">\n    <div class="menu-i">\n        <% content.forEach(function(item){ %>\n        <% if(!item.menuHidden) { %>\n        <div class="menu-item menu-item-<%- item.alias %>">\n            <a href="#<%- item.alias %>" class="menu-item-a">\n                <%- item.title %>\n            </a>\n        </div>\n        <% } %>\n        <% }) %>\n    </div>\n</div>', __filename = "src/tmpl/index/menu.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="menu">\n    <div class="menu-i">\n        ');
	            __line = 3;
	            content.forEach(function(item) {
	                __append("\n        ");
	                __line = 4;
	                if (!item.menuHidden) {
	                    __append('\n        <div class="menu-item menu-item-');
	                    __line = 5;
	                    __append(item.alias);
	                    __append('">\n            <a href="#');
	                    __line = 6;
	                    __append(item.alias);
	                    __append('" class="menu-item-a">\n                ');
	                    __line = 7;
	                    __append(item.title);
	                    __append("\n            </a>\n        </div>\n        ");
	                    __line = 10;
	                }
	                __append("\n        ");
	                __line = 11;
	            });
	            __append("\n    </div>\n</div>");
	            __line = 13;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 68 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<% if(data.title) { %>\n<div class="section-title">\n    <%- data.header || data.title %>\n</div>\n<% } if(data.content && data.content.title) { %>\n<div class="section-subtitle">\n    <%- data.content.title %>\n</div>\n<% } if(data.content && data.content.text) { %>\n<div class="section-text">\n    <%- data.content.text %>\n</div>\n<% } %>\n<div class="partners">\n    <div class="partners-i">\n    <% data.content.partners.forEach(function(item) { %>\n    <div class="partner-title <%- item.alias %>" data-partner="<%- item.alias || \'\' %>"><%= item.title%></div>\n    <% }) %>\n    </div>\n</div>\n<div class="partner-logos">\n    <% data.content.partners.forEach(function(item) { %>\n    <div class="partner-logo <%- item.alias %>">\n        <% if(item.logo) { %>\n            <a href="<%- item.link %>" target="_blank" class="section-logo-pic partner-logo-pic" style="background-image: url(\'/static/i/<%- item.logo %>\')"></a>\n        <% } %>\n        <% if(item.text) { %>\n            <a href="<%- item.link %>" target="_blank" class="partner-logo-text"><%- item.text %></a>\n        <% } %>\n    </div>\n    <% }) %>\n</div>\n<div class="partner-partnership-link"><%= data.content.link %></div>\n', __filename = "src/tmpl/index/partners.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            if (data.title) {
	                __append('\n<div class="section-title">\n    ');
	                __line = 3;
	                __append(data.header || data.title);
	                __append("\n</div>\n");
	                __line = 5;
	            }
	            if (data.content && data.content.title) {
	                __append('\n<div class="section-subtitle">\n    ');
	                __line = 7;
	                __append(data.content.title);
	                __append("\n</div>\n");
	                __line = 9;
	            }
	            if (data.content && data.content.text) {
	                __append('\n<div class="section-text">\n    ');
	                __line = 11;
	                __append(data.content.text);
	                __append("\n</div>\n");
	                __line = 13;
	            }
	            __append('\n<div class="partners">\n    <div class="partners-i">\n    ');
	            __line = 16;
	            data.content.partners.forEach(function(item) {
	                __append('\n    <div class="partner-title ');
	                __line = 17;
	                __append(item.alias);
	                __append('" data-partner="');
	                __append(item.alias || "");
	                __append('">');
	                __append(escapeFn(item.title));
	                __append("</div>\n    ");
	                __line = 18;
	            });
	            __append('\n    </div>\n</div>\n<div class="partner-logos">\n    ');
	            __line = 22;
	            data.content.partners.forEach(function(item) {
	                __append('\n    <div class="partner-logo ');
	                __line = 23;
	                __append(item.alias);
	                __append('">\n        ');
	                __line = 24;
	                if (item.logo) {
	                    __append('\n            <a href="');
	                    __line = 25;
	                    __append(item.link);
	                    __append('" target="_blank" class="section-logo-pic partner-logo-pic" style="background-image: url(\'/static/i/');
	                    __append(item.logo);
	                    __append("')\"></a>\n        ");
	                    __line = 26;
	                }
	                __append("\n        ");
	                __line = 27;
	                if (item.text) {
	                    __append('\n            <a href="');
	                    __line = 28;
	                    __append(item.link);
	                    __append('" target="_blank" class="partner-logo-text">');
	                    __append(item.text);
	                    __append("</a>\n        ");
	                    __line = 29;
	                }
	                __append("\n    </div>\n    ");
	                __line = 31;
	            });
	            __append('\n</div>\n<div class="partner-partnership-link">');
	            __line = 33;
	            __append(escapeFn(data.content.link));
	            __append("</div>\n");
	            __line = 34;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 69 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="main-logo vertical">\n    <div class="main-logo-m"></div>\n</div>\n<div class="landing">\n    <div class="landing-slide">\n        <div class="landing-title">\n            <%- data.content.title %>\n        </div>\n    </div>\n</div>', __filename = "src/tmpl/index/robocar-360.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="main-logo vertical">\n    <div class="main-logo-m"></div>\n</div>\n<div class="landing">\n    <div class="landing-slide">\n        <div class="landing-title">\n            ');
	            __line = 7;
	            __append(data.content.title);
	            __append("\n        </div>\n    </div>\n</div>");
	            __line = 10;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 70 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "", __filename = "src/tmpl/index/robocar.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {}
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 71 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/garlic.js/1.2.4/garlic.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/ScrollToPlugin.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>\n<script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>\n<script type="text/javascript" src="/static/typed.js"></script>\n<script>\n    document.write(\'<scrip\' + \'t src="js/check-code.js?v=\' + Date.now() + \'"></s\' + \'cript>\');\n</script>\n<script>\n    var tag = document.createElement(\'script\');\n\n    tag.src = "https://www.youtube.com/iframe_api";\n    var firstScriptTag = document.getElementsByTagName(\'script\')[0];\n    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\n\n</script>\n<script type="text/javascript" src="/static/build/all.js"></script>\n<script type="text/javascript" src="https://www.youtube.com/iframe_api"></script>\n', __filename = "src/tmpl/index/scripts.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/garlic.js/1.2.4/garlic.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/ScrollToPlugin.min.js"></script>\n<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>\n<script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>\n<script type="text/javascript" src="/static/typed.js"></script>\n<script>\n    document.write(\'<scrip\' + \'t src="js/check-code.js?v=\' + Date.now() + \'"></s\' + \'cript>\');\n</script>\n<script>\n    var tag = document.createElement(\'script\');\n\n    tag.src = "https://www.youtube.com/iframe_api";\n    var firstScriptTag = document.getElementsByTagName(\'script\')[0];\n    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\n\n</script>\n<script type="text/javascript" src="/static/build/all.js"></script>\n<script type="text/javascript" src="https://www.youtube.com/iframe_api"></script>\n');
	            __line = 21;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 72 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="video">\n    <div class="video-list-c">\n        <img src="/static/i/ios-arrow-left.svg" alt="" class="video-btn-prev video-btn">\n        <div class="video-list-i">\n            <div class="video-list">\n                <% items.reverse().forEach(function(item){ %>\n                <% if(!item.video) { %>\n                <div class="video-list-item video-list-placeholder">\n                    <div class="video-list-title"><%- item.title %></div>\n                    <div class="video-list-preview">\n                        <% if(item.date) { %>\n                        <div class="video-list-date">\n                            <div class="video-list-day"><%- item.date.day %></div>\n                            <div class="video-list-month"><%- item.date.month %></div>\n                        </div>\n                        <% } else if(item.text) { %>\n                        <div class="video-list-text"> <%- item.text %></div>\n                        <% } %>\n                    </div>\n                </div>\n                <% } else { %>\n                <div class="video-list-item">\n                    <div class="video-list-content">\n                        <div class="video-list-title"><%- item.title %></div>\n                        <div class="video-list-preview youtube-video" data-id="<%- item.video %>">\n                            <div class="video-list-pic" style="background-image: url(\'https://img.youtube.com/vi/<%- item.video %>/maxresdefault.jpg\')"></div>\n                        </div>\n                    </div>\n                </div>\n                <% } %>\n                <% }); %>\n            </div>\n        </div>\n        <img src="/static/i/ios-arrow-right.svg" alt="" class="video-btn video-btn-next">\n    </div>\n    <div class="video-list-subscribe-control">\n        <div class="g-ytsubscribe" data-channelid="UCDThZNmxhmEOfp0sm7HtFvw" data-layout="default" data-theme="dark"\n             data-count="hidden"></div>\n        <div class="video-list-subscribe-text">subscribe</div>\n    </div>\n</div>\n', __filename = "src/tmpl/index/video-list.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="video">\n    <div class="video-list-c">\n        <img src="/static/i/ios-arrow-left.svg" alt="" class="video-btn-prev video-btn">\n        <div class="video-list-i">\n            <div class="video-list">\n                ');
	            __line = 6;
	            items.reverse().forEach(function(item) {
	                __append("\n                ");
	                __line = 7;
	                if (!item.video) {
	                    __append('\n                <div class="video-list-item video-list-placeholder">\n                    <div class="video-list-title">');
	                    __line = 9;
	                    __append(item.title);
	                    __append('</div>\n                    <div class="video-list-preview">\n                        ');
	                    __line = 11;
	                    if (item.date) {
	                        __append('\n                        <div class="video-list-date">\n                            <div class="video-list-day">');
	                        __line = 13;
	                        __append(item.date.day);
	                        __append('</div>\n                            <div class="video-list-month">');
	                        __line = 14;
	                        __append(item.date.month);
	                        __append("</div>\n                        </div>\n                        ");
	                        __line = 16;
	                    } else if (item.text) {
	                        __append('\n                        <div class="video-list-text"> ');
	                        __line = 17;
	                        __append(item.text);
	                        __append("</div>\n                        ");
	                        __line = 18;
	                    }
	                    __append("\n                    </div>\n                </div>\n                ");
	                    __line = 21;
	                } else {
	                    __append('\n                <div class="video-list-item">\n                    <div class="video-list-content">\n                        <div class="video-list-title">');
	                    __line = 24;
	                    __append(item.title);
	                    __append('</div>\n                        <div class="video-list-preview youtube-video" data-id="');
	                    __line = 25;
	                    __append(item.video);
	                    __append('">\n                            <div class="video-list-pic" style="background-image: url(\'https://img.youtube.com/vi/');
	                    __line = 26;
	                    __append(item.video);
	                    __append("/maxresdefault.jpg')\"></div>\n                        </div>\n                    </div>\n                </div>\n                ");
	                    __line = 30;
	                }
	                __append("\n                ");
	                __line = 31;
	            });
	            __append('\n            </div>\n        </div>\n        <img src="/static/i/ios-arrow-right.svg" alt="" class="video-btn video-btn-next">\n    </div>\n    <div class="video-list-subscribe-control">\n        <div class="g-ytsubscribe" data-channelid="UCDThZNmxhmEOfp0sm7HtFvw" data-layout="default" data-theme="dark"\n             data-count="hidden"></div>\n        <div class="video-list-subscribe-text">subscribe</div>\n    </div>\n</div>\n');
	            __line = 42;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 73 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "<!DOCTYPE html>\n<html lang=\"en\">\n<%- include('common/head', {page: 'legal', dev: dev}) %>\n<body>\n<div id=\"content\" class=\"content content_inner\">\n    <%- include('legal/content', {page: 'legal'})%>\n    <%- include('common/footer', {page: 'legal'})%>\n</div>\n<%- include('common/counters')%>\n</body>\n</html>", __filename = "src/tmpl/legal.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<!DOCTYPE html>\n<html lang="en">\n');
	            __line = 3;
	            __append(include("common/head", {
	                page: "legal",
	                dev: dev
	            }));
	            __append('\n<body>\n<div id="content" class="content content_inner">\n    ');
	            __line = 6;
	            __append(include("legal/content", {
	                page: "legal"
	            }));
	            __append("\n    ");
	            __line = 7;
	            __append(include("common/footer", {
	                page: "legal"
	            }));
	            __append("\n</div>\n");
	            __line = 9;
	            __append(include("common/counters"));
	            __append("\n</body>\n</html>");
	            __line = 11;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 74 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="slide slide_legal">\n    <div class="slide-i">\n        <h1 class="logo legal">\n            <div class="logo-i">\n                <div class="logo-pic"></div>\n            </div>\n        </h1>\n        <h2>Legal Notice and Privacy Policy</h2>\n\n        <p>This website is owned and operated by ROBORACE LIMITED (a company registered in England under registration number 09848067 with its registered office at 3 Charlbury Grove, Ealing, London, W5 2DY (“ROBORACE”). By accessing these webpages and/or any subsections of such pages (the “ROBORACE website”), you confirm you understand and accept the terms and conditions of use and the regulatory requirements governing the use of the ROBORACE website.</p>\n\n        <p>All of the content of the ROBORACE website is subject to copyright with all rights reserved. You may download or print out a hard copy of individual pages and/or sections of the ROBORACE website, provided that you do not remove any copyright or other proprietary notices. Any downloading or copying from the ROBORACE website will not transfer title to any software or material to you. If you print off, copy or download any part of the ROBORACE website in breach of these terms and conditions of use, your right to use the ROBORACE website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. You may not reproduce (in whole or in part), transmit (by electronic means or otherwise), modify, link into or use for any public or commercial purpose or in any way exploit the whole or any part of the ROBORACE website without the prior written permission of ROBORACE. ROBORACE reserves all rights with respect to copyright and trademark ownership of all material on the ROBORACE website. You may not use the ROBORACE website in any way that is fraudulent or unlawful.</p>\n\n        <p>The ROBORACE website is for informational purposes only and should not be construed as a solicitation or offer, or recommendation to acquire or dispose of any investment or to engage in any other transaction. ROBORACE does not offer any investments or investment services to any persons in the UK or elsewhere. Nothing contained on the ROBORACE website constitutes investment, legal, tax or other advice nor is it to be relied on in making an investment or other decision. The ROBORACE website is not directed at any person in any jurisdiction where (by reason of that person\'s nationality, residence or otherwise) the publication or availability of the ROBORACE website is prohibited or which would subject us to any registration or other requirement within such jurisdiction or country. Specifically, the ROBORACE website is not directed at any person in the United States of America or Canada. We reserve the right to limit access to the ROBORACE website to any person, geographic region or jurisdiction. </p>\n\n        <p>While we use reasonable efforts to obtain information from sources which we believe to be reliable, we make no representation that the information or opinions contained in the ROBORACE website are accurate, reliable or complete. The information and opinions contained in the ROBORACE website are provided by us for personal use and informational purposes only and are subject to change without notice. </p>\n\n        <p>The ROBORACE website is provided "as is" and we give no warranties of any kind express or implied in respect thereof. Except as expressly set out in these terms and conditions of use, all warranties, conditions and representations expressed or implied by statute, common law or otherwise (including, without limitation, warranties as to satisfactory quality, fitness for purpose or skill and care) are hereby excluded to the fullest extent permitted by law. It is your responsibility to use anti-virus software on any software or other material that you may download from the ROBORACE website and to ensure the compatibility of such software or material with your equipment and to check any content or information published on the ROBORACE website is accurate or complete.</p>\n\n        <p>We will not (subject only to the exclusion provisions below) be liable to you, whether for negligence, breach of contract or otherwise, for any loss or damage of whatsoever nature suffered by you (including, without limitation, direct loss or damage, indirect or consequential loss or damage, loss of goodwill, loss of business opportunity, loss of data or loss of profit), arising from: any computer virus or other bug transmitted through the ROBORACE website; your use or inability to use the ROBORACE website at any time and any error in the provision of the ROBORACE website; your use of or reliance on other websites to which you have gained access by means of hyperlinks published on the ROBORACE website; any inaccurate information published on the ROBORACE website; your failure to comply with any laws or regulations that are applicable to you; or your reliance on any information published on the ROBORACE website. Nothing in these terms and conditions of use shall operate to exclude or restrict our liability for either fraud, death or personal injury.</p>\n\n        <p>When you access certain links in the ROBORACE website you may leave the ROBORACE website. We have not reviewed any of the websites linked to the ROBORACE website and do not endorse or accept any responsibility for the content of such websites, nor the products or services or other items described on such websites, nor the data protection practices of or policies on such websites which you access at your own risk and ROBORACE is not responsible for damages or losses caused by any inaccuracy or omissions in the information or content of such websites, whether actual or consequential. The editorial content of any other website to which you have gained access by means of any hyperlinks published on the ROBORACE website remains the sole responsibility of the provider of that other website.</p>\n\n        <p>Information posted on the ROBORACE website may be accessible to third parties by means of the Internet or otherwise. You should have no expectation of privacy with respect to any communications on or through the ROBORACE website. Please do not transmit any confidential information to or through the ROBORACE website. You acknowledge that communications transmitted by means of the ROBORACE website are public and not private communications.</p>\n\n        <p>You should be aware that the Internet, being an open network, is not secure. If you choose to send any electronic communications to us by means of the ROBORACE website (whether by means of email or otherwise), you do so at your own risk. We cannot guarantee that such communications will not be intercepted or changed or that they will reach the intended recipient safely. </p>\n\n        <p>We use various technologies to collect and store information when you visit the ROBORACE website, and this may include using cookies or similar technologies to identify your browser or device. We also use these technologies to collect and store information when you interact with the ROBORACE website. We use Google Analytics to analyse the traffic to the ROBORACE website.</p>\n\n        <p>We use the information we collect using cookies and other technologies to improve the ROBORACE website. We do not share any personal information that we collect from you with any person outside our group of companies except with your consent, for external processing or as required by law.</p>\n\n        <p>If any provision of these terms and conditions of use is found by a court of competent jurisdiction to be invalid, you agree that the other provisions of these terms and conditions of use will remain in full force and effect in so far as possible.</p>\n\n        <p>We may revise these terms and conditions of use at any time by amending this page. </p>\n\n        <p>These terms and conditions of use, and all matters connected with them, are governed by the laws of England and Wales. The courts of England have exclusive jurisdiction to settle any dispute, controversy or claim arising from or connected with these terms and conditions of use, including regarding the existence, validity or termination of these terms and conditions of use.</p>\n\n    </div>\n</div>', __filename = "src/tmpl/legal/content.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="slide slide_legal">\n    <div class="slide-i">\n        <h1 class="logo legal">\n            <div class="logo-i">\n                <div class="logo-pic"></div>\n            </div>\n        </h1>\n        <h2>Legal Notice and Privacy Policy</h2>\n\n        <p>This website is owned and operated by ROBORACE LIMITED (a company registered in England under registration number 09848067 with its registered office at 3 Charlbury Grove, Ealing, London, W5 2DY (“ROBORACE”). By accessing these webpages and/or any subsections of such pages (the “ROBORACE website”), you confirm you understand and accept the terms and conditions of use and the regulatory requirements governing the use of the ROBORACE website.</p>\n\n        <p>All of the content of the ROBORACE website is subject to copyright with all rights reserved. You may download or print out a hard copy of individual pages and/or sections of the ROBORACE website, provided that you do not remove any copyright or other proprietary notices. Any downloading or copying from the ROBORACE website will not transfer title to any software or material to you. If you print off, copy or download any part of the ROBORACE website in breach of these terms and conditions of use, your right to use the ROBORACE website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. You may not reproduce (in whole or in part), transmit (by electronic means or otherwise), modify, link into or use for any public or commercial purpose or in any way exploit the whole or any part of the ROBORACE website without the prior written permission of ROBORACE. ROBORACE reserves all rights with respect to copyright and trademark ownership of all material on the ROBORACE website. You may not use the ROBORACE website in any way that is fraudulent or unlawful.</p>\n\n        <p>The ROBORACE website is for informational purposes only and should not be construed as a solicitation or offer, or recommendation to acquire or dispose of any investment or to engage in any other transaction. ROBORACE does not offer any investments or investment services to any persons in the UK or elsewhere. Nothing contained on the ROBORACE website constitutes investment, legal, tax or other advice nor is it to be relied on in making an investment or other decision. The ROBORACE website is not directed at any person in any jurisdiction where (by reason of that person\'s nationality, residence or otherwise) the publication or availability of the ROBORACE website is prohibited or which would subject us to any registration or other requirement within such jurisdiction or country. Specifically, the ROBORACE website is not directed at any person in the United States of America or Canada. We reserve the right to limit access to the ROBORACE website to any person, geographic region or jurisdiction. </p>\n\n        <p>While we use reasonable efforts to obtain information from sources which we believe to be reliable, we make no representation that the information or opinions contained in the ROBORACE website are accurate, reliable or complete. The information and opinions contained in the ROBORACE website are provided by us for personal use and informational purposes only and are subject to change without notice. </p>\n\n        <p>The ROBORACE website is provided "as is" and we give no warranties of any kind express or implied in respect thereof. Except as expressly set out in these terms and conditions of use, all warranties, conditions and representations expressed or implied by statute, common law or otherwise (including, without limitation, warranties as to satisfactory quality, fitness for purpose or skill and care) are hereby excluded to the fullest extent permitted by law. It is your responsibility to use anti-virus software on any software or other material that you may download from the ROBORACE website and to ensure the compatibility of such software or material with your equipment and to check any content or information published on the ROBORACE website is accurate or complete.</p>\n\n        <p>We will not (subject only to the exclusion provisions below) be liable to you, whether for negligence, breach of contract or otherwise, for any loss or damage of whatsoever nature suffered by you (including, without limitation, direct loss or damage, indirect or consequential loss or damage, loss of goodwill, loss of business opportunity, loss of data or loss of profit), arising from: any computer virus or other bug transmitted through the ROBORACE website; your use or inability to use the ROBORACE website at any time and any error in the provision of the ROBORACE website; your use of or reliance on other websites to which you have gained access by means of hyperlinks published on the ROBORACE website; any inaccurate information published on the ROBORACE website; your failure to comply with any laws or regulations that are applicable to you; or your reliance on any information published on the ROBORACE website. Nothing in these terms and conditions of use shall operate to exclude or restrict our liability for either fraud, death or personal injury.</p>\n\n        <p>When you access certain links in the ROBORACE website you may leave the ROBORACE website. We have not reviewed any of the websites linked to the ROBORACE website and do not endorse or accept any responsibility for the content of such websites, nor the products or services or other items described on such websites, nor the data protection practices of or policies on such websites which you access at your own risk and ROBORACE is not responsible for damages or losses caused by any inaccuracy or omissions in the information or content of such websites, whether actual or consequential. The editorial content of any other website to which you have gained access by means of any hyperlinks published on the ROBORACE website remains the sole responsibility of the provider of that other website.</p>\n\n        <p>Information posted on the ROBORACE website may be accessible to third parties by means of the Internet or otherwise. You should have no expectation of privacy with respect to any communications on or through the ROBORACE website. Please do not transmit any confidential information to or through the ROBORACE website. You acknowledge that communications transmitted by means of the ROBORACE website are public and not private communications.</p>\n\n        <p>You should be aware that the Internet, being an open network, is not secure. If you choose to send any electronic communications to us by means of the ROBORACE website (whether by means of email or otherwise), you do so at your own risk. We cannot guarantee that such communications will not be intercepted or changed or that they will reach the intended recipient safely. </p>\n\n        <p>We use various technologies to collect and store information when you visit the ROBORACE website, and this may include using cookies or similar technologies to identify your browser or device. We also use these technologies to collect and store information when you interact with the ROBORACE website. We use Google Analytics to analyse the traffic to the ROBORACE website.</p>\n\n        <p>We use the information we collect using cookies and other technologies to improve the ROBORACE website. We do not share any personal information that we collect from you with any person outside our group of companies except with your consent, for external processing or as required by law.</p>\n\n        <p>If any provision of these terms and conditions of use is found by a court of competent jurisdiction to be invalid, you agree that the other provisions of these terms and conditions of use will remain in full force and effect in so far as possible.</p>\n\n        <p>We may revise these terms and conditions of use at any time by amending this page. </p>\n\n        <p>These terms and conditions of use, and all matters connected with them, are governed by the laws of England and Wales. The courts of England have exclusive jurisdiction to settle any dispute, controversy or claim arising from or connected with these terms and conditions of use, including regarding the existence, validity or termination of these terms and conditions of use.</p>\n\n    </div>\n</div>');
	            __line = 39;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 75 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = "<!DOCTYPE html>\n<html lang=\"en\">\n<%- include('common/head', {page: 'media', dev: dev}) %>\n<body>\n<div id=\"content\" class=\"content content_inner\">\n    <%- include('media/content', {page: 'media'})%>\n    <%- include('common/footer', {page: 'legal'})%>\n</div>\n<%- include('common/counters')%>\n</body>\n</html>", __filename = "src/tmpl/media.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<!DOCTYPE html>\n<html lang="en">\n');
	            __line = 3;
	            __append(include("common/head", {
	                page: "media",
	                dev: dev
	            }));
	            __append('\n<body>\n<div id="content" class="content content_inner">\n    ');
	            __line = 6;
	            __append(include("media/content", {
	                page: "media"
	            }));
	            __append("\n    ");
	            __line = 7;
	            __append(include("common/footer", {
	                page: "legal"
	            }));
	            __append("\n</div>\n");
	            __line = 9;
	            __append(include("common/counters"));
	            __append("\n</body>\n</html>");
	            __line = 11;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 76 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '\n        <div class="slide slide_media">\n            <div class="slide-i">\n                <h1 class="logo legal">\n                    <div class="logo-i">\n                        <a href="/" >\n                            <div class="logo-pic"></div>\n                        </a>\n                    </div>\n                    <div class="media-enquiries">\n                        <div class="media-enquiries-title">Media Enquiries</div>\n                        <a class="media-enquiries-link" href="mailto:hello@roborace.com">hello@roborace.com</a>\n                    </div>\n                </h1>\n                <h2>Press Releases</h2>\n                  <%- include(\'releases\', {\n                      urlBase: "../static/docs/",\n                      releases: [\n                    {\n                         date: \'20 May 2017\',\n                         title: \'Roborace&nbsp;— Driverless car makes History on&nbsp;the streets of&nbsp;Paris\',\n                         fileName: \'ROBORACE_PARIS_200517\',\n                         formats: [\'pdf\', \'docx\']\n                     },\n                     {\n                        date: \'27 Feb 2017\',\n                        title: \'Roborace reveal the world’s first driverless electric race car live at Mobile World congress\',\n                        fileName: \'Roborace_reveal_the_Robocar_live_at_MWC17\',\n                        formats: [\'pdf\', \'docx\']\n                    },\n                    {\n                        date: \'18 Feb 2017\',\n                        title: \'Roborace becomes first company in driverless history to race two cars simultaneously in front of a live audience\',\n                        fileName: \'ROBORACE_RACE_TWO_CARS\',\n                        formats: [\'pdf\', \'docx\']\n                    },\n                    {\n                        date: \'15 dec 2016\',\n                        title: \'Roborace: Michelin partnership announcement\',\n                        fileName: \'Roborace_Michelin_Announcement\',\n                        formats: [\'pdf\', \'docx\']\n                    },\n                    {\n                          date: \'22 Aug 2016\',\n                          title: \'Roborace unveils “DevBot”&nbsp;— an&nbsp;intelligent, robotic, driverless, electric&nbsp;development&nbsp;car\',\n                          fileName: \'ROBORACE_UNVEILS_DEVBOT\',\n                          formats: [\'pdf\', \'docx\']\n                      },\n\n                      {\n                          date: \'30 Mar 2016\',\n                          title: \'The car of&nbsp;the&nbsp;future is&nbsp;here\',\n                          fileName: \'THE_CAR_OF_THE_FUTURE_IS_HERE\',\n                          formats: [\'pdf\', \'docx\']\n                      }]\n                  }) %>\n                <h2>Media Assets</h2>\n                <h3>Roborace&nbsp;— Driverless car makes History on&nbsp;the streets of&nbsp;Paris</h3>\n                    <%- include(\'photos\', {\n                          urlBase: "../static/i/media/",\n                          photos: [\n                            {\n                                 "previewLink": "Paris_4K.jpg",\n                                 "preview": "Paris_preview.jpg",\n                                 "links": [\n                                   {\n                                       "url": "Paris_2K.jpg",\n                                       "text": "2000 &times; 1125"\n                                   },\n                                   {\n                                       "url": "Paris_4K.jpg",\n                                       "text": "4000 &times; 2250"\n                                   }\n\n                                 ]\n                            }\n                          ]\n                      }) %>\n                <h3>Roborace reveal the world’s first driverless electric race car live at Mobile World congress</h3>\n                   <%- include(\'photos\', {\n                       urlBase: "../static/i/media/",\n                       photos: [\n\n                             {\n                                "previewLink": "MWC17_09_2K.jpg",\n                                "preview": "MWC17_09_preview.jpg",\n                                "links": [\n                                  {\n                                      "url": "MWC17_09_2K.jpg",\n                                      "text": "2000 &times; 1333"\n                                  },\n                                  {\n                                      "url": "MWC17_09_4K.jpg",\n                                      "text": "4000 &times; 2667"\n                                  }\n\n                                ]\n                         },\n                        {\n                                  "previewLink": "MWC17_02_2K.jpg",\n                                  "preview": "MWC17_02_preview.jpg",\n                                  "links": [\n                                    {\n                                        "url": "MWC17_02_2K.jpg",\n                                        "text": "2000 &times; 1333"\n                                    },\n                                    {\n                                        "url": "MWC17_02_4K.jpg",\n                                        "text": "4000 &times; 2667"\n                                    }\n\n                                  ]\n                             },\n                         {\n                                "previewLink": "MWC17_01_2K.jpg",\n                                "preview": "MWC17_01_preview.jpg",\n                                "links": [\n                                  {\n                                      "url": "MWC17_01_2K.jpg",\n                                      "text": "2000 &times; 1333"\n                                  },\n                                  {\n                                      "url": "MWC17_01_4K.jpg",\n                                      "text": "4000 &times; 2667"\n                                  }\n\n                                ]\n                           },\n                            {\n                               "previewLink": "MWC17_03_2K.jpg",\n                               "preview": "MWC17_03_preview.jpg",\n                               "links": [\n                                 {\n                                     "url": "MWC17_03_2K.jpg",\n                                     "text": "2000 &times; 1333"\n                                 },\n                                 {\n                                     "url": "MWC17_03_4K.jpg",\n                                     "text": "4000 &times; 2667"\n                                 }\n\n                               ]\n                          },\n                            {\n                               "previewLink": "MWC17_04_2K.jpg",\n                               "preview": "MWC17_04_preview.jpg",\n                               "links": [\n                                 {\n                                     "url": "MWC17_04_2K.jpg",\n                                     "text": "2000 &times; 1333"\n                                 },\n                                 {\n                                     "url": "MWC17_04_4K.jpg",\n                                     "text": "4000 &times; 2667"\n                                 }\n\n                               ]\n                          },\n                            {\n                               "previewLink": "MWC17_05_2K.jpg",\n                               "preview": "MWC17_05_preview.jpg",\n                               "links": [\n                                 {\n                                     "url": "MWC17_05_2K.jpg",\n                                     "text": "2000 &times; 1333"\n                                 },\n                                 {\n                                     "url": "MWC17_05_4K.jpg",\n                                     "text": "4000 &times; 2667"\n                                 }\n\n                               ]\n                          },\n                            {\n                               "previewLink": "MWC17_06_2K.jpg",\n                               "preview": "MWC17_06_preview.jpg",\n                               "links": [\n                                 {\n                                     "url": "MWC17_06_2K.jpg",\n                                     "text": "2000 &times; 1333"\n                                 },\n                                 {\n                                     "url": "MWC17_06_4K.jpg",\n                                     "text": "4000 &times; 2667"\n                                 }\n\n                               ]\n                          },\n                            {\n                               "previewLink": "MWC17_07_2K.jpg",\n                               "preview": "MWC17_07_preview.jpg",\n                               "links": [\n                                 {\n                                     "url": "MWC17_07_2K.jpg",\n                                     "text": "2000 &times; 1333"\n                                 },\n                                 {\n                                     "url": "MWC17_07_4K.jpg",\n                                     "text": "4000 &times; 2667"\n                                 }\n\n                               ]\n                          }\n                       ]\n                   }) %>\n                <h3>Roborace becomes first company in driverless history to race two cars simultaneously in front of a live audience</h3>\n                   <%- include(\'photos\', {\n                       urlBase: "../static/i/media/",\n                       photos: [\n                         {\n                              "previewLink": "BA_Devbot_4K.jpg",\n                              "preview": "BA_Devbot_preview.jpg",\n                              "links": [\n                                {\n                                    "url": "BA_Devbot_2K.jpg",\n                                    "text": "2000 &times; 1333"\n                                },\n                                {\n                                    "url": "BA_Devbot_4K.jpg",\n                                    "text": "4000 &times; 2667"\n                                }\n\n                              ]\n                         },\n                         {\n                             "previewLink": "BA_Devbot2_4K.jpg",\n                             "preview": "BA_Devbot2_preview.jpg",\n                             "links": [\n                               {\n                                   "url": "BA_Devbot2_2K.jpg",\n                                   "text": "2000 &times; 1333"\n                               }\n                             ]\n                         },\n                         {\n                             "previewLink": "BA_Devbot3_4K.jpg",\n                             "preview": "BA_Devbot3_preview.jpg",\n                             "links": [\n                               {\n                                   "url": "BA_Devbot3_2K.jpg",\n                                   "text": "2000 &times; 1333"\n                               },\n                               {\n                                   "url": "BA_Devbot3_4K.jpg",\n                                   "text": "4000 &times; 2667"\n                               }\n\n                             ]\n                         },\n                            {\n                             "previewLink": "BA_Devbot4_4K.jpg",\n                             "preview": "BA_Devbot4_preview.jpg",\n                             "links": [\n                               {\n                                   "url": "BA_Devbot4_2K.jpg",\n                                   "text": "2000 &times; 1333"\n                               },\n                               {\n                                   "url": "BA_Devbot4_4K.jpg",\n                                   "text": "4000 &times; 2667"\n                               }\n\n                             ]\n                         }\n                       ]\n                   }) %>\n                <h3>Roborace: Michelin partnership announcement</h3>\n                  <%- include(\'photos\', {\n                      urlBase: "../static/i/media/",\n                      photos: [\n                        {\n                             "previewLink": "Michelin_Master_4K.jpg",\n                             "preview": "Michelin_Master_preview.jpg",\n                             "links": [\n                               {\n                                   "url": "Michelin_Master_2K.jpg",\n                                   "text": "2000 &times; 1125"\n                               },\n                               {\n                                   "url": "Michelin_Master_4K.jpg",\n                                   "text": "4000 &times; 2250"\n                               }\n\n                             ]\n                        }\n                      ]\n                  }) %>\n                <h3>Roborace unveils “DevBot”&nbsp;— an&nbsp;intelligent, robotic, driverless, electric&nbsp;development&nbsp;car</h3>\n                   <%- include(\'photos\', {\n                       urlBase: "../static/i/media/",\n                       photos: [\n                            {\n                                 "previewLink": "Sam_Christmas_1_4000.jpg",\n                                 "preview": "Sam_Christmas_1_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "Sam_Christmas_1_4000.jpg",\n                                        "text": "4000 &times; 2250"\n                                    },\n                                    {\n                                        "url": "Sam_Christmas_1_2000.jpg",\n                                        "text": "2000 &times; 1125"\n                                    }\n                                 ]\n                            },\n                            {\n                                 "previewLink": "Sam_Christmas_2_4000.jpg",\n                                 "preview": "Sam_Christmas_2_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "Sam_Christmas_2_4000.jpg",\n                                        "text": "4000 &times; 2250"\n                                    },\n                                    {\n                                        "url": "Sam_Christmas_2_2000.jpg",\n                                        "text": "2000 &times; 1125"\n                                    }\n                                 ]\n                            },\n                            {\n                                 "previewLink": "Sam_Christmas_3_1024.jpg",\n                                 "preview": "Sam_Christmas_3_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "Sam_Christmas_3_1024.jpg",\n                                        "text": "1024 &times; 576"\n                                    }\n                                 ]\n                            },\n                            {\n                                 "previewLink": "Sam_Christmas_4_1024.jpg",\n                                 "preview": "Sam_Christmas_4_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "Sam_Christmas_4_1024.jpg",\n                                        "text": "1024 &times; 576"\n                                    }\n                                 ]\n                            }\n                       ]\n                   }) %>\n                   <h3>Roborace unveils tech capabilities on the production version of the car</h3>\n                   <%- include(\'photos\', {\n                       urlBase: "../static/i/media/",\n                       photos: [\n                            {\n                                 "previewLink": "Roborace_Diagram02_2000px.jpg",\n                                 "preview": "Roborace_Diagram02_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "Roborace_Diagram02_2000px.jpg",\n                                        "text": "2000 &times; 1125"\n                                    },\n                                    {\n                                        "url": "Roborace_Diagram02_1200px.jpg",\n                                        "text": "1200 &times; 675"\n                                    }\n                                 ]\n                            }\n                       ]\n                   }) %>\n                <h3>Photo</h3>\n                <%- include(\'photos\', {\n                    urlBase: "../static/i/media/",\n                    photos: [\n                        {\n                              "title": "Denis Sverdlov, CEO",\n                              "previewLink": "denis-sverdlov.jpg",\n                              "preview": "denis-sverdlov-preview.jpg",\n                              "links": [\n                                 {\n                                     "url": "denis-sverdlov.jpg",\n                                     "text": "4400 &times; 2770"\n                                 }\n                              ]\n                         },\n                        {\n                              "title": "Daniel Simon, CDO",\n                              "previewLink": "daniel-simon.jpg",\n                              "preview": "daniel-simon-preview.jpg",\n                              "links": [\n                                 {\n                                     "url": "daniel-simon.jpg",\n                                     "text": "2000 &times; 1333"\n                                 }\n                              ]\n                         },\n                        {\n                             "previewLink": "Michelin_Master_4K.jpg",\n                             "preview": "Michelin_Master_preview.jpg",\n                             "links": [\n                               {\n                                   "url": "Michelin_Master_2K.jpg",\n                                   "text": "2000 &times; 1125"\n                               },\n                               {\n                                   "url": "Michelin_Master_4K.jpg",\n                                   "text": "4000 &times; 2250"\n                               }\n\n                             ]\n                        },\n                        {\n                            "previewLink": "Michelin_Fisheye_4K.jpg",\n                            "preview": "Michelin_Fisheye_preview.jpg",\n                            "links": [\n                              {\n                                  "url": "Michelin_Fisheye_2K.jpg",\n                                  "text": "2000 &times; 1125"\n                              },\n                              {\n                                  "url": "Michelin_Fisheye_4K.jpg",\n                                  "text": "4000 &times; 2250"\n                              }\n\n                            ]\n                       }\n                    ]\n                }) %>\n                <h3>Video</h3>\n                <%- include(\'photos\', {\n                    urlBase: "../static/i/media/",\n                    photos: [\n                    {\n                                 "title": "Roborace reveal the world’s first driverless electric race car <br />live at Mobile World congress",\n                                 "previewLink": "RoboRace_360_1.mp4",\n                                 "preview": "MWC17_video_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "RoboRace_360_1.mp4",\n                                        "text": "MP4"\n                                    }\n                                 ]\n                            },\n                    {\n                                 "title": "Roborace: Michelin partnership announcement",\n                                 "previewLink": "https://www.youtube.com/watch?v=26zawxa1xis",\n                                 "preview": "michelin_mov_preview.jpg",\n                                 "links": [\n                                    {\n                                        "url": "michelan.mov",\n                                        "text": "MOV"\n                                    }\n                                 ]\n                            },\n                         {\n                              "title": "DevBot for Roborace",\n                              "previewLink": "devbot_roborace.mov",\n                              "preview": "devbot_roborace_preview.jpg",\n                              "links": [\n                                 {\n                                     "url": "devbot_roborace.mov",\n                                     "text": "MOV"\n                                 }\n                              ]\n                         },\n                        {\n                              "title": "Robocar Video Teaser",\n                              "previewLink": "robocar_teaser.mov",\n                              "preview": "video-robocar-teaser-preview.jpg",\n                              "links": [\n                                 {\n                                     "url": "robocar_teaser.mov",\n                                     "text": "MOV"\n                                 }\n                              ]\n                         }\n                    ]\n                }) %>\n                <h2>Brand identity</h2>\n                <div class="section logos">\n                    <div class="h">\n                        <%- include(\'logo\', {color: \'white\', urlBase: \'../static/i/media/\', name: \'logo-h-w\'}) %>\n                        <%- include(\'logo\', {color: \'black\', urlBase: \'../static/i/media/\', name: \'logo-h\'}) %>\n                    </div>\n                    <div class="v">\n                        <%- include(\'logo\', {color: \'white\', urlBase: \'../static/i/media/\', name: \'logo-v-w\'}) %>\n                        <%- include(\'logo\', {color: \'black\', urlBase: \'../static/i/media/\', name: \'logo-v\'}) %>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n', __filename = "src/tmpl/media/content.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('\n        <div class="slide slide_media">\n            <div class="slide-i">\n                <h1 class="logo legal">\n                    <div class="logo-i">\n                        <a href="/" >\n                            <div class="logo-pic"></div>\n                        </a>\n                    </div>\n                    <div class="media-enquiries">\n                        <div class="media-enquiries-title">Media Enquiries</div>\n                        <a class="media-enquiries-link" href="mailto:hello@roborace.com">hello@roborace.com</a>\n                    </div>\n                </h1>\n                <h2>Press Releases</h2>\n                  ');
	            __line = 16;
	            __append(include("releases", {
	                urlBase: "../static/docs/",
	                releases: [ {
	                    date: "20 May 2017",
	                    title: "Roborace&nbsp;— Driverless car makes History on&nbsp;the streets of&nbsp;Paris",
	                    fileName: "ROBORACE_PARIS_200517",
	                    formats: [ "pdf", "docx" ]
	                }, {
	                    date: "27 Feb 2017",
	                    title: "Roborace reveal the world’s first driverless electric race car live at Mobile World congress",
	                    fileName: "Roborace_reveal_the_Robocar_live_at_MWC17",
	                    formats: [ "pdf", "docx" ]
	                }, {
	                    date: "18 Feb 2017",
	                    title: "Roborace becomes first company in driverless history to race two cars simultaneously in front of a live audience",
	                    fileName: "ROBORACE_RACE_TWO_CARS",
	                    formats: [ "pdf", "docx" ]
	                }, {
	                    date: "15 dec 2016",
	                    title: "Roborace: Michelin partnership announcement",
	                    fileName: "Roborace_Michelin_Announcement",
	                    formats: [ "pdf", "docx" ]
	                }, {
	                    date: "22 Aug 2016",
	                    title: "Roborace unveils “DevBot”&nbsp;— an&nbsp;intelligent, robotic, driverless, electric&nbsp;development&nbsp;car",
	                    fileName: "ROBORACE_UNVEILS_DEVBOT",
	                    formats: [ "pdf", "docx" ]
	                }, {
	                    date: "30 Mar 2016",
	                    title: "The car of&nbsp;the&nbsp;future is&nbsp;here",
	                    fileName: "THE_CAR_OF_THE_FUTURE_IS_HERE",
	                    formats: [ "pdf", "docx" ]
	                } ]
	            }));
	            __line = 56;
	            __append("\n                <h2>Media Assets</h2>\n                <h3>Roborace&nbsp;— Driverless car makes History on&nbsp;the streets of&nbsp;Paris</h3>\n                    ");
	            __line = 59;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    previewLink: "Paris_4K.jpg",
	                    preview: "Paris_preview.jpg",
	                    links: [ {
	                        url: "Paris_2K.jpg",
	                        text: "2000 &times; 1125"
	                    }, {
	                        url: "Paris_4K.jpg",
	                        text: "4000 &times; 2250"
	                    } ]
	                } ]
	            }));
	            __line = 78;
	            __append("\n                <h3>Roborace reveal the world’s first driverless electric race car live at Mobile World congress</h3>\n                   ");
	            __line = 80;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    previewLink: "MWC17_09_2K.jpg",
	                    preview: "MWC17_09_preview.jpg",
	                    links: [ {
	                        url: "MWC17_09_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_09_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_02_2K.jpg",
	                    preview: "MWC17_02_preview.jpg",
	                    links: [ {
	                        url: "MWC17_02_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_02_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_01_2K.jpg",
	                    preview: "MWC17_01_preview.jpg",
	                    links: [ {
	                        url: "MWC17_01_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_01_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_03_2K.jpg",
	                    preview: "MWC17_03_preview.jpg",
	                    links: [ {
	                        url: "MWC17_03_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_03_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_04_2K.jpg",
	                    preview: "MWC17_04_preview.jpg",
	                    links: [ {
	                        url: "MWC17_04_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_04_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_05_2K.jpg",
	                    preview: "MWC17_05_preview.jpg",
	                    links: [ {
	                        url: "MWC17_05_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_05_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_06_2K.jpg",
	                    preview: "MWC17_06_preview.jpg",
	                    links: [ {
	                        url: "MWC17_06_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_06_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "MWC17_07_2K.jpg",
	                    preview: "MWC17_07_preview.jpg",
	                    links: [ {
	                        url: "MWC17_07_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "MWC17_07_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                } ]
	            }));
	            __line = 205;
	            __append("\n                <h3>Roborace becomes first company in driverless history to race two cars simultaneously in front of a live audience</h3>\n                   ");
	            __line = 207;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    previewLink: "BA_Devbot_4K.jpg",
	                    preview: "BA_Devbot_preview.jpg",
	                    links: [ {
	                        url: "BA_Devbot_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "BA_Devbot_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "BA_Devbot2_4K.jpg",
	                    preview: "BA_Devbot2_preview.jpg",
	                    links: [ {
	                        url: "BA_Devbot2_2K.jpg",
	                        text: "2000 &times; 1333"
	                    } ]
	                }, {
	                    previewLink: "BA_Devbot3_4K.jpg",
	                    preview: "BA_Devbot3_preview.jpg",
	                    links: [ {
	                        url: "BA_Devbot3_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "BA_Devbot3_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                }, {
	                    previewLink: "BA_Devbot4_4K.jpg",
	                    preview: "BA_Devbot4_preview.jpg",
	                    links: [ {
	                        url: "BA_Devbot4_2K.jpg",
	                        text: "2000 &times; 1333"
	                    }, {
	                        url: "BA_Devbot4_4K.jpg",
	                        text: "4000 &times; 2667"
	                    } ]
	                } ]
	            }));
	            __line = 266;
	            __append("\n                <h3>Roborace: Michelin partnership announcement</h3>\n                  ");
	            __line = 268;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    previewLink: "Michelin_Master_4K.jpg",
	                    preview: "Michelin_Master_preview.jpg",
	                    links: [ {
	                        url: "Michelin_Master_2K.jpg",
	                        text: "2000 &times; 1125"
	                    }, {
	                        url: "Michelin_Master_4K.jpg",
	                        text: "4000 &times; 2250"
	                    } ]
	                } ]
	            }));
	            __line = 287;
	            __append("\n                <h3>Roborace unveils “DevBot”&nbsp;— an&nbsp;intelligent, robotic, driverless, electric&nbsp;development&nbsp;car</h3>\n                   ");
	            __line = 289;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    previewLink: "Sam_Christmas_1_4000.jpg",
	                    preview: "Sam_Christmas_1_preview.jpg",
	                    links: [ {
	                        url: "Sam_Christmas_1_4000.jpg",
	                        text: "4000 &times; 2250"
	                    }, {
	                        url: "Sam_Christmas_1_2000.jpg",
	                        text: "2000 &times; 1125"
	                    } ]
	                }, {
	                    previewLink: "Sam_Christmas_2_4000.jpg",
	                    preview: "Sam_Christmas_2_preview.jpg",
	                    links: [ {
	                        url: "Sam_Christmas_2_4000.jpg",
	                        text: "4000 &times; 2250"
	                    }, {
	                        url: "Sam_Christmas_2_2000.jpg",
	                        text: "2000 &times; 1125"
	                    } ]
	                }, {
	                    previewLink: "Sam_Christmas_3_1024.jpg",
	                    preview: "Sam_Christmas_3_preview.jpg",
	                    links: [ {
	                        url: "Sam_Christmas_3_1024.jpg",
	                        text: "1024 &times; 576"
	                    } ]
	                }, {
	                    previewLink: "Sam_Christmas_4_1024.jpg",
	                    preview: "Sam_Christmas_4_preview.jpg",
	                    links: [ {
	                        url: "Sam_Christmas_4_1024.jpg",
	                        text: "1024 &times; 576"
	                    } ]
	                } ]
	            }));
	            __line = 341;
	            __append("\n                   <h3>Roborace unveils tech capabilities on the production version of the car</h3>\n                   ");
	            __line = 343;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    previewLink: "Roborace_Diagram02_2000px.jpg",
	                    preview: "Roborace_Diagram02_preview.jpg",
	                    links: [ {
	                        url: "Roborace_Diagram02_2000px.jpg",
	                        text: "2000 &times; 1125"
	                    }, {
	                        url: "Roborace_Diagram02_1200px.jpg",
	                        text: "1200 &times; 675"
	                    } ]
	                } ]
	            }));
	            __line = 361;
	            __append("\n                <h3>Photo</h3>\n                ");
	            __line = 363;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    title: "Denis Sverdlov, CEO",
	                    previewLink: "denis-sverdlov.jpg",
	                    preview: "denis-sverdlov-preview.jpg",
	                    links: [ {
	                        url: "denis-sverdlov.jpg",
	                        text: "4400 &times; 2770"
	                    } ]
	                }, {
	                    title: "Daniel Simon, CDO",
	                    previewLink: "daniel-simon.jpg",
	                    preview: "daniel-simon-preview.jpg",
	                    links: [ {
	                        url: "daniel-simon.jpg",
	                        text: "2000 &times; 1333"
	                    } ]
	                }, {
	                    previewLink: "Michelin_Master_4K.jpg",
	                    preview: "Michelin_Master_preview.jpg",
	                    links: [ {
	                        url: "Michelin_Master_2K.jpg",
	                        text: "2000 &times; 1125"
	                    }, {
	                        url: "Michelin_Master_4K.jpg",
	                        text: "4000 &times; 2250"
	                    } ]
	                }, {
	                    previewLink: "Michelin_Fisheye_4K.jpg",
	                    preview: "Michelin_Fisheye_preview.jpg",
	                    links: [ {
	                        url: "Michelin_Fisheye_2K.jpg",
	                        text: "2000 &times; 1125"
	                    }, {
	                        url: "Michelin_Fisheye_4K.jpg",
	                        text: "4000 &times; 2250"
	                    } ]
	                } ]
	            }));
	            __line = 419;
	            __append("\n                <h3>Video</h3>\n                ");
	            __line = 421;
	            __append(include("photos", {
	                urlBase: "../static/i/media/",
	                photos: [ {
	                    title: "Roborace reveal the world’s first driverless electric race car <br />live at Mobile World congress",
	                    previewLink: "RoboRace_360_1.mp4",
	                    preview: "MWC17_video_preview.jpg",
	                    links: [ {
	                        url: "RoboRace_360_1.mp4",
	                        text: "MP4"
	                    } ]
	                }, {
	                    title: "Roborace: Michelin partnership announcement",
	                    previewLink: "https://www.youtube.com/watch?v=26zawxa1xis",
	                    preview: "michelin_mov_preview.jpg",
	                    links: [ {
	                        url: "michelan.mov",
	                        text: "MOV"
	                    } ]
	                }, {
	                    title: "DevBot for Roborace",
	                    previewLink: "devbot_roborace.mov",
	                    preview: "devbot_roborace_preview.jpg",
	                    links: [ {
	                        url: "devbot_roborace.mov",
	                        text: "MOV"
	                    } ]
	                }, {
	                    title: "Robocar Video Teaser",
	                    previewLink: "robocar_teaser.mov",
	                    preview: "video-robocar-teaser-preview.jpg",
	                    links: [ {
	                        url: "robocar_teaser.mov",
	                        text: "MOV"
	                    } ]
	                } ]
	            }));
	            __line = 469;
	            __append('\n                <h2>Brand identity</h2>\n                <div class="section logos">\n                    <div class="h">\n                        ');
	            __line = 473;
	            __append(include("logo", {
	                color: "white",
	                urlBase: "../static/i/media/",
	                name: "logo-h-w"
	            }));
	            __append("\n                        ");
	            __line = 474;
	            __append(include("logo", {
	                color: "black",
	                urlBase: "../static/i/media/",
	                name: "logo-h"
	            }));
	            __append('\n                    </div>\n                    <div class="v">\n                        ');
	            __line = 477;
	            __append(include("logo", {
	                color: "white",
	                urlBase: "../static/i/media/",
	                name: "logo-v-w"
	            }));
	            __append("\n                        ");
	            __line = 478;
	            __append(include("logo", {
	                color: "black",
	                urlBase: "../static/i/media/",
	                name: "logo-v"
	            }));
	            __append("\n                    </div>\n                </div>\n\n            </div>\n        </div>\n");
	            __line = 484;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 77 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="<%= color %>">\n    <div class="pic"><a href="<%= urlBase + name  +  \'.png\' %>"><img src="<%= urlBase + name  +  \'.png\' %>"/></a></div>\n    <div class="types">\n        <span class="label">Download:</span>\n        <a href="<%= urlBase + name  +  \'.png\' %>">PNG</a><a href="<%= urlBase + name  +  \'.svg\' %>">SVG</a>\n    </div>\n</div>', __filename = "src/tmpl/media/logo.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="');
	            __append(escapeFn(color));
	            __append('">\n    <div class="pic"><a href="');
	            __line = 2;
	            __append(escapeFn(urlBase + name + ".png"));
	            __append('"><img src="');
	            __append(escapeFn(urlBase + name + ".png"));
	            __append('"/></a></div>\n    <div class="types">\n        <span class="label">Download:</span>\n        <a href="');
	            __line = 5;
	            __append(escapeFn(urlBase + name + ".png"));
	            __append('">PNG</a><a href="');
	            __append(escapeFn(urlBase + name + ".svg"));
	            __append('">SVG</a>\n    </div>\n</div>');
	            __line = 7;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 78 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="section photos">\n    <% photos.forEach( function(photo){ %>\n        <div class="photo">\n            <div class="title"><%- photo.title %></div>\n            <div class="pic">\n                <a href="<%= (photo.previewLink.indexOf(\'http\') === -1) ? urlBase + photo.previewLink : photo.previewLink  %>"><img src="<%= urlBase + photo.preview %>"/></a>\n            </div>\n            <div class="links"><span class="label">Download:</span>\n                <% photo.links.forEach( function(link){%>\n                    <a class="link" href="<%= urlBase + link.url %>"><%- link.text %></a>\n                <% }) %>\n            </div>\n        </div>\n    <% }) %>\n</div>', __filename = "src/tmpl/media/photos.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="section photos">\n    ');
	            __line = 2;
	            photos.forEach(function(photo) {
	                __append('\n        <div class="photo">\n            <div class="title">');
	                __line = 4;
	                __append(photo.title);
	                __append('</div>\n            <div class="pic">\n                <a href="');
	                __line = 6;
	                __append(escapeFn(photo.previewLink.indexOf("http") === -1 ? urlBase + photo.previewLink : photo.previewLink));
	                __append('"><img src="');
	                __append(escapeFn(urlBase + photo.preview));
	                __append('"/></a>\n            </div>\n            <div class="links"><span class="label">Download:</span>\n                ');
	                __line = 9;
	                photo.links.forEach(function(link) {
	                    __append('\n                    <a class="link" href="');
	                    __line = 10;
	                    __append(escapeFn(urlBase + link.url));
	                    __append('">');
	                    __append(link.text);
	                    __append("</a>\n                ");
	                    __line = 11;
	                });
	                __append("\n            </div>\n        </div>\n    ");
	                __line = 14;
	            });
	            __append("\n</div>");
	            __line = 15;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 79 */
/***/ (function(module, exports) {

	module.exports = function anonymous(locals, escapeFn, include, rethrow) {
	    rethrow = rethrow || function rethrow(err, str, flnm, lineno, esc) {
	        var lines = str.split("\n");
	        var start = Math.max(lineno - 3, 0);
	        var end = Math.min(lines.length, lineno + 3);
	        var filename = esc(flnm);
	        var context = lines.slice(start, end).map(function(line, i) {
	            var curr = i + start + 1;
	            return (curr == lineno ? " >> " : "    ") + curr + "| " + line;
	        }).join("\n");
	        err.path = filename;
	        err.message = (filename || "ejs") + ":" + lineno + "\n" + context + "\n\n" + err.message;
	        throw err;
	    };
	    escapeFn = escapeFn || function(markup) {
	        return markup == undefined ? "" : String(markup).replace(_MATCH_HTML, encode_char);
	    };
	    var _ENCODE_HTML_RULES = {
	        "&": "&amp;",
	        "<": "&lt;",
	        ">": "&gt;",
	        '"': "&#34;",
	        "'": "&#39;"
	    }, _MATCH_HTML = /[&<>'"]/g;
	    function encode_char(c) {
	        return _ENCODE_HTML_RULES[c] || c;
	    }
	    var __line = 1, __lines = '<div class="section releases">\n    <% releases.forEach( function(release){ %>\n        <div class="release">\n            <div class="date"><%= release.date %></div>\n            <div class="title"><a class="link" href="<%= urlBase + release.fileName + \'.pdf\' %>"><%- release.title %></a></div>\n            <div class="links"><span class="label">Download:</span>\n                <% release.formats.forEach( function(format){%>\n                    <a class="link" href="<%= urlBase + release.fileName + \'.\' + format %>"><img class="icon" src="<%= \'../static/i/\' + format + \'.svg\' %>" /></a>\n                <% }) %>\n            </div>\n        </div>\n    <% }) %>\n</div>', __filename = "src/tmpl/media/releases.ejs";
	    try {
	        var __output = [], __append = __output.push.bind(__output);
	        with (locals || {}) {
	            __append('<div class="section releases">\n    ');
	            __line = 2;
	            releases.forEach(function(release) {
	                __append('\n        <div class="release">\n            <div class="date">');
	                __line = 4;
	                __append(escapeFn(release.date));
	                __append('</div>\n            <div class="title"><a class="link" href="');
	                __line = 5;
	                __append(escapeFn(urlBase + release.fileName + ".pdf"));
	                __append('">');
	                __append(release.title);
	                __append('</a></div>\n            <div class="links"><span class="label">Download:</span>\n                ');
	                __line = 7;
	                release.formats.forEach(function(format) {
	                    __append('\n                    <a class="link" href="');
	                    __line = 8;
	                    __append(escapeFn(urlBase + release.fileName + "." + format));
	                    __append('"><img class="icon" src="');
	                    __append(escapeFn("../static/i/" + format + ".svg"));
	                    __append('" /></a>\n                ');
	                    __line = 9;
	                });
	                __append("\n            </div>\n        </div>\n    ");
	                __line = 12;
	            });
	            __append("\n</div>");
	            __line = 13;
	        }
	        return __output.join("");
	    } catch (e) {
	        rethrow(e, __lines, __filename, __line, escapeFn);
	    }
	}

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var section = __webpack_require__(81);

	var $menu = $('.menu');
	var $menuItems = $menu.find('.menu-item');

	var activeItem = 0;
	var _isActive = true;
	var startTimeout = null;
	var _wasUserIteraction = false;

	var needAutoStart = false;

	var config = {
	    timeLong: 8,
	    timeNormal: 0.5,
	    timeFast: 0.1
	};

	var checkMenuHasScroll = function checkMenuHasScroll() {
	    return $menu.get(0).scrollWidth > $menu.width();
	};

	var _menuHasScroll = checkMenuHasScroll();

	var makeActive = function makeActive(item, skipScrollWindowTo) {

	    var target = null;
	    if (typeof item === 'string') {
	        target = $menu.find('.menu-item-' + item).get(0);
	    } else {
	        target = $(item).get(0);
	    }

	    /* menuItemsDOM.forEach((item) => {
	         item.classList.remove('is-active');
	     })
	     target.classList.add('is-active');*/
	    activeItem = $(target).index();
	    if (!skipScrollWindowTo) {
	        var id = $(target).find('.menu-item-a').attr('href').replace('#', '');
	        section.scrollTo('#' + id, config.timeNormal, function () {
	            _wasUserIteraction = false;
	        });
	    }
	};

	var fastScrollTo = function fastScrollTo(item) {
	    var cb = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
	    var skipScrollWindowTo = arguments[2];

	    var scrollItem = 0;
	    if (typeof item === 'number') {
	        scrollItem = $menuItems.eq(item);
	    } else if (typeof item === 'string') {
	        scrollItem = $menu.find('.menu-item-' + item);
	    } else {
	        scrollItem = item;
	    }

	    var position = scrollItem.position() || {};

	    TweenLite.to($menu.get(0), config.timeFast, {
	        scrollTo: { x: position.left },
	        ease: Power0.easeNone,
	        onComplete: function onComplete() {
	            makeActive(scrollItem, skipScrollWindowTo);
	            cb();
	        }
	    });
	};

	var startAnimation = function startAnimation() {
	    var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : activeItem;

	    if (index < $menuItems.length - 1) {

	        var scrollItem = $menuItems.eq(index + 1);
	        var position = scrollItem.position();

	        TweenLite.to($menu.get(0), config.timeLong, {
	            scrollTo: { x: position.left },
	            ease: Power0.easeNone,
	            onComplete: function onComplete() {
	                makeActive(scrollItem);
	                activeItem = index + 1;
	                if (_isActive && activeItem < $menuItems.length - 1) {
	                    startAnimation(activeItem);
	                } else {
	                    _isActive = false;
	                }
	            }
	        });
	    }
	};

	var stop = function stop() {
	    _isActive = false;
	    TweenLite.killTweensOf($menu.get(0));
	    clearTimeout(startTimeout);
	    var scrollItem = $menuItems.eq(activeItem);
	    var position = scrollItem.position();

	    TweenLite.to($menu.get(0), config.timeFast, {
	        scrollTo: { x: position.left },
	        ease: Power0.easeNone,
	        onComplete: function onComplete() {
	            makeActive(scrollItem, true);
	        }
	    });
	};

	var start = function start() {
	    if (_menuHasScroll) {
	        _isActive = true;
	        startAnimation();
	    }
	};

	var init = function init() {
	    $menu.on('click', '.menu-item', function (e) {
	        _wasUserIteraction = true;

	        e.preventDefault();
	        _isActive = false;

	        TweenLite.killTweensOf($menu.get(0));
	        fastScrollTo($(e.currentTarget), function () {
	            if (needAutoStart) {
	                startTimeout = setTimeout(start, config.timeLong * 1000);
	            }
	        });
	    });

	    $menu.on('touchstart', function () {
	        if (_isActive) {
	            _isActive = false;
	            TweenLite.killTweensOf($menu.get(0));
	        }
	        if (needAutoStart) {
	            clearTimeout(startTimeout);
	        }
	    });
	    if (needAutoStart) {
	        startTimeout = setTimeout(start, 2000);
	    }

	    $(window).on('orientationchange resize', function () {
	        _menuHasScroll = checkMenuHasScroll();
	    });
	};

	module.exports = {
	    init: init,
	    start: start,
	    stop: stop,
	    makeActive: makeActive,
	    fastScrollTo: fastScrollTo,
	    isActive: function isActive() {
	        return _isActive;
	    },
	    menuHasScroll: function menuHasScroll() {
	        return _menuHasScroll;
	    },
	    wasUserIteraction: function wasUserIteraction() {
	        return _wasUserIteraction;
	    }
	};

/***/ }),
/* 81 */
/***/ (function(module, exports) {

	'use strict';

	var $sectionsContainer = $('.sections');

	var scrollTo = function scrollTo(target, duration) {
	    var cb = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};

	    TweenLite.to($sectionsContainer.get(0), duration, {
	        scrollTo: target,
	        onComplete: function onComplete() {
	            cb();
	        }
	    });
	};

	module.exports = {
	    scrollTo: scrollTo
	};

/***/ }),
/* 82 */
/***/ (function(module, exports) {

	'use strict';

	var nameField = {
	    label: 'Name',
	    id: 'name',
	    autocorrect: false,
	    autocomplete: false,
	    autocapitalize: true
	};

	var emailField = {
	    label: 'Email',
	    id: 'email',
	    type: 'email',
	    autocorrect: false,
	    autocomplete: false,
	    autocapitalize: false
	};

	var captchaStep = {
	    id: 'captcha',
	    textFields: [{
	        label: 'Hi, it\u2019s Roborace AI speaking.<br />I&nbsp;believe&nbsp;you&nbsp;are one of us. Aren\u2019t&nbsp;you?<br /><br />%%captcha%% is:',
	        class: 'js-captcha',
	        id: 'captcha',
	        placeholder: '123',
	        autocorrect: false,
	        autocomplete: false,
	        autocapitalize: false
	    }],
	    hidden: {
	        id: 'captcha-code',
	        class: 'js-captcha-code'
	    },
	    actions: {
	        primary: {
	            text: 'Submit',
	            class: ' js-submit'
	        }
	    }
	};

	var DoneStep = function DoneStep() {
	    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'done';
	    var textContent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'We\u2019ve got your message<br />and will contact you soon';

	    return {
	        id: id,
	        header: false,
	        textContent: textContent,
	        social: { text: 'Also follow us' },
	        actions: {
	            primary: {
	                text: 'Ok',
	                enabled: true,
	                class: ' js-close'
	            }
	        }
	    };
	};

	var errorStep = {
	    id: 'error',
	    textContent: 'Something went wrong<br/> Please, try again',
	    actions: {
	        primary: {
	            text: 'Try Again',
	            enabled: true,
	            goTo: 'step1',
	            class: ' js-back'
	        }
	    }
	};

	var topics = {
	    'join-us-as-news-subscribe': {
	        topic: 'News Subscription',
	        topicValue: 'subscribe',
	        steps: [{
	            id: 'step1',
	            textFields: [nameField, emailField],
	            actions: {
	                primary: {
	                    text: 'Subscribe',
	                    class: ' js-submit'
	                }
	            }
	        }, captchaStep, new DoneStep('done', 'Looking forward<br />to sending you exciting news'), new DoneStep('captcha-done', 'Sorry, my bad. Your message will shortly be read by friendly humans.'), errorStep]
	    },
	    'join-us-as-partner-or-sponsor': {
	        topic: 'Partnership Request',
	        topicValue: 'partnership',
	        steps: [{
	            id: 'step1',
	            textFields: [nameField, emailField],
	            radioGroup: {
	                id: 'partner-type',
	                items: [{
	                    text: 'Sponsor'

	                }, {
	                    text: 'Technology provider'
	                }, {
	                    text: 'Marketing partner'
	                }, {
	                    text: 'Other'
	                }]
	            },
	            actions: {
	                primary: {
	                    text: 'Next',
	                    class: 'js-next',
	                    goTo: 'step2'
	                },
	                secondary: {
	                    class: 'js-back'
	                }
	            }
	        }, {
	            id: 'step2',
	            textarea: {
	                id: 'message',
	                label: 'How do you imagine our&nbsp;future&nbsp;together?'
	            },
	            actions: {
	                primary: {
	                    text: 'Submit',
	                    class: ' js-submit'
	                },
	                secondary: {
	                    class: 'js-back',
	                    goTo: "step1"
	                }
	            }
	        }, captchaStep, new DoneStep(), new DoneStep('captcha-done', 'Sorry, my bad. Your message will shortly be read by friendly humans.'), errorStep]
	    },
	    'join-us-as-media': {
	        topic: 'Media Request',
	        topicValue: 'press',
	        steps: [{
	            id: 'step1',
	            textFields: [nameField, emailField],
	            actions: {
	                primary: {
	                    text: 'Next',
	                    class: 'js-next',
	                    goTo: 'step2'
	                },
	                secondary: {
	                    class: 'js-back'
	                }
	            }
	        }, {
	            id: 'step2',
	            textFields: [{
	                label: 'Media name',
	                id: 'media-name',
	                autocorrect: false,
	                autocomplete: false,
	                autocapitalize: false
	            }, {
	                label: 'Media website',
	                type: 'url',
	                placeholder: 'http://',
	                id: 'media-website'
	            }],
	            actions: {
	                primary: {
	                    text: 'Next',
	                    class: 'js-next',
	                    goTo: 'step3'
	                },
	                secondary: {
	                    class: 'js-back',
	                    goTo: "step1"
	                }
	            }
	        }, {
	            id: 'step3',
	            textarea: {
	                id: 'message',
	                label: 'What would you like to know?'
	            },
	            actions: {
	                primary: {
	                    text: 'Submit',
	                    class: ' js-submit'
	                },
	                secondary: {
	                    class: 'js-back',
	                    goTo: "step2"
	                }
	            }
	        }, captchaStep, new DoneStep(), new DoneStep('captcha-done', 'Sorry, my bad. Your message will shortly be read by friendly humans.'), errorStep]
	    },
	    'join-us-as-team-or-roboracer': {
	        topic: 'Participation Request',
	        topicValue: 'apply',
	        steps: [{
	            id: 'step1',
	            textFields: [nameField, emailField, {
	                label: 'Team name',
	                id: 'team-name'
	            }, {
	                label: 'Team website link',
	                id: 'team-website',
	                type: 'url',
	                placeholder: 'http://'
	            }],
	            actions: {
	                primary: {
	                    text: 'Next',
	                    class: 'js-next',
	                    goTo: 'step2'
	                },
	                secondary: {
	                    class: 'js-back'
	                }
	            }
	        }, {
	            id: 'step2',
	            line: 0,
	            textarea: {
	                id: 'message',
	                label: 'What makes your team outstanding?'
	            },
	            actions: {
	                primary: {
	                    text: 'Submit',
	                    class: ' js-submit'
	                },
	                secondary: {
	                    class: 'js-back',
	                    goTo: "step1"
	                }
	            }
	        }, captchaStep, new DoneStep(), new DoneStep('captcha-done', 'Sorry, my bad. Your message will shortly be read by friendly humans.'), errorStep]
	    },
	    'join-us-as-employee': {
	        topic: 'Job Request',
	        topicValue: 'work',
	        steps: [{
	            id: 'step1',
	            textFields: [nameField, emailField],
	            actions: {
	                primary: {
	                    text: 'Next',
	                    class: 'js-next',
	                    goTo: 'step2'
	                },
	                secondary: {
	                    class: 'js-back'
	                }
	            }
	        }, {
	            id: 'step2',
	            textFields: [{
	                label: 'Desired position',
	                id: 'desired-position'
	            }, {
	                label: 'Professional profile',
	                type: 'url',
	                placeholder: 'LinkedIn, Viadeo, Xing etc.',
	                id: 'cv-link'
	            }],
	            actions: {
	                primary: {
	                    text: 'Next',
	                    class: 'js-next',
	                    goTo: 'step3'
	                },
	                secondary: {
	                    class: 'js-back',
	                    goTo: "step1"
	                }
	            }
	        }, {
	            id: 'step3',
	            textarea: {
	                id: 'message',
	                label: 'Impress us'
	            },
	            actions: {
	                primary: {
	                    text: 'Submit',
	                    class: ' js-submit'
	                },
	                secondary: {
	                    class: 'js-back',
	                    goTo: "step2"
	                }
	            }
	        }, captchaStep, new DoneStep(), new DoneStep('captcha-done', 'Sorry, my bad. Your message will shortly be read by friendly humans.'), errorStep]
	    }
	};

	var data = {
	    topics: topics,
	    content: [{
	        type: 'custom',
	        alias: 'about',
	        title: 'about',
	        menuHidden: true,
	        imagePreview: true,
	        header: 'roborace',
	        pic: ['intro1.jpg@w', 'intro2.jpg@w'],
	        content: {
	            title: ''
	        }
	    }, {
	        alias: 'intro',
	        imagePreview: true,
	        title: 'latest news',
	        header: "Latest&nbsp;News",
	        backgroundVideo: {
	            mp4: 'v2_rc_testing.mp4',
	            ogv: 'v2_rc_testing.ogv',
	            webm: 'v2_rc_testing.webm',
	            poster: 'v2_rc_testing.jpg'
	        }
	    }, {
	        alias: 'robocar',
	        imagePreview: true,
	        title: 'robocar',
	        header: "Roborace",
	        pic: ['robocar-slide-optimised-8.jpg@b', '2017-07-19-photo.jpg@w', 'robocar-sam-christmas-0.jpg@w', 'robocar-barcelona-0.jpg@w', 'sensors.jpg@w'],
	        video: '-U-PLCtn_7M',
	        content: {
	            text: '<p> World’s first competition for human + machine teams, using both self-driving and manually-controlled cars. It is a new platform for brands, organizations and individuals to test the development of their automated driving systems. <br /><br />Race formats will feature new forms of immersive entertainment to engage the public’s imagination. Through sport, innovations in autonomous technology will be advanced, accelerating improvements to road safety.</p> ROBOCAR <br><br> The world’s first driverless electric racing car. Designed by Daniel Simon, known for his work on Hollywood films such as Tron: Legacy, Oblivion and Captain America.</p>',
	            textkey: "value",
	            videoText: 'Watch video'
	        }
	    }, {
	        alias: 'devbot',
	        video: 'xrfCQ7qwPh8',
	        imagePreview: true,
	        title: 'devbot',
	        pic: ['sam-6.jpg@w', 'sam-4.jpg@b'],
	        content: {
	            text: '<p>The primary purpose of&nbsp;the DevBot is&nbsp;to&nbsp;allow teams to&nbsp;develop their software and experience the hardware that will be&nbsp;used on&nbsp;the "Robocar".</p><p>Unlike the Robocar the DevBot has a&nbsp;cabin that can be&nbsp;driven by&nbsp;a&nbsp;human or&nbsp;a&nbsp;computer allowing teams to&nbsp;fully understand how the car thinks and feels on&nbsp;a&nbsp;racetrack alongside the comprehensive real-time data.</p>',
	            title: 'Our development car',
	            videoText: 'Watch devbot'
	        }
	    }, {
	        alias: 'stories',
	        pic: 'inside.jpg',
	        title: 'stories',
	        header: 'What is Roborace?',
	        content: {
	            title: 'Human + Machine teams working together to advance autonomous driving technology',
	            videoList: [{
	                title: 'What is Roborace?',
	                video: 'hiBJinX3fy8'
	            }, {
	                title: 'Goodwood Hillclimb',
	                video: 'Ucrh1XTR-bE'
	            }, {
	                title: 'Goodwood FOS',
	                video: 'RHYV1tDla9s'
	            }, {
	                title: 'The future of Racing',
	                video: '1dPtgaEnzI4'
	            }, {
	                title: 'Pro Drifter vs Autonomous Race Car',
	                video: 'kh00GAx66sA'
	            }]
	        }
	    }, {
	        type: 'custom',
	        alias: 'partners',
	        bgLoopPartner: true,
	        imagePreview: true,
	        title: 'partners',
	        pic: ['nvidia.jpg@w', 'michelin.jpg@w', 'arrival.jpg@w'],
	        content: {
	            link: "Become a Partner",
	            'partners': [{
	                title: "Nvidia",
	                alias: "nvidia",
	                link: "http://nvidia.com/"
	            }, {
	                logo: 'michelin-logo.svg',
	                link: "http://www.michelin.com/",
	                title: "Michelin",
	                alias: "michelin"
	            }, {
	                title: "ARRIVAL",
	                alias: "ARRIVAL"
	            }]
	        }
	    },
	    /* {
	        type: 'custom',
	        imagePreview: true,
	        alias: 'follow',
	        title: 'follow',
	        inputText: 'your@email',
	        inputButton: 'send',
	        topic: 'News Subscription',
	        topicValue: 'subscribe',
	        done: 'Looking forward<br />to sending you exciting news',
	        error: 'Something went wrong<br/> Please, try again',
	        pic: ['sam-2.jpg@w', 'sam-1.jpg@w', 'sam-3.jpg@w', 'sam-5.jpg@b', 'devbot-hk.jpg@w']
	    },
	    */
	    {
	        type: 'custom',
	        alias: 'joinus',
	        title: 'Join us',
	        header: 'Join us',
	        contactEmail: 'hello@roborace.com',
	        content: []
	        /*
	        {
	             type: 'custom',
	             alias: 'enquire',
	             title: 'enquire',
	             header: 'Enquire',
	             contactEmail: 'hello@roborace.com',
	             content: [
	                 {
	                     "href": "#enquire/join-us-as-team-or-roboracer/step1",
	                     "text": "Team"
	                 },
	                 {
	                     "href": "#enquire/join-us-as-partner-or-sponsor/step1",
	                     "text": "Partner or Sponsor"
	                 },
	                 {
	                     "href": "/media",
	                     "text": "Media"
	                 },
	                 {
	                     "href": "#enquire/join-us-as-employee/step1",
	                     "text": "Employee"
	                 }
	             ]
	         }
	         */
	    }],
	    description: 'Autonomous cars compete in driving algorithms on Formula E tracks',
	    shortTitle: 'Roborace',
	    title: 'Global championship of driverless cars'
	};

	module.exports = data;

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var menu = __webpack_require__(80);
	var debounce = __webpack_require__(46).debounce;
	var bgLoop = __webpack_require__(84);
	var $sections = $('.sections');
	var sectionsRaw = $('.sections').get(0);
	var $scrollTo = $('.scrollto');
	var hidden = $('.section-image-preview');
	var $modal = $('.modal');
	var $modalYoutube = $('.modal.modal-youtube');
	var $modalBg = $('.modal-bg');

	window.needToPreventRepaint = false;

	var close = function close() {
	    $modal.removeClass('is-visible');
	    $modalBg.removeClass('modal-image');
	};

	var init = function init() {
	    $sections.scrollTop(0);

	    var timer = null;

	    $sections.on('touchstart wheel', function () {
	        if (menu.isActive()) {
	            menu.stop();
	        }
	        $sections.off('touchstart wheel', function () {});
	    });

	    $modal.on('click', '.modal-close', close);
	    $modal.on('click', '.modal-image', close);

	    var makeSectionsPositions = function makeSectionsPositions() {
	        return $scrollTo.map(function (i, item) {
	            var $item = $(item);
	            var windowHeight = $(window).height();
	            return {
	                windowHeight: windowHeight,
	                top: $item.offset().top,
	                needBgUpdate: $item.height() > windowHeight + 70,
	                bottom: $item.offset().top + $item.height(),
	                margin: $item.offset().top + $item.height() - windowHeight,
	                bg: $item.find('.section-bg'),
	                id: $item.attr('id')
	            };
	        }).get().reverse();
	    };

	    var sectionsPosition = makeSectionsPositions();

	    var prevCurrentActiveSection = null;

	    var sectionsActiveUpdate = function sectionsActiveUpdate() {
	        var position = sectionsRaw.scrollTop;

	        if (menu.wasUserIteraction()) {
	            return;
	        }

	        var currentActiveSection = sectionsPosition.find(function (item) {
	            return position >= item.top - item.windowHeight / 2 && position <= item.bottom + item.windowHeight / 2;
	        });

	        if (currentActiveSection && prevCurrentActiveSection != currentActiveSection.id) {
	            prevCurrentActiveSection = currentActiveSection.id;
	            if (!menu.isActive() && menu.menuHasScroll()) {
	                menu.fastScrollTo(currentActiveSection.id, undefined, true);
	            } else {
	                menu.makeActive(currentActiveSection.id, true);
	            }
	        }
	    };

	    var handleScrollComplete = debounce(function () {
	        window.needToPreventRepaint = false;
	        sectionsActiveUpdate();
	        bgLoop.start();
	    }, 100, true);

	    var handleScrollStart = debounce(function () {
	        window.needToPreventRepaint = true;
	        bgLoop.stop();
	    }, 100, true);

	    $sections.scroll(function () {
	        handleScrollStart();
	        clearTimeout(timer);
	        timer = setTimeout(function () {
	            handleScrollComplete();
	        }, 150);
	    });

	    $(window).on('orientationchange', function () {
	        if (!$modal.is('.is-visible')) {
	            setTimeout(function () {
	                window.location.reload();
	            }, 10);
	        }
	    });
	};
	module.exports = {
	    init: init
	};

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var sections = __webpack_require__(83);
	var menu = __webpack_require__(80);
	var Swipe = __webpack_require__(85);

	var $sections = $('.sections');

	var $bgContainer = $('.bg-loop');

	var loops = [];

	var init = function init() {
	    $bgContainer.each(function (index, item) {

	        var $item = $(item);
	        var $section = $item.parents('.section');
	        var $points = $section.find('.swipe-point');
	        var $pointsControl = $section.find('.swipe-points');
	        var $swipeClose = $section.find('.swipe-close.modal-close');

	        var colors = $pointsControl.data('colors');

	        var $controls = $section.find('.swipe-controls');
	        var $sectionI = $section.find('.section-i');
	        var $swipeWrap = $section.find('.swipe-wrap');

	        var $prevBtn = $(item).parents('.sections').find('.swipe-controls-btn-prev');
	        var $nextBtn = $(item).parents('.sections').find('.swipe-controls-btn-next');

	        var makePointActive = function makePointActive(index) {
	            $pointsControl.attr('data-color', colors[index]);
	            $points.removeClass('is-active');
	            $points.eq(index).addClass('is-active');

	            $controls.attr('data-color', colors[index]);
	            $swipeClose.attr('data-color', colors[index]);
	        };

	        var mySwipe = Swipe(item, {
	            startSlide: 0,
	            auto: 5000,
	            draggable: true,
	            disableScroll: true,
	            autoRestart: false,
	            continuous: true,
	            callback: makePointActive
	        });

	        loops.push(mySwipe);

	        var timer = null;

	        $swipeClose.on('click', function () {
	            resetControl();
	            unsetTimeout();
	            clearTimeout(addWrapUp);
	        });

	        var resetControl = function resetControl() {
	            $item.removeClass('swipe-up');
	            $sectionI.removeClass('swipe-visible');
	            $controls.removeClass('swipe-up-controls');
	            $sections.removeClass('with-image-preview');
	            $swipeWrap.removeClass('swipe-wrap-up');
	            mySwipe.restart();
	        };

	        var setControl = function setControl() {
	            menu.makeActive($section.attr('id'));
	            $item.addClass('swipe-up');
	            $sections.addClass('with-image-preview');
	            $sectionI.addClass('swipe-visible');
	            $controls.addClass('swipe-up-controls');
	            setTimeout(addWrapUp, 700);
	            makePointActive(mySwipe.getPos());

	            timer = setTimer();
	        };

	        var addWrapUp = function addWrapUp() {
	            $swipeWrap.addClass('swipe-wrap-up');
	        };

	        var setTimer = function setTimer() {
	            return setTimeout(resetControl, 10000);
	        };

	        var unsetTimeout = function unsetTimeout() {
	            clearTimeout(timer);
	            timer = null;
	        };

	        $section.find('.section-image-preview').on('click', function () {
	            setControl();
	        });

	        $section.find('.section-text').on('click', function () {
	            setControl();
	        });

	        $swipeWrap.find('.bg-pic').on('click touchstart', function () {
	            unsetTimeout();
	            timer = setTimer();
	        });

	        $prevBtn.on('click touchstart', function (e) {
	            e.preventDefault();

	            mySwipe.prev();
	            unsetTimeout();
	            timer = setTimer();
	            mySwipe.restart();
	        });

	        $nextBtn.on('click touchstart', function (e) {
	            e.preventDefault();

	            mySwipe.next();
	            unsetTimeout();
	            timer = setTimer();
	            mySwipe.restart();
	        });
	    });
	};

	var stop = function stop() {
	    loops.forEach(function (item) {
	        item.stop();
	    });
	};

	var start = function start() {
	    loops.forEach(function (item) {
	        item.restart();
	    });
	};

	module.exports = {
	    init: init,
	    start: start,
	    stop: stop,
	    loops: loops
	};

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(global) {/*!
	 * Swipe 2.2.11
	 *
	 * Brad Birdsall
	 * Copyright 2013, MIT License
	 *
	*/

	// if the module has no dependencies, the above pattern can be simplified to
	// eslint-disable-next-line no-extra-semi
	;(function (root, factory) {
	  // eslint-disable-next-line no-undef
	  if (true) {
	    // AMD. Register as an anonymous module.
	    // eslint-disable-next-line no-undef
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function(){
	      root.Swipe = factory();
	      return root.Swipe;
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof module === 'object' && module.exports) {
	    // Node. Does not work with strict CommonJS, but
	    // only CommonJS-like environments that support module.exports,
	    // like Node.
	    module.exports = factory();
	  } else {
	    // Browser globals
	    root.Swipe = factory();
	  }
	}(this, function () {
	  // Establish the root object, `window` (`self`) in the browser, `global`
	  // on the server, or `this` in some virtual machines. We use `self`
	  // instead of `window` for `WebWorker` support.
	  var root = typeof self == 'object' && self.self === self && self ||
	             typeof global == 'object' && global.global === global && global ||
	             this;

	  var _document = root.document;

	  function Swipe(container, options) {

	    'use strict';

	    options = options || {};

	    // setup initial vars
	    var start = {};
	    var delta = {};
	    var isScrolling;

	    // setup auto slideshow
	    var delay = options.auto || 0;
	    var interval;

	    var disabled = false;

	    // utilities
	    // simple no operation function
	    var noop = function() {};
	    // offload a functions execution
	    var offloadFn = function(fn) { setTimeout(fn || noop, 0); };
	    // Returns a function, that, as long as it continues to be invoked, will not
	    // be triggered.
	    var throttle = function (fn, threshhold) {
	      threshhold = threshhold || 100;
	      var timeout = null;

	      function cancel() {
	        if (timeout) clearTimeout(timeout);
	      }

	      function throttledFn() {
	        var context = this;
	        var args = arguments;
	        cancel();
	        timeout = setTimeout(function() {
	          timeout = null;
	          fn.apply(context, args);
	        }, threshhold);
	      }

	      // allow remove throttled timeout
	      throttledFn.cancel = cancel;

	      return throttledFn;
	    };

	    // check browser capabilities
	    var browser = {
	      addEventListener: !!root.addEventListener,
	      // eslint-disable-next-line no-undef
	      touch: ('ontouchstart' in root) || root.DocumentTouch && _document instanceof DocumentTouch,
	      transitions: (function(temp) {
	        var props = ['transitionProperty', 'WebkitTransition', 'MozTransition', 'OTransition', 'msTransition'];
	        for ( var i in props ) {
	          if (temp.style[ props[i] ] !== undefined){
	            return true;
	          }
	        }
	        return false;
	      })(_document.createElement('swipe'))
	    };

	    // quit if no root element
	    if (!container) return;

	    var element = container.children[0];
	    var slides, slidePos, width, length;
	    var index = parseInt(options.startSlide, 10) || 0;
	    var speed = options.speed || 300;
	    options.continuous = options.continuous !== undefined ? options.continuous : true;

	    // AutoRestart option: auto restart slideshow after user's touch event
	    options.autoRestart = options.autoRestart !== undefined ? options.autoRestart : false;

	    // throttled setup
	    var throttledSetup = throttle(setup);

	    // setup event capturing
	    var events = {

	      handleEvent: function(event) {
	        if (disabled) return;

	        switch (event.type) {
	          case 'mousedown':
	          case 'touchstart': this.start(event); break;
	          case 'mousemove':
	          case 'touchmove': this.move(event); break;
	          case 'mouseup':
	          case 'mouseleave':
	          case 'touchend': this.end(event); break;
	          case 'webkitTransitionEnd':
	          case 'msTransitionEnd':
	          case 'oTransitionEnd':
	          case 'otransitionend':
	          case 'transitionend': this.transitionEnd(event); break;
	          case 'resize': throttledSetup(); break;
	        }

	        if (options.stopPropagation) {
	          event.stopPropagation();
	        }
	      },

	      start: function(event) {
	        var touches;

	        if (isMouseEvent(event)) {
	          touches = event;
	          event.preventDefault(); // For desktop Safari drag
	        } else {
	          touches = event.touches[0];
	        }

	        // measure start values
	        start = {

	          // get initial touch coords
	          x: touches.pageX,
	          y: touches.pageY,

	          // store time to determine touch duration
	          time: +new Date()

	        };

	        // used for testing first move event
	        isScrolling = undefined;

	        // reset delta and end measurements
	        delta = {};

	        // attach touchmove and touchend listeners
	        if (isMouseEvent(event)) {
	          element.addEventListener('mousemove', this, false);
	          element.addEventListener('mouseup', this, false);
	          element.addEventListener('mouseleave', this, false);
	        } else {
	          element.addEventListener('touchmove', this, false);
	          element.addEventListener('touchend', this, false);
	        }

	      },

	      move: function(event) {
	        var touches;

	        if (isMouseEvent(event)) {
	          touches = event;
	        } else {
	          // ensure swiping with one touch and not pinching
	          if ( event.touches.length > 1 || event.scale && event.scale !== 1) {
	            return;
	          }

	          if (options.disableScroll) {
	            event.preventDefault();
	          }

	          touches = event.touches[0];
	        }

	        // measure change in x and y
	        delta = {
	          x: touches.pageX - start.x,
	          y: touches.pageY - start.y
	        };

	        // determine if scrolling test has run - one time test
	        if ( typeof isScrolling === 'undefined') {
	          isScrolling = !!( isScrolling || Math.abs(delta.x) < Math.abs(delta.y) );
	        }

	        // if user is not trying to scroll vertically
	        if (!isScrolling) {

	          // prevent native scrolling
	          event.preventDefault();

	          // stop slideshow
	          stop();

	          // increase resistance if first or last slide
	          if (options.continuous) { // we don't add resistance at the end

	            translate(circle(index-1), delta.x + slidePos[circle(index-1)], 0);
	            translate(index, delta.x + slidePos[index], 0);
	            translate(circle(index+1), delta.x + slidePos[circle(index+1)], 0);

	          } else {

	            delta.x =
	              delta.x /
	              ( (!index && delta.x > 0 ||             // if first slide and sliding left
	                 index === slides.length - 1 &&        // or if last slide and sliding right
	                 delta.x < 0                           // and if sliding at all
	                ) ?
	               ( Math.abs(delta.x) / width + 1 )      // determine resistance level
	               : 1 );                                 // no resistance if false

	            // translate 1:1
	            translate(index-1, delta.x + slidePos[index-1], 0);
	            translate(index, delta.x + slidePos[index], 0);
	            translate(index+1, delta.x + slidePos[index+1], 0);
	          }
	        }
	      },

	      end: function(event) {

	        // measure duration
	        var duration = +new Date() - start.time;

	        // determine if slide attempt triggers next/prev slide
	        var isValidSlide =
	            Number(duration) < 250 &&         // if slide duration is less than 250ms
	            Math.abs(delta.x) > 20 ||         // and if slide amt is greater than 20px
	            Math.abs(delta.x) > width/2;      // or if slide amt is greater than half the width

	        // determine if slide attempt is past start and end
	        var isPastBounds =
	            !index && delta.x > 0 ||                      // if first slide and slide amt is greater than 0
	            index === slides.length - 1 && delta.x < 0;   // or if last slide and slide amt is less than 0

	        if (options.continuous) {
	          isPastBounds = false;
	        }

	        // OLD determine direction of swipe (true:right, false:left)
	        // determine direction of swipe (1: backward, -1: forward)
	        var direction = Math.abs(delta.x) / delta.x;

	        // if not scrolling vertically
	        if (!isScrolling) {

	          if (isValidSlide && !isPastBounds) {

	            // if we're moving right
	            if (direction < 0) {

	              if (options.continuous) { // we need to get the next in this direction in place

	                move(circle(index-1), -width, 0);
	                move(circle(index+2), width, 0);

	              } else {
	                move(index-1, -width, 0);
	              }

	              move(index, slidePos[index]-width, speed);
	              move(circle(index+1), slidePos[circle(index+1)]-width, speed);
	              index = circle(index+1);

	            } else {
	              if (options.continuous) { // we need to get the next in this direction in place

	                move(circle(index+1), width, 0);
	                move(circle(index-2), -width, 0);

	              } else {
	                move(index+1, width, 0);
	              }

	              move(index, slidePos[index]+width, speed);
	              move(circle(index-1), slidePos[circle(index-1)]+width, speed);
	              index = circle(index-1);
	            }

	            runCallback(getPos(), slides[index], direction);

	          } else {

	            if (options.continuous) {

	              move(circle(index-1), -width, speed);
	              move(index, 0, speed);
	              move(circle(index+1), width, speed);

	            } else {

	              move(index-1, -width, speed);
	              move(index, 0, speed);
	              move(index+1, width, speed);
	            }
	          }
	        }

	        // kill touchmove and touchend event listeners until touchstart called again
	        if (isMouseEvent(event)) {
	          element.removeEventListener('mousemove', events, false);
	          element.removeEventListener('mouseup', events, false);
	          element.removeEventListener('mouseleave', events, false);
	        } else {
	          element.removeEventListener('touchmove', events, false);
	          element.removeEventListener('touchend', events, false);
	        }

	      },

	      transitionEnd: function(event) {
	        var currentIndex = parseInt(event.target.getAttribute('data-index'), 10);
	        if (currentIndex === index) {
	          if (delay || options.autoRestart) restart();

	          runTransitionEnd(getPos(), slides[index]);
	        }
	      }
	    };

	    // trigger setup
	    setup();

	    // start auto slideshow if applicable
	    begin();

	    // Expose the Swipe API
	    return {
	      // initialize
	      setup: setup,

	      // go to slide
	      slide: function(to, speed) {
	        stop();
	        slide(to, speed);
	      },

	      // move to previous
	      prev: function() {
	        stop();
	        prev();
	      },

	      // move to next
	      next: function() {
	        stop();
	        next();
	      },

	      // Restart slideshow
	      restart: restart,

	      // cancel slideshow
	      stop: stop,

	      // return current index position
	      getPos: getPos,

	      // disable slideshow
	      disable: disable,

	      // enable slideshow
	      enable: enable,

	      // return total number of slides
	      getNumSlides: function() { return length; },

	      // completely remove swipe
	      kill: kill
	    };

	    // remove all event listeners
	    function detachEvents() {
	      if (browser.addEventListener) {
	        // remove current event listeners
	        element.removeEventListener('touchstart', events, false);
	        element.removeEventListener('mousedown', events, false);
	        element.removeEventListener('webkitTransitionEnd', events, false);
	        element.removeEventListener('msTransitionEnd', events, false);
	        element.removeEventListener('oTransitionEnd', events, false);
	        element.removeEventListener('otransitionend', events, false);
	        element.removeEventListener('transitionend', events, false);
	        root.removeEventListener('resize', events, false);
	      } else {
	        root.onresize = null;
	      }
	    }

	    // add event listeners
	    function attachEvents() {
	      if (browser.addEventListener) {

	        // set touchstart event on element
	        if (browser.touch) {
	          element.addEventListener('touchstart', events, false);
	        }

	        if (options.draggable) {
	          element.addEventListener('mousedown', events, false);
	        }

	        if (browser.transitions) {
	          element.addEventListener('webkitTransitionEnd', events, false);
	          element.addEventListener('msTransitionEnd', events, false);
	          element.addEventListener('oTransitionEnd', events, false);
	          element.addEventListener('otransitionend', events, false);
	          element.addEventListener('transitionend', events, false);
	        }

	        // set resize event on window
	        root.addEventListener('resize', events, false);

	      } else {
	        root.onresize = throttledSetup; // to play nice with old IE
	      }
	    }

	    // clone nodes when there is only two slides
	    function cloneNode(el) {
	      var clone = el.cloneNode(true);
	      element.appendChild(clone);

	      // tag these slides as clones (to remove them on kill)
	      clone.setAttribute('data-cloned', true);

	      // Remove id from element
	      clone.removeAttribute('id');
	    }

	    function setup(opts) {
	      // Overwrite options if necessary
	      if (opts != null) {
	        for (var prop in opts) {
	          options[prop] = opts[prop];
	        }
	      }

	      // cache slides
	      slides = element.children;
	      length = slides.length;

	      // slides length correction, minus cloned slides
	      for (var i = 0; i < slides.length; i++) {
	        if (slides[i].getAttribute('data-cloned')) length--;
	      }

	      // set continuous to false if only one slide
	      if (slides.length < 2) {
	        options.continuous = false;
	      }

	      // special case if two slides
	      if (browser.transitions && options.continuous && slides.length < 3) {
	        cloneNode(slides[0]);
	        cloneNode(slides[1]);

	        slides = element.children;
	      }

	      // create an array to store current positions of each slide
	      slidePos = new Array(slides.length);

	      // determine width of each slide
	      width = container.getBoundingClientRect().width || container.offsetWidth;

	      element.style.width = (slides.length * width * 2) + 'px';

	      // stack elements
	      var pos = slides.length;
	      while(pos--) {
	        var slide = slides[pos];

	        slide.style.width = width + 'px';
	        slide.setAttribute('data-index', pos);

	        if (browser.transitions) {
	          slide.style.left = (pos * -width) + 'px';
	          move(pos, index > pos ? -width : (index < pos ? width : 0), 0);
	        }
	      }

	      // reposition elements before and after index
	      if (options.continuous && browser.transitions) {
	        move(circle(index-1), -width, 0);
	        move(circle(index+1), width, 0);
	      }

	      if (!browser.transitions) {
	        element.style.left = (index * -width) + 'px';
	      }

	      container.style.visibility = 'visible';

	      // reinitialize events
	      detachEvents();
	      attachEvents();
	    }

	    function prev() {
	      if (disabled) return;

	      if (options.continuous) {
	        slide(index-1);
	      } else if (index) {
	        slide(index-1);
	      }
	    }

	    function next() {
	      if (disabled) return;

	      if (options.continuous) {
	        slide(index+1);
	      } else if (index < slides.length - 1) {
	        slide(index+1);
	      }
	    }

	    function runCallback(pos, index, dir) {
	      if (options.callback) {
	        options.callback(pos, index, dir);
	      }
	    }

	    function runTransitionEnd(pos, index) {
	      if (options.transitionEnd) {
	        options.transitionEnd(pos, index);
	      }
	    }

	    function circle(index) {

	      // a simple positive modulo using slides.length
	      return (slides.length + (index % slides.length)) % slides.length;
	    }

	    function getPos() {
	      // Fix for the clone issue in the event of 2 slides
	      var currentIndex = index;

	      if (currentIndex >= length) {
	        currentIndex = currentIndex - length;
	      }

	      return currentIndex;
	    }

	    function slide(to, slideSpeed) {

	      // ensure to is of type 'number'
	      to = typeof to !== 'number' ? parseInt(to, 10) : to;

	      // do nothing if already on requested slide
	      if (index === to) return;

	      if (browser.transitions) {

	        var direction = Math.abs(index-to) / (index-to); // 1: backward, -1: forward

	        // get the actual position of the slide
	        if (options.continuous) {
	          var natural_direction = direction;
	          direction = -slidePos[circle(to)] / width;

	          // if going forward but to < index, use to = slides.length + to
	          // if going backward but to > index, use to = -slides.length + to
	          if (direction !== natural_direction) {
	            to = -direction * slides.length + to;
	          }

	        }

	        var diff = Math.abs(index-to) - 1;

	        // move all the slides between index and to in the right direction
	        while (diff--) {
	          move( circle((to > index ? to : index) - diff - 1), width * direction, 0);
	        }

	        to = circle(to);

	        move(index, width * direction, slideSpeed || speed);
	        move(to, 0, slideSpeed || speed);

	        if (options.continuous) { // we need to get the next in place
	          move(circle(to - direction), -(width * direction), 0);
	        }

	      } else {

	        to = circle(to);
	        animate(index * -width, to * -width, slideSpeed || speed);
	        // no fallback for a circular continuous if the browser does not accept transitions
	      }

	      index = to;
	      offloadFn(function() {
	        runCallback(getPos(), slides[index], direction);
	      });
	    }

	    function move(index, dist, speed) {
	      translate(index, dist, speed);
	      slidePos[index] = dist;
	    }

	    function translate(index, dist, speed) {

	      var slide = slides[index];
	      var style = slide && slide.style;

	      if (!style) return;

	      style.webkitTransitionDuration =
	        style.MozTransitionDuration =
	        style.msTransitionDuration =
	        style.OTransitionDuration =
	        style.transitionDuration = speed + 'ms';

	      style.webkitTransform = 'translate(' + dist + 'px,0)' + 'translateZ(0)';
	      style.msTransform =
	        style.MozTransform =
	        style.OTransform = 'translateX(' + dist + 'px)';

	    }

	    function animate(from, to, speed) {

	      // if not an animation, just reposition
	      if (!speed) {
	        element.style.left = to + 'px';
	        return;
	      }

	      var start = +new Date();

	      var timer = setInterval(function() {
	        var timeElap = +new Date() - start;

	        if (timeElap > speed) {

	          element.style.left = to + 'px';

	          if (delay || options.autoRestart) restart();

	          runTransitionEnd(getPos(), slides[index]);

	          clearInterval(timer);

	          return;
	        }

	        element.style.left = (( (to - from) * (Math.floor((timeElap / speed) * 100) / 100) ) + from) + 'px';
	      }, 4);

	    }

	    function begin() {
	      delay = options.auto || 0;
	      if (delay) interval = setTimeout(next, delay);
	    }

	    function stop() {
	      delay = 0;
	      clearTimeout(interval);
	    }

	    function restart() {
	      stop();
	      begin();
	    }

	    function disable() {
	      stop();
	      disabled = true;
	    }

	    function enable() {
	      disabled = false;
	      restart();
	    }

	    function isMouseEvent(e) {
	      return /^mouse/.test(e.type);
	    }

	    function kill() {
	      // cancel slideshow
	      stop();

	      // remove inline styles
	      container.style.visibility = '';

	      // reset element
	      element.style.width = '';
	      element.style.left = '';

	      // reset slides
	      var pos = slides.length;
	      while (pos--) {

	        if (browser.transitions) {
	          translate(pos, 0, 0);
	        }

	        var slide = slides[pos];

	        // if the slide is tagged as clone, remove it
	        if (slide.getAttribute('data-cloned')) {
	          var _parent = slide.parentElement;
	          _parent.removeChild(slide);
	        }

	        // remove styles
	        slide.style.width = '';
	        slide.style.left = '';

	        slide.style.webkitTransitionDuration =
	          slide.style.MozTransitionDuration =
	          slide.style.msTransitionDuration =
	          slide.style.OTransitionDuration =
	          slide.style.transitionDuration = '';

	        slide.style.webkitTransform =
	          slide.style.msTransform =
	          slide.style.MozTransform =
	          slide.style.OTransform = '';

	        // remove custom attributes (?)
	        // slide.removeAttribute('data-index');
	      }

	      // remove all events
	      detachEvents();

	      // remove throttled function timeout
	      throttledSetup.cancel();
	    }
	  }

	  if ( root.jQuery || root.Zepto ) {
	    (function($) {
	      $.fn.Swipe = function(params) {
	        return this.each(function() {
	          $(this).data('Swipe', new Swipe($(this)[0], params));
	        });
	      };
	    })( root.jQuery || root.Zepto );
	  }

	  return Swipe;
	}));

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var menu = __webpack_require__(80);
	var players = {};

	var $modal = $('.modal.modal-youtube');

	var close = function close() {
	    var id = $modal.attr('video-id');

	    var player = players[id];

	    if (player) {
	        player.destroy();
	        delete players[id];
	    }

	    $modal.removeClass('is-visible');
	};

	module.exports = {
	    close: close,
	    init: function init() {
	        $('.section-bg-youtube iframe').each(function (i, item) {
	            var player = new YT.Player($(item).attr('id'), {
	                events: {
	                    'onReady': function onReady(e) {
	                        if (window.mediaQueryMatch >= 2) {
	                            e.target.setLoop(false);
	                        } else {
	                            e.target.mute();
	                        }
	                    }
	                }
	            });
	        });

	        $('.youtube-video').each(function (i, item) {
	            $(item).on('click', function () {
	                var id = $(this).attr('data-id');
	                players[id] = new YT.Player('player', {
	                    height: $(window).height() > 500 ? '500px' : '200px',
	                    width: '100%',
	                    playerVars: { rel: 0, showinfo: 0, controls: 0 },
	                    videoId: id,
	                    events: {
	                        'onReady': function onReady(e) {
	                            menu.stop();

	                            $modal.attr('video-id', id);
	                            $modal.addClass('is-visible');
	                            e.target.playVideo();

	                            $modal.on('click', '.modal-bg', close);

	                            $modal.on('click', '.modal-close', close);
	                        }
	                    }
	                });
	            });
	        });
	    }
	};

/***/ }),
/* 87 */
/***/ (function(module, exports) {

	'use strict';

	var $bgContainer = $('.bg-video');

	var init = function init() {
	    $bgContainer.each(function (index, item) {
	        setTimeout(function () {
	            var data = $(item).data('background-video');
	            var html = '<video class="section-bg-video" poster="/static/i/' + data.poster + '" autoplay loop muted playsinline>\n                                            <source\n                                                    src="/static/i/' + data.webm + '"\n                                                    type="video/webm">\n                                            <source\n                                                    src="/static/i/' + data.mp4 + '"\n                                                    type="video/mp4">\n                                            <source\n                                                    src="/static/i/' + data.ogv + '"\n                                                    type="video/ogg">\n                                        </video>';

	            $(item).html(html);
	            $(item).addClass('is-ready');
	            $(item).css({ background: 'none' });
	        }, 3500);
	    });
	};

	module.exports = {
	    init: init
	};

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _stringify = __webpack_require__(3);

	var _stringify2 = _interopRequireDefault(_stringify);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var $form = $('.follow-form');
	var init = function init() {
	    var submit = function submit(e) {
	        e.preventDefault();
	        ga('send', 'event', 'follow-form', 'submit');

	        var emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

	        var headers = {
	            "content-type": "application/json"
	        };

	        var email = $form.find('input[name=email]').val();

	        if (!email || !emailPattern.test(email)) {
	            return;
	        }
	        if ($('[name=captcha]', $form).val()) {
	            headers['x-captcha'] = $('[name=captcha]', $form).val();
	        }

	        if ($('[name=captcha-code]', $form).val()) {
	            headers['x-captcha-code'] = $('[name=captcha-code]', $form).val();
	        }

	        if (window._checkCode_) {
	            headers["x-check-code"] = window._checkCode_;
	        }

	        $form.addClass('loading');

	        $.ajax({
	            type: 'POST',
	            url: '/subscribe/',
	            dataType: 'json',
	            headers: headers,
	            data: (0, _stringify2.default)({
	                email: $('[name=email]', $form).val(),
	                topic: $('[name=topic]', $form).val(),
	                topicValue: $('[name=topicValue]', $form).val(),
	                details: {}
	            }),
	            success: function success(data) {
	                if (data.status === 'success') {
	                    $form.trigger('ok');
	                } else if (data.data && data.data.title === "Member Exists") {
	                    $form.trigger('ok');
	                } else if (data.status === 'captcha') {
	                    ga('send', 'event', 'contact-form', 'captcha');
	                    $form.trigger('captcha', data.data);
	                } else if (data.error && data.error === "Email has already been taken") {
	                    $form.trigger('error');
	                } else {
	                    $form.trigger('error');
	                }
	            },
	            error: function error() {
	                $form.trigger('error');
	            }
	        });
	    };

	    $form.on('click', '.js-submit', submit);
	    $form.on('submit', submit);

	    $form.on('click', function () {
	        $form.removeClass('is-error');
	    });

	    $form.on('ok', function () {
	        ga('send', 'event', 'follow-form', 'successful');
	        $form.removeClass('loading');
	        $form.removeClass('is-captcha');
	        $form.removeClass('is-error');
	        $form.addClass('is-done');
	    });

	    $form.on('error', function () {
	        ga('send', 'event', 'follow-form', 'error');
	        $form.removeClass('loading');
	        $form.removeClass('is-captcha');
	        $form.removeClass('is-done');
	        $form.addClass('is-error');
	        setTimeout(function () {
	            $form.removeClass('is-error');
	        }, 5000);
	    });

	    $form.on('captcha', function (e, data) {
	        $form.removeClass('loading');
	        $form.removeClass('is-done');
	        $form.removeClass('is-error');

	        $form.find('.js-captcha-code').val(data.code);

	        var $label = $form.find('.js-captcha .form-input-label');
	        $label.html('Hi, it\u2019s Roborace AI speaking.<br />I&nbsp;believe&nbsp;you&nbsp;are one of us. Aren\u2019t&nbsp;you?<br /><br />%%captcha%% is:'.replace('%%captcha%%', data.question));
	        $form.addClass('is-captcha');
	    });
	};

	module.exports = {
	    init: init
	};

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var menu = __webpack_require__(80);
	var $partners = $('.partners');
	var $partnerLogos = $('.partner-logos');
	var sliderLoop = $('.bg-loop-partner');
	var partnerLink = $('.partner-partnership-link');
	var Swipe = __webpack_require__(85);
	var activeLogo = void 0;
	var activePartner = void 0;
	var $partnersArray = $('.partners-i').children().filter(function () {
	    return $(this).data("partner");
	});

	var init = function init() {

	    sliderLoop.each(function (index, item) {
	        var mySwipePartner = new Swipe(item, {
	            startSlide: 0,
	            draggable: true,
	            disableScroll: true,
	            autoRestart: false,
	            continuous: true
	        });

	        var changeActivePartner = function changeActivePartner(partner, partnerNumber) {

	            mySwipePartner.slide(partnerNumber, 1);

	            var partnerName = partner.dataset.partner;

	            if (activeLogo) {
	                activeLogo.classList.remove('is-active');
	            }

	            if (activePartner) {
	                activePartner.classList.remove('is-active');
	            }

	            activeLogo = $partnerLogos.find('.' + partnerName)[0];
	            activeLogo.classList.add('is-active');

	            activePartner = partner;
	            activePartner.classList.add('is-active');
	        };

	        var becomePartner = function becomePartner() {
	            partnerLink.on('click', function () {
	                window.location.hash = "#enquire/join-us-as-partner-or-sponsor/step1";
	                menu.makeActive('enquire');
	            });
	        };

	        $partners.find('.partner-title').on('click', function (e) {
	            var index = $(e.currentTarget).index();
	            changeActivePartner(e.currentTarget, index);
	        });

	        becomePartner();

	        changeActivePartner($partnersArray[0], 0);
	        mySwipePartner.slide($partnersArray[0], 1);
	    });
	};

	module.exports = {
	    init: init
	};

/***/ }),
/* 90 */
/***/ (function(module, exports) {

	'use strict';

	var $video = $('.video');
	var $videoList = $video.find('.video-list');
	var $videoListItems = $videoList.find('.video-list-item');
	var $prevBtn = $video.find('.video-btn-prev');
	var $nextBtn = $video.find('.video-btn-next');
	var itemsLength = $videoListItems.length;
	var videoIndex = itemsLength;

	var checkButtons = function checkButtons() {
	    if (videoIndex < itemsLength) {
	        $nextBtn.addClass('btn-shown');
	    } else {
	        $nextBtn.removeClass('btn-shown');
	    }

	    if (videoIndex - 1 > 0) {
	        $prevBtn.addClass('btn-shown');
	    } else {
	        $prevBtn.removeClass('btn-shown');
	    }
	};

	var init = function init() {

	    if (screen.width >= 768 || Math.abs(window.orientation) > 65) {
	        var itemWidth = $videoListItems.eq(0).width();
	        $videoList.css({
	            transform: 'translate3d(' + itemWidth * (itemsLength - videoIndex) + 'px, 0, 0)'
	        });

	        $nextBtn.on('click', function () {
	            ++videoIndex;

	            $videoList.css({
	                transform: 'translate3d(' + itemWidth * (itemsLength - videoIndex) + 'px, 0, 0)'
	            });

	            checkButtons();
	        });

	        $prevBtn.on('click', function () {
	            --videoIndex;
	            $videoList.css({
	                transform: 'translate3d(' + itemWidth * (itemsLength - videoIndex) + 'px, 0, 0)'
	            });

	            checkButtons();
	        });

	        checkButtons();
	    }
	};

	module.exports = {
	    init: init
	};

/***/ })
/******/ ]);
